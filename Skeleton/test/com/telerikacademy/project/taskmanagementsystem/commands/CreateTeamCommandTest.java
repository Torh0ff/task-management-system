package com.telerikacademy.project.taskmanagementsystem.commands;

import com.telerikacademy.project.taskmanagementsystem.commands.contracts.Command;
import com.telerikacademy.project.taskmanagementsystem.core.TaskManagementRepositoryImpl;
import com.telerikacademy.project.taskmanagementsystem.core.contract.TaskManagementRepository;
import com.telerikacademy.project.taskmanagementsystem.exceptions.InvalidArgumentCountException;
import com.telerikacademy.project.taskmanagementsystem.models.team.TeamImplTest;
import com.telerikacademy.project.taskmanagementsystem.models.team.contacts.Person;
import com.telerikacademy.project.taskmanagementsystem.models.team.contacts.Team;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static com.telerikacademy.project.taskmanagementsystem.TestUtilities.getList;
import static com.telerikacademy.project.taskmanagementsystem.models.team.PersonImplTest.*;
import static com.telerikacademy.project.taskmanagementsystem.models.team.TeamImplTest.initializeTeam;
import static org.junit.jupiter.api.Assertions.*;

public class CreateTeamCommandTest {

    private static final int ARGUMENTS_COUNT = 1;

    private Command createTeamCommand;
    private TaskManagementRepository taskManagementRepository;

    @BeforeEach
    public void beforeEach(){
        taskManagementRepository = new TaskManagementRepositoryImpl();
        createTeamCommand = new CreateTeamCommand(taskManagementRepository);
    }

    @Test
    public void should_ThrowException_When_ArgumentCountDifferentThanExpected() {
        // Arrange
        List<String> params = getList(ARGUMENTS_COUNT - 1);

        // Act, Assert
        Assertions.assertThrows(InvalidArgumentCountException.class, () -> createTeamCommand.execute(params));
    }

    @Test
    public void should_ThrowException_When_TeamExists_Already(){
        Team team = initializeTeam();
        taskManagementRepository.createTeam(team.getName());
        List<String> params = List.of(TeamImplTest.VALID_NAME);

        assertThrows(IllegalArgumentException.class, () -> createTeamCommand.execute(params));
    }

    @Test
    public void should_ThrowException_When_TeamName_Invalid(){
        List<String> params = List.of(TeamImplTest.INVALID_NAME_LONGER);

        assertThrows(IllegalArgumentException.class, () -> createTeamCommand.execute(params));
    }

    @Test
    public void should_Execute_When_Valid(){
        Team team = initializeTeam();
        List<String> params = List.of(team.getName());

        assertDoesNotThrow(() -> createTeamCommand.execute(params));
    }


}