package com.telerikacademy.project.taskmanagementsystem.commands;

import com.telerikacademy.project.taskmanagementsystem.commands.contracts.Command;
import com.telerikacademy.project.taskmanagementsystem.core.TaskManagementRepositoryImpl;
import com.telerikacademy.project.taskmanagementsystem.core.contract.TaskManagementRepository;
import com.telerikacademy.project.taskmanagementsystem.exceptions.InvalidArgumentCountException;
import com.telerikacademy.project.taskmanagementsystem.models.tasks.contracts.enums.TaskPriority;
import com.telerikacademy.project.taskmanagementsystem.models.tasks.contracts.enums.story.Size;
import com.telerikacademy.project.taskmanagementsystem.models.team.BoardImpl;
import com.telerikacademy.project.taskmanagementsystem.models.team.BoardImplTest;
import com.telerikacademy.project.taskmanagementsystem.models.team.TeamImplTest;
import com.telerikacademy.project.taskmanagementsystem.models.team.contacts.Board;
import com.telerikacademy.project.taskmanagementsystem.models.team.contacts.Person;
import com.telerikacademy.project.taskmanagementsystem.models.team.contacts.Team;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static com.telerikacademy.project.taskmanagementsystem.TestUtilities.getList;
import static com.telerikacademy.project.taskmanagementsystem.models.team.PersonImplTest.*;
import static com.telerikacademy.project.taskmanagementsystem.models.team.TeamImplTest.initializeTeam;
import static java.util.Arrays.asList;
import static org.junit.jupiter.api.Assertions.*;

public class CreateNewBoardInTeamCommandTests {

    public static final int EXPECTED_ARGUMENTS_COUNT = 2;

    private Command createNewBoardInTeamCommand;
    private TaskManagementRepository taskManagementRepository;

    @BeforeEach
    public void beforeEach() {
        taskManagementRepository = new TaskManagementRepositoryImpl();
        createNewBoardInTeamCommand = new CreateNewBoardInTeamCommand(taskManagementRepository);
    }

    @Test
    public void should_ThrowException_When_ArgumentCountDifferentThanExpected() {
        // Arrange
        List<String> params = getList(EXPECTED_ARGUMENTS_COUNT - 1);

        // Act, Assert
        Assertions.assertThrows(InvalidArgumentCountException.class, () -> createNewBoardInTeamCommand.execute(params));
    }


    @Test
    public void should_ThrowException_When_BoardName_Invalid() {
        List<String> params = List.of(BoardImplTest.INVALID_NAME_LONGER, "TeamName");

        assertThrows(IllegalArgumentException.class, () -> createNewBoardInTeamCommand.execute(params));
    }


    @Test
    public void should_CreateBoard_When_Input_Is_Valid() {

        Board board = new BoardImpl("Abracada");
        taskManagementRepository.createBoard("newBoard");
        Team team = TeamImplTest.initializeTeam();
        taskManagementRepository.createTeam(team.getName());
        List<String> params = List.of(board.getName(), team.getName());
        // Act, Assert

        assertAll(
                () -> assertDoesNotThrow(() -> createNewBoardInTeamCommand.execute(params)),
                () -> assertEquals(2, taskManagementRepository.getBoards().size())
        );

    }

    @Test
    public void should_ThrowException_When_Bug_Exists() {
        // Arrange
        Board board = new BoardImpl("Abraca");
        taskManagementRepository.createBoard(board.getName());
        Team team = TeamImplTest.initializeTeam();


        taskManagementRepository.createBoard(board.getName());

        List<String> params = List.of(board.getName(), team.getName());
        // Act, Assert

        assertThrows(IllegalArgumentException.class, () -> createNewBoardInTeamCommand.execute(params));

    }
}
