package com.telerikacademy.project.taskmanagementsystem.commands;

import com.telerikacademy.project.taskmanagementsystem.commands.contracts.Command;
import com.telerikacademy.project.taskmanagementsystem.core.TaskManagementRepositoryImpl;
import com.telerikacademy.project.taskmanagementsystem.core.contract.TaskManagementRepository;
import com.telerikacademy.project.taskmanagementsystem.exceptions.InvalidArgumentCountException;
import com.telerikacademy.project.taskmanagementsystem.models.tasks.BugImpl;
import com.telerikacademy.project.taskmanagementsystem.models.tasks.CommentImpl;
import com.telerikacademy.project.taskmanagementsystem.models.tasks.contracts.Bug;
import com.telerikacademy.project.taskmanagementsystem.models.tasks.contracts.Comment;
import com.telerikacademy.project.taskmanagementsystem.models.tasks.contracts.Task;
import com.telerikacademy.project.taskmanagementsystem.models.tasks.contracts.enums.TaskPriority;
import com.telerikacademy.project.taskmanagementsystem.models.tasks.contracts.enums.bug.Severity;
import com.telerikacademy.project.taskmanagementsystem.models.tasks.contracts.enums.story.Size;
import com.telerikacademy.project.taskmanagementsystem.models.team.BoardImpl;
import com.telerikacademy.project.taskmanagementsystem.models.team.PersonImpl;
import com.telerikacademy.project.taskmanagementsystem.models.team.PersonImplTest;
import com.telerikacademy.project.taskmanagementsystem.models.team.TeamImplTest;
import com.telerikacademy.project.taskmanagementsystem.models.team.contacts.Board;
import com.telerikacademy.project.taskmanagementsystem.models.team.contacts.Person;
import com.telerikacademy.project.taskmanagementsystem.models.team.contacts.Team;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.net.CookieHandler;
import java.util.List;

import static com.telerikacademy.project.taskmanagementsystem.TestUtilities.getList;
import static com.telerikacademy.project.taskmanagementsystem.models.team.PersonImplTest.VALID_NAME;
import static com.telerikacademy.project.taskmanagementsystem.models.team.PersonImplTest.initializePerson;
import static com.telerikacademy.project.taskmanagementsystem.models.team.TeamImplTest.*;
import static org.junit.jupiter.api.Assertions.*;


class ShowTaskActivityCommandTest {

    private static final int EXPECTED_ARGUMENTS_COUNT = 1;
    private Command showTaskActivityCommand;
    private TaskManagementRepository taskManagementRepository;

    @BeforeEach
    public void beforeEach() {
        taskManagementRepository = new TaskManagementRepositoryImpl();
        showTaskActivityCommand = new ShowTaskActivityCommand(taskManagementRepository);
    }

    @Test
    public void should_ThrowException_When_ArgumentCountDifferentThanExpected() {
        // Arrange
        List<String> params = getList(EXPECTED_ARGUMENTS_COUNT - 1);

        // Act, Assert
        assertThrows(InvalidArgumentCountException.class, () -> showTaskActivityCommand.execute(params));
    }

    @Test
    public void should_ThrowException_When_FindTaskByIdDoesNotExist() {
        // Arrange


        List<String> params = List.of("1");

        // Act, Assert

        assertThrows(IllegalArgumentException.class, () -> showTaskActivityCommand.execute(params));

    }

    @Test
    public void should_ReturnToString_When_Input_Is_Valid() {

        taskManagementRepository.createBug("TitleBugOne", "Description", Severity.CRITICAL,
                TaskPriority.LOW, new BoardImpl("BoardImpl"), List.of("StepOne", "StepTwo"));
        List<String> params = List.of("1");

        assertAll(
                () -> assertDoesNotThrow(() -> showTaskActivityCommand.execute(params)));
        // () -> assertEquals(1, bug.getComments().size()));

    }


}