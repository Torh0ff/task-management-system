package com.telerikacademy.project.taskmanagementsystem.commands;

import com.telerikacademy.project.taskmanagementsystem.commands.contracts.Command;
import com.telerikacademy.project.taskmanagementsystem.core.TaskManagementRepositoryImpl;
import com.telerikacademy.project.taskmanagementsystem.core.contract.TaskManagementRepository;
import com.telerikacademy.project.taskmanagementsystem.exceptions.InvalidArgumentCountException;
import com.telerikacademy.project.taskmanagementsystem.models.tasks.contracts.enums.TaskPriority;
import com.telerikacademy.project.taskmanagementsystem.models.tasks.contracts.enums.bug.Severity;
import com.telerikacademy.project.taskmanagementsystem.models.team.BoardImpl;
import com.telerikacademy.project.taskmanagementsystem.models.team.TeamImplTest;
import com.telerikacademy.project.taskmanagementsystem.models.team.contacts.Board;
import com.telerikacademy.project.taskmanagementsystem.models.team.contacts.Team;
//import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static com.telerikacademy.project.taskmanagementsystem.TestUtilities.getList;

import static org.junit.jupiter.api.Assertions.*;

public class CreateFeedbackCommandTests {


    private static final int ARGUMENTS_COUNT = 7;
    private Command createFeedbackCommand;
    private TaskManagementRepository taskManagementRepository;

    @BeforeEach
    public void beforeEach() {
        taskManagementRepository = new TaskManagementRepositoryImpl();
        createFeedbackCommand = new CreateFeedbackCommand(taskManagementRepository);
    }


    @Test
    public void should_ThrowException_When_ArgumentCountDifferentThanExpected() {
        // Arrange
        List<String> params = getList(ARGUMENTS_COUNT - 5);

        // Act, Assert
        assertThrows(InvalidArgumentCountException.class, () -> createFeedbackCommand.execute(params));
    }

    @Test
    public void should_ThrowException_When_FindBoardByNameDoesNotExist() {
        // Arrange
        Board board = new BoardImpl("Abraca");
        Team team = TeamImplTest.initializeTeam();
        taskManagementRepository.createTeam(team.getName());
        //String title, String description, int rating, String nameOfBoard, String nameOfTeam
        List<String> params = List.of("Title", "Description", "7", board.getName(), team.getName());

        // Act, Assert

        assertThrows(IllegalArgumentException.class, () -> createFeedbackCommand.execute(params));

    }


    @Test
    public void should_ThrowException_When_FindTeamByNameDoesNotExist() {
        // Arrange
        Board board = new BoardImpl("Abraca");
        taskManagementRepository.createBoard(board.getName());
        Team team = TeamImplTest.initializeTeam();

        List<String> params = List.of("Title", "Description", "7", board.getName(), team.getName());
        // Act, Assert

        assertThrows(IllegalArgumentException.class, () -> createFeedbackCommand.execute(params));

    }


    @Test
    public void should_CreateFeedback_When_Input_Is_Valid() {
        Board board = new BoardImpl("Abraca");
        taskManagementRepository.createBoard(board.getName());
        Team team = TeamImplTest.initializeTeam();
        taskManagementRepository.createTeam(team.getName());

        List<String> params = List.of("TitleNewTitle", "Description", "7", board.getName(), team.getName());
        // Act, Assert

        assertAll(
               () -> assertDoesNotThrow(() -> createFeedbackCommand.execute(params)),
                () -> assertEquals(1, taskManagementRepository.
                        getFeedbacks().size()));

    }

    @Test
    public void should_ThrowException_When_Bug_Exists() {
        // Arrange
        Board board = new BoardImpl("Abraca");
        taskManagementRepository.createBoard(board.getName());
        Team team = TeamImplTest.initializeTeam();


        taskManagementRepository.createFeedback("FeedbackTest", "Description", 3, board);

        List<String> params = List.of("Description", "FeedbackTest", "3", board.getName(), team.getName());
        // Act, Assert

        assertThrows(IllegalArgumentException.class, () -> createFeedbackCommand.execute(params));

    }

}
