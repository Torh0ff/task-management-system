package com.telerikacademy.project.taskmanagementsystem.commands;


import com.telerikacademy.project.taskmanagementsystem.commands.contracts.Command;
import com.telerikacademy.project.taskmanagementsystem.core.TaskManagementRepositoryImpl;
import com.telerikacademy.project.taskmanagementsystem.core.contract.TaskManagementRepository;
import com.telerikacademy.project.taskmanagementsystem.exceptions.InvalidArgumentCountException;
import com.telerikacademy.project.taskmanagementsystem.models.tasks.StoryImpl;
import com.telerikacademy.project.taskmanagementsystem.models.tasks.contracts.Story;
import com.telerikacademy.project.taskmanagementsystem.models.tasks.contracts.enums.TaskPriority;
import com.telerikacademy.project.taskmanagementsystem.models.tasks.contracts.enums.TaskStatus;
import com.telerikacademy.project.taskmanagementsystem.models.tasks.contracts.enums.story.Size;
import com.telerikacademy.project.taskmanagementsystem.models.team.BoardImpl;
import com.telerikacademy.project.taskmanagementsystem.models.team.PersonImpl;
import com.telerikacademy.project.taskmanagementsystem.models.team.PersonImplTest;
import com.telerikacademy.project.taskmanagementsystem.models.team.TeamImplTest;
import com.telerikacademy.project.taskmanagementsystem.models.team.contacts.Person;
import com.telerikacademy.project.taskmanagementsystem.models.team.contacts.Team;
import com.telerikacademy.project.taskmanagementsystem.utils.ValidationHelpers;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Collections;
import java.util.List;

import static com.telerikacademy.project.taskmanagementsystem.TestUtilities.getList;
import static com.telerikacademy.project.taskmanagementsystem.models.team.PersonImplTest.*;
import static com.telerikacademy.project.taskmanagementsystem.models.team.TeamImplTest.initializeTeam;
import static org.junit.jupiter.api.Assertions.*;

public class ShowAllTeamsCommandTests {


    private static final int EXPECTED_ARGUMENTS = 0;

    private Command showAllTeamsCommand;

    private TaskManagementRepository taskManagementRepository;

    @BeforeEach
    public void beforeEach() {
        taskManagementRepository = new TaskManagementRepositoryImpl();
        showAllTeamsCommand = new ShowAllTeamsCommand(taskManagementRepository);
    }


    @Test
    public void should_ThrowException_When_ParametersCountDifferentThanExpected() {
        // Arrange
        List<String> params = getList(EXPECTED_ARGUMENTS + 1);

        // Act, Assert
        Assertions.assertThrows(InvalidArgumentCountException.class, () -> showAllTeamsCommand.execute(params));
    }


    @Test
    public void should_ThrowException_When_Teams_List_Empty() {
        // Arrange
        List<String> params = List.of();
        // Act, Assert
        Assertions.assertThrows(IllegalArgumentException.class, () -> showAllTeamsCommand.execute(params));
    }


    @Test
    public void should_ShowAllTeams_When_InputIsValid() {
        //Arrange

        taskManagementRepository.createTeam("TeamBest");
        List<String> params = List.of();

        //Act, Assert
        assertAll(
                () -> assertDoesNotThrow(() -> showAllTeamsCommand.execute(params)),
                () -> assertEquals(String.format("==================================================================================================%n" +
                        "Teams----%n --1. TeamBest%n=================================================================================================="), showAllTeamsCommand.execute(params))
        );
    }
}
