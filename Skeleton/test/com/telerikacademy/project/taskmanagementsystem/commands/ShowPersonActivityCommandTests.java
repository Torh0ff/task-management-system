package com.telerikacademy.project.taskmanagementsystem.commands;

import com.telerikacademy.project.taskmanagementsystem.commands.contracts.Command;
import com.telerikacademy.project.taskmanagementsystem.core.TaskManagementRepositoryImpl;
import com.telerikacademy.project.taskmanagementsystem.core.contract.TaskManagementRepository;
import com.telerikacademy.project.taskmanagementsystem.exceptions.InvalidArgumentCountException;
import com.telerikacademy.project.taskmanagementsystem.models.tasks.StoryImpl;
import com.telerikacademy.project.taskmanagementsystem.models.tasks.contracts.Story;
import com.telerikacademy.project.taskmanagementsystem.models.tasks.contracts.enums.TaskPriority;
import com.telerikacademy.project.taskmanagementsystem.models.tasks.contracts.enums.TaskStatus;
import com.telerikacademy.project.taskmanagementsystem.models.tasks.contracts.enums.story.Size;
import com.telerikacademy.project.taskmanagementsystem.models.team.BoardImpl;
import com.telerikacademy.project.taskmanagementsystem.models.team.PersonImpl;
import com.telerikacademy.project.taskmanagementsystem.models.team.PersonImplTest;
import com.telerikacademy.project.taskmanagementsystem.models.team.TeamImplTest;
import com.telerikacademy.project.taskmanagementsystem.models.team.contacts.Person;
import com.telerikacademy.project.taskmanagementsystem.models.team.contacts.Team;
import com.telerikacademy.project.taskmanagementsystem.utils.ValidationHelpers;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.text.DateFormat;
import java.time.LocalDateTime;
import java.text.SimpleDateFormat;
import java.util.Date.*;

import java.util.Collections;
import java.util.List;

import static com.telerikacademy.project.taskmanagementsystem.TestUtilities.getList;
import static com.telerikacademy.project.taskmanagementsystem.models.team.PersonImplTest.*;
import static com.telerikacademy.project.taskmanagementsystem.models.team.TeamImplTest.initializeTeam;
import static org.junit.jupiter.api.Assertions.*;

public class ShowPersonActivityCommandTests {


    private static final int EXPECTED_PARAMETERS_COUNT = 1;

    private Command showPersonActivityCommand;

    private TaskManagementRepository taskManagementRepository;

    @BeforeEach
    public void beforeEach() {
        taskManagementRepository = new TaskManagementRepositoryImpl();
        showPersonActivityCommand = new ShowPersonActivityCommand(taskManagementRepository);
    }


    @Test
    public void should_ThrowException_When_ParametersCountDifferentThanExpected() {
        // Arrange
        List<String> params = getList(EXPECTED_PARAMETERS_COUNT - 1);

        // Act, Assert
        Assertions.assertThrows(InvalidArgumentCountException.class, () -> showPersonActivityCommand.execute(params));
    }

    @Test
    public void should_ThrowException_When_Person_Is_NotFound() {
        // Arrange
        List<String> params = List.of("Aleks");
        // Act, Assert
        Assertions.assertThrows(IllegalArgumentException.class, () -> showPersonActivityCommand.execute(params));
    }

    @Test
    public void should_Execute_When_Valid() {
        // Arrange
        List<String> params = List.of("Aleks");
        taskManagementRepository.createPerson("Aleks");
        // Act, Assert
        assertDoesNotThrow(() -> showPersonActivityCommand.execute(params));
    }
}
