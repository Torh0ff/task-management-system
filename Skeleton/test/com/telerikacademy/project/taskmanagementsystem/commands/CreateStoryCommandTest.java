package com.telerikacademy.project.taskmanagementsystem.commands;

import com.telerikacademy.project.taskmanagementsystem.commands.contracts.Command;
import com.telerikacademy.project.taskmanagementsystem.core.TaskManagementRepositoryImpl;
import com.telerikacademy.project.taskmanagementsystem.core.contract.TaskManagementRepository;
import com.telerikacademy.project.taskmanagementsystem.exceptions.InvalidArgumentCountException;
import com.telerikacademy.project.taskmanagementsystem.models.tasks.contracts.enums.TaskPriority;
import com.telerikacademy.project.taskmanagementsystem.models.tasks.contracts.enums.story.Size;
import com.telerikacademy.project.taskmanagementsystem.models.team.BoardImpl;
import com.telerikacademy.project.taskmanagementsystem.models.team.TeamImplTest;
import com.telerikacademy.project.taskmanagementsystem.models.team.contacts.Board;
import com.telerikacademy.project.taskmanagementsystem.models.team.contacts.Team;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static com.telerikacademy.project.taskmanagementsystem.TestUtilities.getList;
import static org.junit.jupiter.api.Assertions.*;

class CreateStoryCommandTest {


    private static final int ARGUMENTS_COUNT = 6;
    private Command createStoryCommand;
    private TaskManagementRepository taskManagementRepository;

    @BeforeEach
    public void beforeEach(){
        taskManagementRepository = new TaskManagementRepositoryImpl();
        createStoryCommand = new CreateStoryCommand(taskManagementRepository);
    }

    @Test
    public void should_ThrowException_When_ArgumentCountDifferentThanExpected() {
        // Arrange
        List<String> params = getList(ARGUMENTS_COUNT - 1);

        // Act, Assert
        assertThrows(InvalidArgumentCountException.class, () -> createStoryCommand.execute(params));
    }

    @Test
    public void should_ThrowException_When_FindBoardByNameDoesNotExist() {
        // Arrange
        Board board = new BoardImpl("Abraca");
        Team team = TeamImplTest.initializeTeam();
        taskManagementRepository.createTeam(team.getName());
        List<String> params = List.of("Title", "Description",
                Size.LARGE.toString(), TaskPriority.LOW.toString(), board.getName(), team.getName());
        // Act, Assert

        assertThrows(IllegalArgumentException.class,  () -> createStoryCommand.execute(params));

    }

    @Test
    public void should_ThrowException_When_FindTeamByNameDoesNotExist() {
        // Arrange
        Board board = new BoardImpl("Abraca");
        taskManagementRepository.createBoard(board.getName());
        Team team = TeamImplTest.initializeTeam();
        List<String> params = List.of("Title", "Description",
                Size.LARGE.toString(), TaskPriority.LOW.toString(), board.getName(), team.getName());
        // Act, Assert

        assertThrows(IllegalArgumentException.class,  () -> createStoryCommand.execute(params));

    }

    @Test
    public void should_ThrowException_When_Task_Exists() {
        // Arrange
        Board board = new BoardImpl("Abraca");
        taskManagementRepository.createBoard(board.getName());
        Team team = TeamImplTest.initializeTeam();
        taskManagementRepository.createStory("TitleTestName", "Description", Size.LARGE,
                TaskPriority.LOW, board);
        List<String> params = List.of("Description", "TitleTestName",
                Size.LARGE.toString(), TaskPriority.LOW.toString(), board.getName(), team.getName());
        // Act, Assert

        assertThrows(IllegalArgumentException.class,  () -> createStoryCommand.execute(params));

    }

    @Test
    public void should_CreateTask_When_Input_Is_Valid() {
        Board board = new BoardImpl("Abraca");
        taskManagementRepository.createBoard(board.getName());
        Team team = TeamImplTest.initializeTeam();
        taskManagementRepository.createTeam(team.getName());
        List<String> params = List.of("DreamTeamTheBest!", "Description",
                Size.LARGE.toString(), TaskPriority.LOW.toString(), board.getName(), team.getName());
        // Act, Assert

        assertAll(
                () -> assertDoesNotThrow(() -> createStoryCommand.execute(params)),
                () -> assertEquals(1, taskManagementRepository.getTasks().size())
        );

    }

}