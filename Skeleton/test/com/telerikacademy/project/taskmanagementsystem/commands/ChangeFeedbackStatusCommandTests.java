package com.telerikacademy.project.taskmanagementsystem.commands;

import com.telerikacademy.project.taskmanagementsystem.commands.contracts.Command;
import com.telerikacademy.project.taskmanagementsystem.core.TaskManagementRepositoryImpl;
import com.telerikacademy.project.taskmanagementsystem.core.contract.TaskManagementRepository;
import com.telerikacademy.project.taskmanagementsystem.exceptions.InvalidArgumentCountException;
import com.telerikacademy.project.taskmanagementsystem.models.tasks.FeedbackImpl;
import com.telerikacademy.project.taskmanagementsystem.models.tasks.contracts.Feedback;
import com.telerikacademy.project.taskmanagementsystem.models.tasks.contracts.enums.feedback.FeedbackStatus;
import com.telerikacademy.project.taskmanagementsystem.models.team.BoardImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static com.telerikacademy.project.taskmanagementsystem.TestUtilities.getList;
import static org.junit.jupiter.api.Assertions.*;

public class ChangeFeedbackStatusCommandTests {

    private static final int EXPECTED_NUMBER_OF_ARGUMENTS_CHANGE = 2;
    private Command changeFeedbackStatusCommand;
    private TaskManagementRepository taskManagementRepository;

    @BeforeEach
    public void beforeEach() {
        taskManagementRepository = new TaskManagementRepositoryImpl();
        changeFeedbackStatusCommand = new ChangeFeedbackStatusCommand(taskManagementRepository);
    }

    @Test
    public void should_ThrowException_When_ArgumentCountDifferentThanExpected() {
        // Arrange
        List<String> params = getList(EXPECTED_NUMBER_OF_ARGUMENTS_CHANGE - 1);

        // Act, Assert
        assertThrows(InvalidArgumentCountException.class, () -> changeFeedbackStatusCommand.execute(params));
    }

    @Test
    public void should_ChangeFeedback_When_InputIs_Valid() {
        // Arrange
        List<String> params = List.of("1", FeedbackStatus.DONE.toString());
        // Act, Assert

        Feedback feedback = initializeFeedback();
        taskManagementRepository.createFeedback("TitleFeedback",
                "Description", 5,
                new BoardImpl("Feedback"));

        assertAll(
                () -> assertDoesNotThrow(() -> changeFeedbackStatusCommand.execute(params)),
                () -> assertNotEquals(FeedbackStatus.NEW.toString(), params.get(1))
        );
    }

    @Test
    public void should_Throw_Exception_When_Same_Status() {
        // Arrange
        List<String> params = List.of("1", FeedbackStatus.DONE.toString());
        // Act, Assert

        taskManagementRepository.createFeedback("TitleFeedback",
                "Description", 5,
                new BoardImpl("Feedback"));

        taskManagementRepository.findTaskByName(taskManagementRepository.getFeedbacks(), "Feedback",
                "TitleFeedback").setStatus(FeedbackStatus.DONE);

        assertThrows(IllegalArgumentException.class, () -> changeFeedbackStatusCommand.execute(params));
    }

    private Feedback initializeFeedback() {
        return new FeedbackImpl(7, "TitleFeedback", "Description", 5);
    }
}
