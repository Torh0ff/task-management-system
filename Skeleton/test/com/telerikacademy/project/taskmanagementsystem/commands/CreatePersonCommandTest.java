package com.telerikacademy.project.taskmanagementsystem.commands;

import com.telerikacademy.project.taskmanagementsystem.commands.contracts.Command;
import com.telerikacademy.project.taskmanagementsystem.core.TaskManagementRepositoryImpl;
import com.telerikacademy.project.taskmanagementsystem.core.contract.TaskManagementRepository;
import com.telerikacademy.project.taskmanagementsystem.exceptions.InvalidArgumentCountException;
import com.telerikacademy.project.taskmanagementsystem.models.team.contacts.Person;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static com.telerikacademy.project.taskmanagementsystem.TestUtilities.getList;
import static com.telerikacademy.project.taskmanagementsystem.models.team.PersonImplTest.*;
import static java.util.Arrays.asList;
import static org.junit.jupiter.api.Assertions.*;

class CreatePersonCommandTest {

    public static final int EXPECTED_PARAMETERS_COUNT = 1;

    private Command createPersonCommand;
    private TaskManagementRepository taskManagementRepository;

    @BeforeEach
    public void beforeEach(){
        taskManagementRepository = new TaskManagementRepositoryImpl();
        createPersonCommand = new CreatePersonCommand(taskManagementRepository);
    }

    @Test
    public void should_ThrowException_When_ArgumentCountDifferentThanExpected() {
        // Arrange
        List<String> params = getList(EXPECTED_PARAMETERS_COUNT - 1);

        // Act, Assert
        Assertions.assertThrows(InvalidArgumentCountException.class, () -> createPersonCommand.execute(params));
    }

    @Test
    public void should_ThrowException_When_UserExists_Already(){
        Person person = initializePerson();
        taskManagementRepository.createPerson(person.getName());

        List<String> params = List.of(VALID_NAME);

        assertThrows(IllegalArgumentException.class, () -> createPersonCommand.execute(params));
    }

    @Test
    public void should_ThrowException_When_UserName_Invalid(){
        List<String> params = List.of(INVALID_NAME_LONGER);

        assertThrows(IllegalArgumentException.class, () -> createPersonCommand.execute(params));
    }

    @Test
    public void should_Execute_When_Valid(){
        List<String> params = List.of("Iliyan");

        assertDoesNotThrow(() -> createPersonCommand.execute(params));
    }
}