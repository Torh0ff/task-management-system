package com.telerikacademy.project.taskmanagementsystem.commands;

import com.telerikacademy.project.taskmanagementsystem.commands.contracts.Command;
import com.telerikacademy.project.taskmanagementsystem.core.TaskManagementRepositoryImpl;
import com.telerikacademy.project.taskmanagementsystem.core.contract.TaskManagementRepository;
import com.telerikacademy.project.taskmanagementsystem.exceptions.InvalidArgumentCountException;
import com.telerikacademy.project.taskmanagementsystem.models.tasks.contracts.enums.TaskPriority;
import com.telerikacademy.project.taskmanagementsystem.models.tasks.contracts.enums.bug.Severity;
import com.telerikacademy.project.taskmanagementsystem.models.team.BoardImpl;
import com.telerikacademy.project.taskmanagementsystem.models.team.PersonImpl;
import com.telerikacademy.project.taskmanagementsystem.models.team.TeamImpl;
import com.telerikacademy.project.taskmanagementsystem.models.team.contacts.Person;
import com.telerikacademy.project.taskmanagementsystem.models.team.contacts.Team;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.stream.Collectors;

import static com.telerikacademy.project.taskmanagementsystem.TestUtilities.getList;
import static org.junit.jupiter.api.Assertions.*;

class UnAssignTaskCommandTest {

    private static final int EXPECTED_NUMBER_OF_ARGUMENTS = 2;
    private Command unAssignTaskCommand;
    private TaskManagementRepository taskManagementRepository;

    @BeforeEach
    public void beforeEach() {
        taskManagementRepository = new TaskManagementRepositoryImpl();
        unAssignTaskCommand = new UnAssignTaskCommand(taskManagementRepository);
    }

    @Test
    public void should_ThrowException_When_ArgumentCountDifferentThanExpected() {
        // Arrange
        List<String> params = getList(EXPECTED_NUMBER_OF_ARGUMENTS - 1);

        // Act, Assert
        assertThrows(InvalidArgumentCountException.class, () -> unAssignTaskCommand.execute(params));
    }

    @Test
    public void should_ThrowException_When_ID_is_Invalid() {
        // Arrange
        List<String> params = List.of("Content", "1");
        // Act, Assert
        assertThrows(IllegalArgumentException.class, () -> unAssignTaskCommand.execute(params));
    }

    @Test
    public void should_ThrowException_When_Person_is_not_found() {

        // Arrange
        List<String> params = List.of("1", "Content");
        // Act, Assert
        assertThrows(IllegalArgumentException.class, () -> unAssignTaskCommand.execute(params));

    }

    @Test
    public void should_ThrowException_When_Task_is_not_found() {

        // Arrange

        List<String> params = List.of("1", "Content");
        taskManagementRepository.createPerson("Content");
        // Act, Assert
        assertThrows(IllegalArgumentException.class, () -> unAssignTaskCommand.execute(params));

    }
    @Test
    public void should_Throw_Exception_When_Person_Is_Not_Assigned() {

        // Arrange
        taskManagementRepository.createBug("TitleBugName", "Description", Severity.CRITICAL,
                TaskPriority.LOW, new BoardImpl("BoardName"), List.of("Step 1", "Step 2"));
        List<String> params = List.of("1", "Content");

        Team team = new TeamImpl("TeamNameNew");
        Person person = new PersonImpl("Content");
        taskManagementRepository.createPerson(person.getName());
        taskManagementRepository.createTeam(team.getName());
        List<Team> teams = taskManagementRepository.getTeams()
                .stream().filter(t -> t.getName().equals(team.getName()))
                .collect(Collectors.toList());

        taskManagementRepository.findTaskById( taskManagementRepository.getTasks(), 1).addAssignee(person);
        // Act, Assert
        assertThrows(IllegalArgumentException.class, () -> unAssignTaskCommand.execute(params));

    }

    @Test
    public void should_Throw_Exception_When_Task_Is_Not_Assigned() {

        // Arrange
        taskManagementRepository.createBug("TitleBugName", "Description", Severity.CRITICAL,
                TaskPriority.LOW, new BoardImpl("BoardName"), List.of("Step 1", "Step 2"));
        List<String> params = List.of("1", "Content");

        Team team = new TeamImpl("TeamNameNew");
        Person person = new PersonImpl("Content");
        taskManagementRepository.createPerson(person.getName());
        taskManagementRepository.createTeam(team.getName());
        // Act, Assert
        assertThrows(IllegalArgumentException.class, () -> unAssignTaskCommand.execute(params));

    }

    @Test
    public void should_Execute_When_Valid() {

        // Arrange
        taskManagementRepository.createBug("TitleBugName", "Description", Severity.CRITICAL,
                TaskPriority.LOW, new BoardImpl("BoardName"), List.of("Step 1", "Step 2"));
        List<String> params = List.of("1", "Content");

        Team team = new TeamImpl("TeamNameNew");
        Person person = new PersonImpl("Content");
        taskManagementRepository.createPerson(person.getName());
        taskManagementRepository.createTeam(team.getName());
        List<Team> teams = taskManagementRepository.getTeams()
                .stream().filter(t -> t.getName().equals(team.getName()))
                .collect(Collectors.toList());

        taskManagementRepository.findTaskById( taskManagementRepository.getTasks(), 1).addAssignee(person);
        taskManagementRepository.findPersonByName(person.getName()).setAssigned(true);
        // Act, Assert
        assertDoesNotThrow(() -> unAssignTaskCommand.execute(params));

    }

}