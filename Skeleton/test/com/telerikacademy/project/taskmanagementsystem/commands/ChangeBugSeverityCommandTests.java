package com.telerikacademy.project.taskmanagementsystem.commands;

import com.telerikacademy.project.taskmanagementsystem.commands.contracts.Command;
import com.telerikacademy.project.taskmanagementsystem.core.TaskManagementRepositoryImpl;
import com.telerikacademy.project.taskmanagementsystem.core.contract.TaskManagementRepository;
import com.telerikacademy.project.taskmanagementsystem.exceptions.InvalidArgumentCountException;
import com.telerikacademy.project.taskmanagementsystem.models.tasks.BugImpl;
import com.telerikacademy.project.taskmanagementsystem.models.tasks.contracts.Bug;
import com.telerikacademy.project.taskmanagementsystem.models.tasks.contracts.Task;
import com.telerikacademy.project.taskmanagementsystem.models.tasks.contracts.enums.TaskPriority;
import com.telerikacademy.project.taskmanagementsystem.models.tasks.contracts.enums.bug.Severity;
import com.telerikacademy.project.taskmanagementsystem.models.tasks.contracts.enums.story.Size;
import com.telerikacademy.project.taskmanagementsystem.models.team.BoardImpl;
import com.telerikacademy.project.taskmanagementsystem.models.team.TeamImplTest;
import com.telerikacademy.project.taskmanagementsystem.models.team.contacts.Board;
import com.telerikacademy.project.taskmanagementsystem.models.team.contacts.Namable;
import com.telerikacademy.project.taskmanagementsystem.models.team.contacts.Team;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static com.telerikacademy.project.taskmanagementsystem.TestUtilities.getList;

import static org.junit.jupiter.api.Assertions.*;

public class ChangeBugSeverityCommandTests {

    private static final int EXPECTED_NUMBER_OF_ARGUMENTS_CHANGE = 2;
    private Command changeBugSeverityCommand;
    private TaskManagementRepository taskManagementRepository;

    @BeforeEach
    public void beforeEach() {
        taskManagementRepository = new TaskManagementRepositoryImpl();
        changeBugSeverityCommand = new ChangeBugSeverityCommand(taskManagementRepository);

    }

    @Test
    public void should_ThrowException_When_ArgumentCountDifferentThanExpected() {
        // Arrange
        List<String> params = getList(EXPECTED_NUMBER_OF_ARGUMENTS_CHANGE - 1);

        // Act, Assert
        assertThrows(InvalidArgumentCountException.class, () -> changeBugSeverityCommand.execute(params));
    }

    @Test
    public void should_ChangeBugSeverity_When_InputIs_Valid() {
        // Arrange
        List<String> params = List.of("1", Severity.CRITICAL.toString());
        // Act, Assert

        Bug bug = initializeBug();
        taskManagementRepository.createBug("titleBugTitle", "Description",
                Severity.MAJOR, TaskPriority.LOW, new BoardImpl("BugBoard"), List.of("step1", "step2"));

        assertAll(
                () -> assertDoesNotThrow(() -> changeBugSeverityCommand.execute(params)),
                () -> assertNotEquals(bug.getSeverity(), params.get(1))
        );

    }

    private Bug initializeBug() {
        return new BugImpl(7, "titleBugTitle", "Description", Severity.MAJOR, TaskPriority.LOW, List.of("step1", "step2"));

    }
}
