package com.telerikacademy.project.taskmanagementsystem.commands;

import com.telerikacademy.project.taskmanagementsystem.commands.contracts.Command;
import com.telerikacademy.project.taskmanagementsystem.core.TaskManagementRepositoryImpl;
import com.telerikacademy.project.taskmanagementsystem.core.contract.TaskManagementRepository;
import com.telerikacademy.project.taskmanagementsystem.exceptions.InvalidArgumentCountException;
import com.telerikacademy.project.taskmanagementsystem.models.tasks.contracts.enums.TaskPriority;
import com.telerikacademy.project.taskmanagementsystem.models.tasks.contracts.enums.bug.Severity;
import com.telerikacademy.project.taskmanagementsystem.models.team.BoardImpl;
import com.telerikacademy.project.taskmanagementsystem.models.team.PersonImpl;
import com.telerikacademy.project.taskmanagementsystem.models.team.TeamImpl;
import com.telerikacademy.project.taskmanagementsystem.models.team.contacts.Person;
import com.telerikacademy.project.taskmanagementsystem.models.team.contacts.Team;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.stream.Collectors;

import static com.telerikacademy.project.taskmanagementsystem.TestUtilities.getList;
import static org.junit.jupiter.api.Assertions.*;

public class AssignTaskCommandTest {
    private static final int ARGUMENTS_COUNT = 2;
    private Command assignTaskCommand;
    private TaskManagementRepository taskManagementRepository;

    @BeforeEach
    public void beforeEach() {
        taskManagementRepository = new TaskManagementRepositoryImpl();
        assignTaskCommand = new AssignTaskCommand(taskManagementRepository);
    }

    @Test
    public void should_ThrowException_When_ArgumentCountDifferentThanExpected() {
        // Arrange
        List<String> params = getList(ARGUMENTS_COUNT - 1);

        // Act, Assert
        assertThrows(InvalidArgumentCountException.class, () -> assignTaskCommand.execute(params));
    }

    @Test
    public void should_ThrowException_When_ID_is_Invalid() {
        // Arrange
        List<String> params = List.of("Content", "1");
        // Act, Assert
        assertThrows(IllegalArgumentException.class, () -> assignTaskCommand.execute(params));
    }

    @Test
    public void should_ThrowException_When_Person_is_not_found() {

        // Arrange
        List<String> params = List.of("1", "Content");
        // Act, Assert
        assertThrows(IllegalArgumentException.class, () -> assignTaskCommand.execute(params));

    }

    @Test
    public void should_ThrowException_When_Task_is_not_found() {

        // Arrange

        List<String> params = List.of("1", "Content");
        taskManagementRepository.createPerson("Content");
        // Act, Assert
        assertThrows(IllegalArgumentException.class, () -> assignTaskCommand.execute(params));

    }

    @Test
    public void should_Return_Correct_Team_Value_When_Member_is_valid() {

        // Arrange
        taskManagementRepository.createBug("TitleBugName", "Description", Severity.CRITICAL,
                TaskPriority.LOW, new BoardImpl("BoardName"), List.of("Step 1", "Step 2"));
        List<String> params = List.of("1", "Content");

        Team team = new TeamImpl("TeamNameNew");
        Person person = new PersonImpl("Content");
        taskManagementRepository.createPerson(person.getName());
        taskManagementRepository.createTeam(team.getName());
        List<Team> teams = taskManagementRepository.getTeams()
                .stream().filter(t -> t.getName().equals(team.getName()))
                        .collect(Collectors.toList());

        for (Team team1 : teams) {
            team1.addMember(person);
            taskManagementRepository.createTeam(team1.getName());
        }
        // Act, Assert
        assertThrows(IllegalArgumentException.class, () -> assignTaskCommand.execute(params));

    }

    @Test
    public void should_Return_Correct_Board_Value_When_Member_is_valid() {

        // Arrange

        List<String> params = List.of("1", "Content");
        taskManagementRepository.createPerson("Content");
        Team team = new TeamImpl("TeamNameNew");
        Person person = new PersonImpl("Content");
        team.addMember(person);
        taskManagementRepository.createPerson(person.getName());
        taskManagementRepository.createTeam(team.getName());
        taskManagementRepository.createBug("TitleBugName", "Description", Severity.CRITICAL,
                TaskPriority.LOW, new BoardImpl("BoardName"), List.of("Step 1", "Step 2"));
        // Act, Assert
        assertThrows(IllegalArgumentException.class, () -> assignTaskCommand.execute(params));

    }

}