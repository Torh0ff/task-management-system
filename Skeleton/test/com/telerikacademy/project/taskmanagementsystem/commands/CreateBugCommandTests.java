package com.telerikacademy.project.taskmanagementsystem.commands;

import com.telerikacademy.project.taskmanagementsystem.commands.contracts.Command;
import com.telerikacademy.project.taskmanagementsystem.core.TaskManagementRepositoryImpl;
import com.telerikacademy.project.taskmanagementsystem.core.contract.TaskManagementRepository;
import com.telerikacademy.project.taskmanagementsystem.exceptions.InvalidArgumentCountException;
import com.telerikacademy.project.taskmanagementsystem.models.tasks.contracts.enums.TaskPriority;
import com.telerikacademy.project.taskmanagementsystem.models.tasks.contracts.enums.bug.Severity;
import com.telerikacademy.project.taskmanagementsystem.models.team.BoardImpl;
import com.telerikacademy.project.taskmanagementsystem.models.team.TeamImplTest;
import com.telerikacademy.project.taskmanagementsystem.models.team.contacts.Board;
import com.telerikacademy.project.taskmanagementsystem.models.team.contacts.Team;
//import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static com.telerikacademy.project.taskmanagementsystem.TestUtilities.getList;

import static org.junit.jupiter.api.Assertions.*;

public class CreateBugCommandTests {

    private static final int ARGUMENTS_COUNT = 7;
    private Command createBugCommand;
    private TaskManagementRepository taskManagementRepository;

    @BeforeEach
    public void beforeEach() {
        taskManagementRepository = new TaskManagementRepositoryImpl();
        createBugCommand = new CreateBugCommand(taskManagementRepository);
    }

    @Test
    public void should_ThrowException_When_ArgumentCountDifferentThanExpected() {
        // Arrange
        List<String> params = getList(ARGUMENTS_COUNT - 6);

        // Act, Assert
        assertThrows(InvalidArgumentCountException.class, () -> createBugCommand.execute(params));
    }

    @Test
    public void should_ThrowException_When_FindBoardByNameDoesNotExist() {
        // Arrange
        Board board = new BoardImpl("Abraca");
        Team team = TeamImplTest.initializeTeam();
        taskManagementRepository.createTeam(team.getName());
        List<String> params = List.of("Name", "Description",
                "steps", Severity.CRITICAL.toString(), TaskPriority.LOW.toString(), board.getName(), team.getName());

        // Act, Assert

        assertThrows(IllegalArgumentException.class, () -> createBugCommand.execute(params));

    }




    @Test
    public void should_ThrowException_When_FindTeamByNameDoesNotExist() {
        // Arrange
        Board board = new BoardImpl("Abraca");
        taskManagementRepository.createBoard(board.getName());
        Team team = TeamImplTest.initializeTeam();

        List<String> params = List.of("Title", "Description",
                "steps", Severity.CRITICAL.toString(), TaskPriority.LOW.toString(), board.getName(), team.getName());

        // Act, Assert

        assertThrows(IllegalArgumentException.class, () -> createBugCommand.execute(params));

    }


    @Test
    public void should_CreateBug_When_Input_Is_Valid() {
        Board board = new BoardImpl("Abraca");
        taskManagementRepository.createBoard(board.getName());
        Team team = TeamImplTest.initializeTeam();
        taskManagementRepository.createTeam(team.getName());
        List<String> params = List.of( "Description",
                "stepsNameOne", Severity.CRITICAL.toString(), TaskPriority.LOW.toString(),
                board.getName(), team.getName(), "TitleOfBug");
        // Act, Assert

        assertAll(
               () -> assertDoesNotThrow(() -> createBugCommand.execute(params)),
                () -> assertEquals(1, taskManagementRepository.getBugs().size())
        );

    }

    @Test
    public void should_ThrowException_When_Bug_Exists() {
        // Arrange
        Board board = new BoardImpl("Abraca");
        taskManagementRepository.createBoard(board.getName());
        Team team = TeamImplTest.initializeTeam();


        taskManagementRepository.createBug("TitleNameTest", "Description", Severity.CRITICAL
                , TaskPriority.LOW, board, List.of("Step1 "));

        List<String> params = List.of("Step 1", "TitleNameTest", "Description",
                 Severity.CRITICAL.toString(), TaskPriority.LOW.toString(), board.getName(), team.getName());
        // Act, Assert

        assertThrows(IllegalArgumentException.class, () -> createBugCommand.execute(params));

    }



}
