package com.telerikacademy.project.taskmanagementsystem.commands;

import com.telerikacademy.project.taskmanagementsystem.commands.contracts.Command;
import com.telerikacademy.project.taskmanagementsystem.core.TaskManagementRepositoryImpl;
import com.telerikacademy.project.taskmanagementsystem.core.contract.TaskManagementRepository;
import com.telerikacademy.project.taskmanagementsystem.exceptions.InvalidArgumentCountException;
import com.telerikacademy.project.taskmanagementsystem.models.tasks.StoryImpl;
import com.telerikacademy.project.taskmanagementsystem.models.tasks.contracts.Story;
import com.telerikacademy.project.taskmanagementsystem.models.tasks.contracts.enums.TaskPriority;
import com.telerikacademy.project.taskmanagementsystem.models.tasks.contracts.enums.TaskStatus;
import com.telerikacademy.project.taskmanagementsystem.models.tasks.contracts.enums.story.Size;
import com.telerikacademy.project.taskmanagementsystem.models.team.BoardImpl;
import com.telerikacademy.project.taskmanagementsystem.models.team.PersonImpl;
import com.telerikacademy.project.taskmanagementsystem.models.team.PersonImplTest;
import com.telerikacademy.project.taskmanagementsystem.models.team.TeamImplTest;
import com.telerikacademy.project.taskmanagementsystem.models.team.contacts.Person;
import com.telerikacademy.project.taskmanagementsystem.models.team.contacts.Team;
import com.telerikacademy.project.taskmanagementsystem.utils.ValidationHelpers;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Collections;
import java.util.List;

import static com.telerikacademy.project.taskmanagementsystem.TestUtilities.getList;
import static com.telerikacademy.project.taskmanagementsystem.models.team.PersonImplTest.*;
import static com.telerikacademy.project.taskmanagementsystem.models.team.TeamImplTest.initializeTeam;
import static org.junit.jupiter.api.Assertions.*;

public class ShowAllPeopleCommandTests {


    private static final int EXPECTED_PARAMETERS_COUNT = 0;

    private Command showAllPeopleCommand;

    private TaskManagementRepository taskManagementRepository;

    @BeforeEach
    public void beforeEach() {
        taskManagementRepository = new TaskManagementRepositoryImpl();
        showAllPeopleCommand = new ShowAllPeopleCommand(taskManagementRepository);
    }


    @Test
    public void should_ThrowException_When_ParametersCountDifferentThanExpected() {
        // Arrange
        List<String> params = getList(EXPECTED_PARAMETERS_COUNT + 1);

        // Act, Assert
        Assertions.assertThrows(InvalidArgumentCountException.class, () -> showAllPeopleCommand.execute(params));
    }

    @Test
    public void should_ThrowException_When_People_List_Empty() {
        // Arrange
        List<String> params = List.of();

        // Act, Assert
        Assertions.assertThrows(IllegalArgumentException.class, () -> showAllPeopleCommand.execute(params));
    }

    @Test
    public void should_ShowAllPeople_When_InputIsValid() {
        //Arrange

        taskManagementRepository.createPerson("Person");
        List<String> arguments = List.of();

        //Act, Assert
        assertAll(
                () -> assertDoesNotThrow(() -> showAllPeopleCommand.execute(arguments)),
                () -> assertEquals(String.format("People----%n   --1. Person%n=========="), showAllPeopleCommand.execute(arguments))
        );
    }
}