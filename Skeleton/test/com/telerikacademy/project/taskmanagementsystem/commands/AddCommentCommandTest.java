package com.telerikacademy.project.taskmanagementsystem.commands;

import com.telerikacademy.project.taskmanagementsystem.commands.contracts.Command;
import com.telerikacademy.project.taskmanagementsystem.core.TaskManagementRepositoryImpl;
import com.telerikacademy.project.taskmanagementsystem.core.contract.TaskManagementRepository;
import com.telerikacademy.project.taskmanagementsystem.exceptions.InvalidArgumentCountException;
import com.telerikacademy.project.taskmanagementsystem.models.tasks.BugImpl;
import com.telerikacademy.project.taskmanagementsystem.models.tasks.contracts.Bug;
import com.telerikacademy.project.taskmanagementsystem.models.tasks.contracts.Task;
import com.telerikacademy.project.taskmanagementsystem.models.tasks.contracts.enums.TaskPriority;
import com.telerikacademy.project.taskmanagementsystem.models.tasks.contracts.enums.bug.Severity;
import com.telerikacademy.project.taskmanagementsystem.models.team.BoardImpl;
import com.telerikacademy.project.taskmanagementsystem.models.team.PersonImpl;
import com.telerikacademy.project.taskmanagementsystem.models.team.contacts.Person;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static com.telerikacademy.project.taskmanagementsystem.TestUtilities.getList;
import static org.junit.jupiter.api.Assertions.*;

public class AddCommentCommandTest {

    private static final int ARGUMENTS_COUNT = 7;
    private Command addCommentCommand;
    private TaskManagementRepository taskManagementRepository;

    @BeforeEach
    public void beforeEach() {
        taskManagementRepository = new TaskManagementRepositoryImpl();
        addCommentCommand = new AddCommentCommand(taskManagementRepository);
    }


    @Test
    public void should_Throw_Exception_When_Invalid_Arguments_Count(){
        List<String> params = getList(ARGUMENTS_COUNT - 1);

        assertThrows(InvalidArgumentCountException.class, () -> addCommentCommand.execute(params));
    }

    @Test
    public void should_AddComment_When_Input_Is_Valid() {
        Person person = new PersonImpl("Person");
        taskManagementRepository.createPerson(person.getName());
        taskManagementRepository.createBug("TitleBugOne", "Description", Severity.CRITICAL,
                TaskPriority.LOW, new BoardImpl("BoardImpl"), List.of("StepOne", "StepTwo"));


        Task bug = taskManagementRepository.findTaskById(taskManagementRepository.getBugs(), 1);
        List<String> params = List.of("Content", person.getName(), "1");

        assertAll(
                () -> assertDoesNotThrow(() -> addCommentCommand.execute(params)),
                () -> assertEquals(1, bug.getComments().size()));

    }

}