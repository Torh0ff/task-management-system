package com.telerikacademy.project.taskmanagementsystem.commands.listings;

import com.telerikacademy.project.taskmanagementsystem.commands.contracts.Command;
import com.telerikacademy.project.taskmanagementsystem.core.TaskManagementRepositoryImpl;
import com.telerikacademy.project.taskmanagementsystem.core.contract.TaskManagementRepository;
import com.telerikacademy.project.taskmanagementsystem.exceptions.InvalidArgumentCountException;
import com.telerikacademy.project.taskmanagementsystem.models.tasks.contracts.enums.TaskPriority;
import com.telerikacademy.project.taskmanagementsystem.models.team.BoardImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static com.telerikacademy.project.taskmanagementsystem.TestUtilities.getList;
import static com.telerikacademy.project.taskmanagementsystem.models.tasks.BugImplTest.*;
import static com.telerikacademy.project.taskmanagementsystem.models.tasks.BugImplTest.VALID_STEPS;
import static com.telerikacademy.project.taskmanagementsystem.models.tasks.StoryImplTest.VALID_SIZE;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class FilterStoriesByPriorityCommandTest {

    private static final int ARGUMENTS_COUNT = 1;

    private static final TaskPriority VALID_PRIORITY = TaskPriority.LOW;
    private Command filterStoriesByPriority;
    private TaskManagementRepository taskManagementRepository;



    @BeforeEach
    public void beforeEach(){
        taskManagementRepository = new TaskManagementRepositoryImpl();
        filterStoriesByPriority = new FilterStoriesByPriorityCommand(taskManagementRepository);
    }

    @Test
    public void should_ThrowException_When_Arguments_Invalid(){
        List<String> params = getList(ARGUMENTS_COUNT - 1);

        assertThrows(InvalidArgumentCountException.class, () -> filterStoriesByPriority.execute(params));
    }

    @Test
    public void should_ThrowException_When_List_Empty(){

        List<String> params = List.of(VALID_PRIORITY.toString());

        assertThrows(IllegalArgumentException.class, () -> filterStoriesByPriority.execute(params));
    }


    @Test
    public void should_Print_When_All_Is_Valid() {

        taskManagementRepository.createStory(VALID_TITLE, VALID_DESCR, VALID_SIZE
                , VALID_PRIORITY, new BoardImpl("BoardImpl"));

        List<String> params = List.of(VALID_PRIORITY.toString());

        assertDoesNotThrow(() -> filterStoriesByPriority.execute(params));

    }

}
