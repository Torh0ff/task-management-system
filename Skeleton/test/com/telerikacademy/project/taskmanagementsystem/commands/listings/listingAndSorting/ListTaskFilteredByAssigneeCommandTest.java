package com.telerikacademy.project.taskmanagementsystem.commands.listings.listingAndSorting;

import com.telerikacademy.project.taskmanagementsystem.commands.contracts.Command;
import com.telerikacademy.project.taskmanagementsystem.core.TaskManagementRepositoryImpl;
import com.telerikacademy.project.taskmanagementsystem.core.contract.TaskManagementRepository;
import com.telerikacademy.project.taskmanagementsystem.exceptions.InvalidArgumentCountException;
import com.telerikacademy.project.taskmanagementsystem.models.tasks.contracts.Bug;
import com.telerikacademy.project.taskmanagementsystem.models.tasks.contracts.enums.TaskPriority;
import com.telerikacademy.project.taskmanagementsystem.models.tasks.contracts.enums.TaskStatus;
import com.telerikacademy.project.taskmanagementsystem.models.tasks.contracts.enums.bug.BugStatus;
import com.telerikacademy.project.taskmanagementsystem.models.tasks.contracts.enums.bug.Severity;
import com.telerikacademy.project.taskmanagementsystem.models.team.BoardImpl;
import com.telerikacademy.project.taskmanagementsystem.models.team.PersonImpl;
import com.telerikacademy.project.taskmanagementsystem.models.team.contacts.Person;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static com.telerikacademy.project.taskmanagementsystem.TestUtilities.getList;
import static org.junit.jupiter.api.Assertions.*;

public class ListTaskFilteredByAssigneeCommandTest {
    private static final int ARGUMENTS_COUNT_TEST = 2;

    private Command listAllTasksFilteredByAssignee;
    private TaskManagementRepository taskManagementRepository;

    @BeforeEach
    public void beforeEach(){
        taskManagementRepository = new TaskManagementRepositoryImpl();
        listAllTasksFilteredByAssignee = new
                ListTaskFilteredByAssigneeCommand(taskManagementRepository);
    }

    @Test
    public void should_ThrowException_When_Arguments_Invalid(){
        List<String> params = getList(ARGUMENTS_COUNT_TEST - 1);

        assertThrows(InvalidArgumentCountException.class, () -> listAllTasksFilteredByAssignee.execute(params));
    }

    @Test
    public void should_ThrowException_Task_Type_Invalid(){
        List<String> params = List.of("Invalid", "Aleks");

        assertThrows(IllegalArgumentException.class, () -> listAllTasksFilteredByAssignee.execute(params));
    }


    @Test
    public void should_Throw_Exception_When_List_Is_Empty(){
        List<String> params = List.of("Bug", "Aleks");

        assertThrows(IllegalArgumentException.class, () ->
                listAllTasksFilteredByAssignee.execute(params));
    }

    @Test
    public void should_Execute_When_All_Is_Valid(){
        Person person = new PersonImpl("Aleks");
        List<String> params = List.of("Bug", person.getName());
        taskManagementRepository.createPerson(person.getName());
        taskManagementRepository.createBug("NewBugTitleTest", "Description", Severity.CRITICAL,
                TaskPriority.LOW, new BoardImpl("nameee"), List.of("Step 1"));


        for (Bug task : taskManagementRepository.getBugs()) {
            if (task.getTitle().equals("NewBugTitleTest")){
               task.addAssignee(person);
            }
        }

        assertDoesNotThrow(() -> listAllTasksFilteredByAssignee.execute(params));

    }
}