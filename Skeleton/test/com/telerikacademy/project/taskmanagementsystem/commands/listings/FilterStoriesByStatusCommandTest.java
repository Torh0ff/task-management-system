package com.telerikacademy.project.taskmanagementsystem.commands.listings;

import com.telerikacademy.project.taskmanagementsystem.commands.contracts.Command;
import com.telerikacademy.project.taskmanagementsystem.core.TaskManagementRepositoryImpl;
import com.telerikacademy.project.taskmanagementsystem.core.contract.TaskManagementRepository;
import com.telerikacademy.project.taskmanagementsystem.exceptions.InvalidArgumentCountException;
import com.telerikacademy.project.taskmanagementsystem.models.tasks.contracts.enums.story.StoryStatus;
import com.telerikacademy.project.taskmanagementsystem.models.team.BoardImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static com.telerikacademy.project.taskmanagementsystem.TestUtilities.getList;
import static com.telerikacademy.project.taskmanagementsystem.models.tasks.BugImplTest.*;
import static com.telerikacademy.project.taskmanagementsystem.models.tasks.StoryImplTest.VALID_SIZE;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class FilterStoriesByStatusCommandTest {

    private static final int ARGUMENTS_COUNT = 1;

    private static final StoryStatus VALID_STATUS = StoryStatus.NOT_DONE;
    private Command filterStoriesByStatus;
    private TaskManagementRepository taskManagementRepository;



    @BeforeEach
    public void beforeEach(){
        taskManagementRepository = new TaskManagementRepositoryImpl();
        filterStoriesByStatus = new FilterStoriesByStatusCommand(taskManagementRepository);
    }

    @Test
    public void should_ThrowException_When_Arguments_Invalid(){
        List<String> params = getList(ARGUMENTS_COUNT - 1);

        assertThrows(InvalidArgumentCountException.class, () -> filterStoriesByStatus.execute(params));
    }

    @Test
    public void should_ThrowException_When_List_Empty(){

        List<String> params = List.of(VALID_STATUS.toString());

        assertThrows(IllegalArgumentException.class, () -> filterStoriesByStatus.execute(params));
    }


    @Test
    public void should_Print_When_All_Is_Valid() {

        taskManagementRepository.createStory(VALID_TITLE, VALID_DESCR, VALID_SIZE
                , VALID_PRIORITY, new BoardImpl("BoardImpl"));

        List<String> params = List.of(VALID_STATUS.toString());

        assertDoesNotThrow(() -> filterStoriesByStatus.execute(params));

    }

}
