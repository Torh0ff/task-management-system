package com.telerikacademy.project.taskmanagementsystem.commands.listings.listingAndSorting;

import com.telerikacademy.project.taskmanagementsystem.commands.contracts.Command;
import com.telerikacademy.project.taskmanagementsystem.core.TaskManagementRepositoryImpl;
import com.telerikacademy.project.taskmanagementsystem.core.contract.TaskManagementRepository;
import com.telerikacademy.project.taskmanagementsystem.exceptions.InvalidArgumentCountException;
import com.telerikacademy.project.taskmanagementsystem.models.tasks.contracts.enums.TaskPriority;
import com.telerikacademy.project.taskmanagementsystem.models.tasks.contracts.enums.bug.Severity;
import com.telerikacademy.project.taskmanagementsystem.models.tasks.contracts.enums.story.Size;
import com.telerikacademy.project.taskmanagementsystem.models.team.BoardImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static com.telerikacademy.project.taskmanagementsystem.TestUtilities.getList;
import static org.junit.jupiter.api.Assertions.*;

public class ListTasksSortedByCommandTest {
    private static final int ARGUMENTS_COUNT_TEST = 3;

    private Command listAllTasksSortedByStatus;
    private TaskManagementRepository taskManagementRepository;

    @BeforeEach
    public void beforeEach(){
        taskManagementRepository = new TaskManagementRepositoryImpl();
        listAllTasksSortedByStatus = new
                ListTasksSortedByCommand(taskManagementRepository);
    }

    @Test
    public void should_ThrowException_When_Arguments_Invalid(){
        List<String> params = getList(ARGUMENTS_COUNT_TEST - 1);

        assertThrows(InvalidArgumentCountException.class, () -> listAllTasksSortedByStatus.execute(params));
    }

    @Test
    public void should_ThrowException_Task_Type_Invalid(){
        List<String> params = List.of("Invalid", "title", "Title");

        assertThrows(IllegalArgumentException.class, () -> listAllTasksSortedByStatus.execute(params));
    }

    @Test
    public void should_ThrowException_When_Title_Tasks_Empty(){
        List<String> params = List.of("Bug", "title", "Title");

        assertThrows(IllegalArgumentException.class, () -> listAllTasksSortedByStatus.execute(params));
    }

    @Test
    public void should_Execute_When_Title_Tasks_Not_Empty(){
        List<String> params = List.of("Bug", "title", "Title");

        taskManagementRepository.createBug("TitleTestString", "Description", Severity.CRITICAL,
                TaskPriority.LOW, new BoardImpl("nameee"), List.of("Step 1"));
        assertDoesNotThrow( () -> listAllTasksSortedByStatus.execute(params));
    }

    @Test
    public void should_Execute_When_Priority_Bug_Tasks_Not_Empty(){
        List<String> params = List.of("Bug", "priority", "LOW");

        taskManagementRepository.createBug("TitleTestString", "Description", Severity.CRITICAL,
                TaskPriority.LOW, new BoardImpl("nameee"), List.of("Step 1"));
        assertDoesNotThrow( () -> listAllTasksSortedByStatus.execute(params));
    }

    @Test
    public void should_ThrowException_When_Priority_Story_Tasks_Empty(){
        List<String> params = List.of("Story", "priority", "LOW");

        assertThrows(IllegalArgumentException.class, () -> listAllTasksSortedByStatus.execute(params));
    }


    @Test
    public void should_Execute_When_Priority_Story_Tasks_Not_Empty(){
        List<String> params = List.of("Story", "priority", "LOW");

       taskManagementRepository.createStory("TitleNameTest", "Description", Size.LARGE, TaskPriority.LOW,
               new BoardImpl("newBoard"));
        assertDoesNotThrow( () -> listAllTasksSortedByStatus.execute(params));
    }

    @Test
    public void should_ThrowException_When_Priority_No_Sorting_Option(){
        List<String> params = List.of("Feedback", "priority", "LOW");

        assertThrows(IllegalArgumentException.class, () -> listAllTasksSortedByStatus.execute(params));
    }

    @Test
    public void should_ThrowException_When_Severity_No_Sorting_Option(){
        List<String> params = List.of("Feedback", "severity", "MINOR");

        assertThrows(IllegalArgumentException.class, () -> listAllTasksSortedByStatus.execute(params));
    }

    @Test
    public void should_ThrowException_When_Severity_Bug_List_Empty(){
        List<String> params = List.of("Bug", "severity", "MINOR");

        assertThrows(IllegalArgumentException.class, () -> listAllTasksSortedByStatus.execute(params));
    }

    @Test
    public void should_ThrowException_When_Size_No_Sorting_Option(){
        List<String> params = List.of("Feedback", "size", "SMALL");

        assertThrows(IllegalArgumentException.class, () -> listAllTasksSortedByStatus.execute(params));
    }

    @Test
    public void should_ThrowException_When_Size_Story_List_Empty(){
        List<String> params = List.of("Story", "size", "SMALL");

        assertThrows(IllegalArgumentException.class, () -> listAllTasksSortedByStatus.execute(params));
    }

    @Test
    public void should_Execute_When_Priority_Size_Story_List_Not_Empty(){
        List<String> params = List.of("Story", "size", "SMALL");

        taskManagementRepository.createStory("TitleTestName", "Description",
                Size.SMALL, TaskPriority.LOW, new BoardImpl("newName"));
        assertDoesNotThrow( () -> listAllTasksSortedByStatus.execute(params));
    }

    @Test
    public void should_ThrowException_When_Rating_No_Sorting_Option(){
        List<String> params = List.of("Bug", "rating", "1");

        assertThrows(IllegalArgumentException.class, () -> listAllTasksSortedByStatus.execute(params));
    }

    @Test
    public void should_ThrowException_When_Rating_Feedback_List_Empty(){
        List<String> params = List.of("Feedback", "rating", "1");

        assertThrows(IllegalArgumentException.class, () -> listAllTasksSortedByStatus.execute(params));
    }

    @Test
    public void should_Execute_When_Rating_Feedback_List_Not_Empty(){
        List<String> params = List.of("Feedback", "rating", "1");

        taskManagementRepository.createFeedback("TitleTestName", "Description", 2,
                new BoardImpl("BoardName"));
        assertDoesNotThrow(() -> listAllTasksSortedByStatus.execute(params));
    }

    @Test
    public void should_ThrowException_When_No_Sorting_Option(){
        List<String> params = List.of("Bug", "status", "1");

        assertThrows(IllegalArgumentException.class, () -> listAllTasksSortedByStatus.execute(params));
    }
}