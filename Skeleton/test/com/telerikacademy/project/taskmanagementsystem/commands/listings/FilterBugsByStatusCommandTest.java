package com.telerikacademy.project.taskmanagementsystem.commands.listings;

import com.telerikacademy.project.taskmanagementsystem.commands.contracts.Command;
import com.telerikacademy.project.taskmanagementsystem.core.TaskManagementRepositoryImpl;
import com.telerikacademy.project.taskmanagementsystem.core.contract.TaskManagementRepository;
import com.telerikacademy.project.taskmanagementsystem.exceptions.InvalidArgumentCountException;
import com.telerikacademy.project.taskmanagementsystem.models.tasks.contracts.enums.TaskPriority;
import com.telerikacademy.project.taskmanagementsystem.models.tasks.contracts.enums.TaskStatus;
import com.telerikacademy.project.taskmanagementsystem.models.tasks.contracts.enums.bug.BugStatus;
import com.telerikacademy.project.taskmanagementsystem.models.team.BoardImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static com.telerikacademy.project.taskmanagementsystem.TestUtilities.getList;
import static com.telerikacademy.project.taskmanagementsystem.models.tasks.BugImplTest.*;
import static com.telerikacademy.project.taskmanagementsystem.models.tasks.BugImplTest.VALID_STEPS;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class FilterBugsByStatusCommandTest {

    private static final int ARGUMENTS_COUNT = 1;

    private static final BugStatus VALID_STATUS = BugStatus.ACTIVE;
    private Command filterBugsByStatus;
    private TaskManagementRepository taskManagementRepository;



    @BeforeEach
    public void beforeEach(){
        taskManagementRepository = new TaskManagementRepositoryImpl();
        filterBugsByStatus = new FilterBugsByStatusCommand(taskManagementRepository);
    }

    @Test
    public void should_ThrowException_When_Arguments_Invalid(){
        List<String> params = getList(ARGUMENTS_COUNT - 1);

        assertThrows(InvalidArgumentCountException.class, () -> filterBugsByStatus.execute(params));
    }

    @Test
    public void should_ThrowException_When_List_Empty(){

        List<String> params = List.of(VALID_STATUS.toString());

        assertThrows(IllegalArgumentException.class, () -> filterBugsByStatus.execute(params));
    }


    @Test
    public void should_Print_When_All_Is_Valid() {

        taskManagementRepository.createBug(VALID_TITLE, VALID_DESCR, VALID_SEVERITY
                , VALID_PRIORITY, new BoardImpl("BoardImpl"), VALID_STEPS);

        List<String> params = List.of(VALID_STATUS.toString());

        assertDoesNotThrow(() -> filterBugsByStatus.execute(params));

    }

}
