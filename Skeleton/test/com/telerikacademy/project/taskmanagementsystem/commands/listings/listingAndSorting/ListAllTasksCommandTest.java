package com.telerikacademy.project.taskmanagementsystem.commands.listings.listingAndSorting;

import com.telerikacademy.project.taskmanagementsystem.commands.contracts.Command;
import com.telerikacademy.project.taskmanagementsystem.core.TaskManagementRepositoryImpl;
import com.telerikacademy.project.taskmanagementsystem.core.contract.TaskManagementRepository;
import com.telerikacademy.project.taskmanagementsystem.exceptions.InvalidArgumentCountException;
import com.telerikacademy.project.taskmanagementsystem.models.tasks.contracts.enums.TaskPriority;
import com.telerikacademy.project.taskmanagementsystem.models.tasks.contracts.enums.bug.Severity;
import com.telerikacademy.project.taskmanagementsystem.models.team.BoardImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static com.telerikacademy.project.taskmanagementsystem.TestUtilities.getList;
import static org.junit.jupiter.api.Assertions.*;

public class ListAllTasksCommandTest {
    private static final int ARGUMENTS_COUNT = 1;

    private Command listAllTasksCommand;
    private TaskManagementRepository taskManagementRepository;

    @BeforeEach
    public void beforeEach(){
        taskManagementRepository = new TaskManagementRepositoryImpl();
        listAllTasksCommand = new ListAllTasksCommand(taskManagementRepository);
    }

    @Test
    public void should_ThrowException_When_Arguments_Invalid(){
        List<String> params = getList(ARGUMENTS_COUNT - 1);

        assertThrows(InvalidArgumentCountException.class, () -> listAllTasksCommand.execute(params));
    }

    @Test
    public void should_ThrowException_When_List_Empty(){
        List<String> params = getList(ARGUMENTS_COUNT);

        assertThrows(IllegalArgumentException.class, () -> listAllTasksCommand.execute(params));
    }

    @Test
    public void should_Print_When_All_Is_Valid(){
        List<String> params = List.of("xxxxx");


        taskManagementRepository.createBug("xxxxxxxxxxxxx", "Description", Severity.CRITICAL
                , TaskPriority.LOW, new BoardImpl("BoardImpl"), List.of("Step 1"));

        assertDoesNotThrow(() -> listAllTasksCommand.execute(params));
    }



}