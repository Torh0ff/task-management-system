package com.telerikacademy.project.taskmanagementsystem.commands.listings.listingAndSorting;

import com.telerikacademy.project.taskmanagementsystem.commands.contracts.Command;
import com.telerikacademy.project.taskmanagementsystem.core.TaskManagementRepositoryImpl;
import com.telerikacademy.project.taskmanagementsystem.core.contract.TaskManagementRepository;
import com.telerikacademy.project.taskmanagementsystem.exceptions.InvalidArgumentCountException;
import com.telerikacademy.project.taskmanagementsystem.models.tasks.BugImpl;
import com.telerikacademy.project.taskmanagementsystem.models.tasks.contracts.Bug;
import com.telerikacademy.project.taskmanagementsystem.models.tasks.contracts.Task;
import com.telerikacademy.project.taskmanagementsystem.models.tasks.contracts.enums.TaskPriority;
import com.telerikacademy.project.taskmanagementsystem.models.tasks.contracts.enums.bug.Severity;
import com.telerikacademy.project.taskmanagementsystem.models.team.BoardImpl;
import com.telerikacademy.project.taskmanagementsystem.models.team.PersonImpl;
import com.telerikacademy.project.taskmanagementsystem.models.team.contacts.Person;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static com.telerikacademy.project.taskmanagementsystem.TestUtilities.getList;
import static org.junit.jupiter.api.Assertions.*;

class ListAllTasksWithAndFilteredByAssigneeCommandTest {
    private static final int ARGUMENTS_COUNT = 2;

    private Command listAllTasksWithAndFilteredByAssignee;
    private TaskManagementRepository taskManagementRepository;

    @BeforeEach
    public void beforeEach(){
        taskManagementRepository = new TaskManagementRepositoryImpl();
        listAllTasksWithAndFilteredByAssignee = new
                ListAllTasksWithAndFilteredByAssigneeCommand(taskManagementRepository);
    }

    @Test
    public void should_ThrowException_When_Arguments_Invalid(){
        List<String> params = getList(ARGUMENTS_COUNT - 1);

        assertThrows(InvalidArgumentCountException.class, () -> listAllTasksWithAndFilteredByAssignee.execute(params));
    }

    @Test
    public void should_ThrowException_When_List_Empty(){
        List<String> params = List.of("xxxxx", "xxxxx");
        Person person = new PersonImpl("xxxxx");
        taskManagementRepository.createPerson(person.getName());

        assertThrows(IllegalArgumentException.class, () -> listAllTasksWithAndFilteredByAssignee.execute(params));
    }

    @Test
    public void should_Print_When_All_Is_Valid(){
        List<String> params = List.of("xxxxx", "xxxxx");



        Person person = new PersonImpl("xxxxx");
        taskManagementRepository.createPerson(person.getName());
        Task bug = new BugImpl(1, "xxxxxxxxxxxxxx", "Description", Severity.CRITICAL
                , TaskPriority.LOW, List.of("Step 1"));
        taskManagementRepository.createBug("xxxxxxxxxxxxxx", "Description", Severity.CRITICAL
                , TaskPriority.LOW, new BoardImpl("xxxxx"), List.of("Step 1"));
        for (Task task : taskManagementRepository.getTasks()) {
            if (task.getTitle().equals("xxxxxxxxxxxxxx")){
                task.addAssignee(person);
            }
        }
        for (Person taskManagementRepositoryPerson : taskManagementRepository.getPeople()) {
            if (taskManagementRepositoryPerson.getName().equals("xxxxx")){
                taskManagementRepositoryPerson.addTask(bug);
            }
        }



        assertDoesNotThrow(() -> listAllTasksWithAndFilteredByAssignee.execute(params));
    }
}