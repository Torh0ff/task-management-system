package com.telerikacademy.project.taskmanagementsystem.commands.listings;

import com.telerikacademy.project.taskmanagementsystem.commands.contracts.Command;
import com.telerikacademy.project.taskmanagementsystem.core.TaskManagementRepositoryImpl;
import com.telerikacademy.project.taskmanagementsystem.core.contract.TaskManagementRepository;
import com.telerikacademy.project.taskmanagementsystem.exceptions.InvalidArgumentCountException;
import com.telerikacademy.project.taskmanagementsystem.models.team.BoardImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static com.telerikacademy.project.taskmanagementsystem.TestUtilities.getList;
import static com.telerikacademy.project.taskmanagementsystem.models.tasks.BugImplTest.VALID_DESCR;
import static com.telerikacademy.project.taskmanagementsystem.models.tasks.BugImplTest.VALID_TITLE;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class FilterFeedbackByRatingCommandTest {

    private static final int ARGUMENTS_COUNT = 1;

    private static final int VALID_RATING = 1;
    private Command filterFeedbackByRating;
    private TaskManagementRepository taskManagementRepository;



    @BeforeEach
    public void beforeEach(){
        taskManagementRepository = new TaskManagementRepositoryImpl();
        filterFeedbackByRating = new FilterFeedbacksByRatingCommand(taskManagementRepository);
    }

    @Test
    public void should_ThrowException_When_Arguments_Invalid(){
        List<String> params = getList(ARGUMENTS_COUNT - 1);

        assertThrows(InvalidArgumentCountException.class, () -> filterFeedbackByRating.execute(params));
    }

    @Test
    public void should_ThrowException_When_List_Empty(){

        List<String> params = List.of(String.valueOf(VALID_RATING));

        assertThrows(IllegalArgumentException.class, () -> filterFeedbackByRating.execute(params));
    }


    @Test
    public void should_Print_When_All_Is_Valid() {

        taskManagementRepository.createFeedback(VALID_TITLE, VALID_DESCR, VALID_RATING, new BoardImpl("BoardImpl"));

        List<String> params = List.of(String.valueOf(VALID_RATING));

        assertDoesNotThrow(() -> filterFeedbackByRating.execute(params));

    }

}
