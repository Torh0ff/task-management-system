package com.telerikacademy.project.taskmanagementsystem.commands;

import com.telerikacademy.project.taskmanagementsystem.commands.contracts.Command;
import com.telerikacademy.project.taskmanagementsystem.core.TaskManagementRepositoryImpl;
import com.telerikacademy.project.taskmanagementsystem.core.contract.TaskManagementRepository;
import com.telerikacademy.project.taskmanagementsystem.exceptions.InvalidArgumentCountException;
import com.telerikacademy.project.taskmanagementsystem.models.team.PersonImplTest;
import com.telerikacademy.project.taskmanagementsystem.models.team.TeamImplTest;
import com.telerikacademy.project.taskmanagementsystem.models.team.contacts.Person;
import com.telerikacademy.project.taskmanagementsystem.models.team.contacts.Team;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static com.telerikacademy.project.taskmanagementsystem.TestUtilities.getList;
import static com.telerikacademy.project.taskmanagementsystem.models.team.PersonImplTest.VALID_NAME;
import static com.telerikacademy.project.taskmanagementsystem.models.team.PersonImplTest.initializePerson;
import static com.telerikacademy.project.taskmanagementsystem.models.team.TeamImplTest.*;
import static org.junit.jupiter.api.Assertions.*;

public class AddPersonToTeamCommandTest {
    private static final int EXPECTED_ARGUMENTS_COUNT = 2;

    private Command addPersonToTeamCommand;
    private TaskManagementRepository taskManagementRepository;

    @BeforeEach
    public void beforeEach(){
        taskManagementRepository = new TaskManagementRepositoryImpl();
        addPersonToTeamCommand = new AddPersonToTeamCommand(taskManagementRepository);
    }

    @Test
    public void should_ThrowException_When_ArgumentCountDifferentThanExpected() {
        // Arrange
        List<String> params = getList(EXPECTED_ARGUMENTS_COUNT - 1);

        // Act, Assert
        Assertions.assertThrows(InvalidArgumentCountException.class, () -> addPersonToTeamCommand.execute(params));
    }

    @Test
    public void should_ThrowException_When_Team_Does_Not_Exist(){
        Person person = initializePerson();
        taskManagementRepository.createPerson(person.getName());

        List<String> params = List.of(VALID_NAME, TeamImplTest.VALID_NAME);
        assertThrows(IllegalArgumentException.class, () -> addPersonToTeamCommand.execute(params));
    }

    @Test
    public void should_ThrowException_When_Person_Does_Not_Exist(){
        Team team = initializeTeam();
        taskManagementRepository.createTeam(team.getName());

        List<String> params = List.of(VALID_NAME, TeamImplTest.VALID_NAME);
        assertThrows(IllegalArgumentException.class, () -> addPersonToTeamCommand.execute(params));
    }

    @Test
    public void should_ThrowException_When_Input_Invalid(){
        List<String> params = List.of(PersonImplTest.INVALID_NAME_LONGER, INVALID_NAME_SHORTER);
        assertThrows(IllegalArgumentException.class, () -> addPersonToTeamCommand.execute(params));
    }

    @Test
    public void should_Add_Person_To_Team_When_Input_Valid(){
        Person person = initializePerson();
        Team team = initializeTeam();

        taskManagementRepository.createPerson(person.getName());
        taskManagementRepository.createTeam(team.getName());

        List<String> params = List.of(person.getName(), team.getName());

        addPersonToTeamCommand.execute(params);
        team.addMember(person);

        assertEquals(1, team.getMembers().size());
    }


}