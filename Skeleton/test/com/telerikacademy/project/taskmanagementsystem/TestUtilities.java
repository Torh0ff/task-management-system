package com.telerikacademy.project.taskmanagementsystem;

import java.util.List;

import static java.util.Arrays.asList;

public class TestUtilities {

    /**
     * Returns a new String with size equal to wantedSize.
     * Useful when you do not care what the value of the String is,
     * for example when testing if a String is of a certain size.
     *
     * @param size the size of the String to be returned.
     * @return a new String with size equal to wantedSize
     */
    public static String getString(int size) {
        return "x".repeat(size);
    }

    /**
     * Returns a new List with size equal to wantedSize.
     * Useful when you do not care what the contents of the List are,
     * for example when testing if a list of a command throws exception
     * when it's parameters list's size is less/more than expected.
     *
     * @param size the size of the List to be returned.
     * @return a new List with size equal to wantedSize
     */
    public static List<String> getList(int size) {
        return asList(new String[size]);
    }

}
