package com.telerikacademy.project.taskmanagementsystem.models.tasks;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static com.telerikacademy.project.taskmanagementsystem.TestUtilities.getString;
import static org.junit.jupiter.api.Assertions.assertAll;

public class CommentImplTest {

    public static final int USERNAME_LEN_MIN = 5;
    public static final String VALID_AUTHOR = getString(USERNAME_LEN_MIN + 1);


@Test
public void constructor_Should_CreateNewComment_When_ParametersAreCorrect() {
    // Arrange, Act
    CommentImpl comment = initializeTestComment();
    // Assert
    assertAll(
            () -> Assertions.assertEquals("Content", comment.getContent()),
            () -> Assertions.assertEquals(VALID_AUTHOR, comment.getAuthor())
    );
}
    public static CommentImpl initializeTestComment() {
        return new CommentImpl(
                "Content",
                VALID_AUTHOR);
    }

    @Test
    public void print() {
    CommentImpl comment = initializeTestComment();
    String fact = comment.toString();
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(String.format(" %s%n User: %s%n -----------------------------"
                ,comment.getContent()
                ,comment.getAuthor()));
        Assertions.assertEquals(stringBuilder.toString(), fact);

    }

}
