package com.telerikacademy.project.taskmanagementsystem.models.tasks;

import com.telerikacademy.project.taskmanagementsystem.models.tasks.contracts.Task;
import com.telerikacademy.project.taskmanagementsystem.models.tasks.contracts.enums.TaskPriority;
import com.telerikacademy.project.taskmanagementsystem.models.tasks.contracts.enums.bug.BugStatus;
import com.telerikacademy.project.taskmanagementsystem.models.tasks.contracts.enums.bug.Severity;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class BugImplTest {
    public static final String VALID_TITLE = "VALIDTITLE";
    public static final String VALID_DESCR = "VALIDDESCR";
    public static ArrayList<String> VALID_STEPS = new ArrayList<>();
    public static final Severity VALID_SEVERITY = Severity.CRITICAL;
    public static final TaskPriority VALID_PRIORITY = TaskPriority.HIGH;
    public static final BugStatus VALID_STATUS = BugStatus.ACTIVE;

    public static final Severity ANOTHER_SEVERITY = Severity.MAJOR;
    public static final TaskPriority ANOTHER_PRIORITY = TaskPriority.LOW;
    public static final BugStatus ANOTHER_STATUS = BugStatus.FIXED;

    @Test
    public void BugImpl_ShouldImplementTaskInterface() {
        BugImpl bug = initializeTestBug();
        Assertions.assertTrue(bug instanceof Task);
    }

    @Test
    public void constructor_Should_CreateNewBug_When_ParametersAreCorrect() {
        // Arrange, Act
        BugImpl bug = initializeTestBug();

        // Assert
        assertAll(
                () -> Assertions.assertEquals(1, bug.getId()),
                () -> Assertions.assertEquals(VALID_TITLE, bug.getTitle()),
                () -> Assertions.assertEquals(VALID_DESCR, bug.getDescription()),
                () -> Assertions.assertEquals(VALID_SEVERITY.toString(), bug.getSeverity()),
                () -> Assertions.assertEquals(VALID_PRIORITY.toString(), bug.getPriority()),
                () -> Assertions.assertEquals(VALID_STEPS, bug.getSteps()),
                () -> Assertions.assertEquals(VALID_STATUS.toString(), bug.getStatus())
        );
    }

    @Test
    public void constructor_Should_Throw_Exception_When_SetTheSameSeverity() {
        BugImpl bug = initializeTestBug();
        assertThrows(IllegalArgumentException.class, () ->bug.setSeverity(VALID_SEVERITY));

    }

    @Test
    public void constructor_Should_Throw_Exception_When_SetTheSameStatus() {
        BugImpl bug = initializeTestBug();
        assertThrows(IllegalArgumentException.class, () ->bug.setStatus(VALID_STATUS));

    }

    @Test
    public void constructor_Should_Throw_Exception_When_SetTheSamePriority() {
        BugImpl bug = initializeTestBug();
        assertThrows(IllegalArgumentException.class, () ->bug.setPriority(VALID_PRIORITY));

    }

    @Test
    public void constructor_Should_Set_Severity_WhenNotTheSame() {
        BugImpl bug = initializeTestBug();
        Assertions.assertDoesNotThrow (() -> bug.setSeverity(ANOTHER_SEVERITY));
    }

    @Test
    public void constructor_Should_Set_Status_WhenNotTheSame() {
        BugImpl bug = initializeTestBug();
        Assertions.assertDoesNotThrow (() -> bug.setStatus(ANOTHER_STATUS));
    }

    @Test
    public void constructor_Should_Set_Priority_WhenNotTheSame() {
        BugImpl bug = initializeTestBug();
        Assertions.assertDoesNotThrow (() -> bug.setPriority(ANOTHER_PRIORITY));
    }

    @Test
    public void print() {

        BugImpl bug = initializeTestBug();
        String fact = bug.print();
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(String.format("Bug%n"));
        stringBuilder.append(String.format("ID: %d%n", bug.getId()));
        stringBuilder.append(String.format("Title: %s%n", bug.getTitle()));
        stringBuilder.append(String.format("Description: %s%n", bug.getDescription()));
        stringBuilder.append(String.format("Severity: %s%nPriority: %s%nSteps: %s%nStatus: %s%nAssignee: %s%n----------------------------------------------%n".trim(),
                 bug.getSeverity(), bug.getPriority(), bug.getSteps().toString(), bug.getStatus(), bug.getAssignee()));
        Assertions.assertEquals(stringBuilder.toString(), fact);
    }

    public static BugImpl initializeTestBug() {
        return new BugImpl(
                1,
                VALID_TITLE,
                VALID_DESCR,
                VALID_SEVERITY,
                VALID_PRIORITY,
                VALID_STEPS);
    }
}
