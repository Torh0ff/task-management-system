package com.telerikacademy.project.taskmanagementsystem.models.tasks;

import com.telerikacademy.project.taskmanagementsystem.models.tasks.contracts.Task;
import com.telerikacademy.project.taskmanagementsystem.models.tasks.contracts.enums.feedback.FeedbackStatus;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static com.telerikacademy.project.taskmanagementsystem.models.tasks.BugImplTest.*;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class FeedbackImplTest {
    public static final int VALID_RATING = 1;
    public static final FeedbackStatus VALID_STATUS = FeedbackStatus.NEW;

    @Test
    public void FeedbackImpl_ShouldImplementTaskInterface() {
        FeedbackImpl feedback = initializeTestFeedback();
        Assertions.assertTrue(feedback instanceof Task);
    }

    @Test
    public void constructor_Should_CreateNewFeedback_When_ParametersAreCorrect() {
        // Arrange, Act
        FeedbackImpl feedback = initializeTestFeedback();
        // Assert
        assertAll(
                () -> Assertions.assertEquals(1, feedback.getId()),
                () -> Assertions.assertEquals(VALID_TITLE, feedback.getTitle()),
                () -> Assertions.assertEquals(VALID_DESCR, feedback.getDescription()),
                () -> Assertions.assertEquals(VALID_RATING, feedback.getRating()),
                () -> Assertions.assertEquals(VALID_STATUS.toString(), feedback.getStatus())
        );
    }

    @Test
    public void constructor_Should_Throw_Exception_When_SetTheSameStatus() {
        FeedbackImpl feedback = initializeTestFeedback();
        assertThrows(IllegalArgumentException.class, () -> feedback.setStatus(VALID_STATUS));
    }

    @Test
    public void constructor_Should_ThrowException_When_RatingValueIsInvalidOrNegative() {
        // Arrange, Act, Assert
        assertThrows(IllegalArgumentException.class, () ->
                new FeedbackImpl(
                        1,
                        VALID_TITLE,
                        VALID_DESCR,
                        -1));

    }

    @Test
    public void print() {
        FeedbackImpl feedback = initializeTestFeedback();
        String fact = feedback.print();
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(String.format("Bug%n"));
        stringBuilder.append(String.format("ID: %d%n", feedback.getId()));
        stringBuilder.append(String.format("Title: %s%n", feedback.getTitle()));
        stringBuilder.append(String.format("Description: %s%n", feedback.getDescription()));
        stringBuilder.append(String.format("Rating: %d%nStatus: %s%nAssignee: %s%n----------------------------------------------%n".trim(),
                feedback.getRating(), feedback.getStatus(), feedback.getAssignee()));
    }

    public static FeedbackImpl initializeTestFeedback() {
        return new FeedbackImpl(
                1,
                VALID_TITLE,
                VALID_DESCR,
                VALID_RATING);
    }
}