package com.telerikacademy.project.taskmanagementsystem.models.tasks;

import com.telerikacademy.project.taskmanagementsystem.models.tasks.contracts.Task;
import com.telerikacademy.project.taskmanagementsystem.models.tasks.contracts.enums.story.Size;
import com.telerikacademy.project.taskmanagementsystem.models.tasks.contracts.enums.story.StoryStatus;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static com.telerikacademy.project.taskmanagementsystem.models.tasks.BugImplTest.*;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class StoryImplTest {
    public static final Size VALID_SIZE = Size.MEDIUM;
    public static final Size ANOTHER_SIZE = Size.LARGE;

    public static final StoryStatus VALID_STATUS = StoryStatus.NOT_DONE;
    public static final StoryStatus ANOTHER_STATUS = StoryStatus.IN_PROGRESS;


    @Test
    public void StoryImpl_ShouldImplementTaskInterface() {
        StoryImpl story = initializeTestStory();
        Assertions.assertTrue(story instanceof Task);
    }

    @Test
    public void constructor_Should_CreateNewStory_When_ParametersAreCorrect() {
        // Arrange, Act
        StoryImpl story = initializeTestStory();

        // Assert
        assertAll(
                () -> Assertions.assertEquals(1, story.getId()),
                () -> Assertions.assertEquals(VALID_TITLE, story.getTitle()),
                () -> Assertions.assertEquals(VALID_DESCR, story.getDescription()),
                () -> Assertions.assertEquals(VALID_SIZE.toString(), story.getSize()),
                () -> Assertions.assertEquals(VALID_PRIORITY.toString(), story.getPriority()),
                () -> Assertions.assertEquals(VALID_STATUS.toString(), story.getStatus())
        );
    }

    @Test
    public void constructor_Should_Throw_Exception_When_SetTheSameSize() {
        StoryImpl story = initializeTestStory();
        assertThrows(IllegalArgumentException.class, () -> story.setSize(VALID_SIZE));
}

    @Test
    public void constructor_Should_Throw_Exception_When_SetTheSamePriority() {
        StoryImpl story = initializeTestStory();
        assertThrows(IllegalArgumentException.class, () -> story.setPriority(VALID_PRIORITY));
    }

    @Test
    public void constructor_Should_Throw_Exception_When_SetTheSameStatus() {
        StoryImpl story = initializeTestStory();
        assertThrows(IllegalArgumentException.class, () -> story.setStatus(VALID_STATUS));
    }

    @Test
    public void constructor_Should_Set_Status_WhenNotTheSame() {
        StoryImpl story = initializeTestStory();
        Assertions.assertDoesNotThrow( () -> story.setStatus(ANOTHER_STATUS));
    }

    @Test
    public void constructor_Should_Set_Priority_WhenNotTheSame() {
        StoryImpl story = initializeTestStory();
        Assertions.assertDoesNotThrow( () -> story.setPriority(ANOTHER_PRIORITY));
    }

    @Test
    public void constructor_Should_Set_Size_WhenNotTheSame() {
        StoryImpl story = initializeTestStory();
        Assertions.assertDoesNotThrow( () -> story.setSize(ANOTHER_SIZE));
    }

    @Test
    public void print() {
        StoryImpl story = initializeTestStory();
        String fact = story.print();
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(String.format("Story%n"));
        stringBuilder.append(String.format("ID: %d%n", story.getId()));
        stringBuilder.append(String.format("Title: %s%n", story.getTitle()));
        stringBuilder.append(String.format("Description: %s%n", story.getDescription()));
        stringBuilder.append(String.format("Size: %s%nPriority: %s%nAssignee: %s%n----------------------------------------------%n".trim(),
                 story.getSize(), story.getPriority(), story.getAssignee()));
        Assertions.assertEquals(stringBuilder.toString(), fact);
    }

    public static StoryImpl initializeTestStory() {
        return new StoryImpl(
                1,
                VALID_TITLE,
                VALID_DESCR,
                VALID_SIZE,
                VALID_PRIORITY);
    }
}
