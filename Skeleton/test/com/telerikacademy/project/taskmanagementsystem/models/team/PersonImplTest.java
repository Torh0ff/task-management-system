package com.telerikacademy.project.taskmanagementsystem.models.team;

import com.telerikacademy.project.taskmanagementsystem.models.tasks.FeedbackImpl;
import com.telerikacademy.project.taskmanagementsystem.models.tasks.StoryImpl;
import com.telerikacademy.project.taskmanagementsystem.models.tasks.contracts.Task;
import com.telerikacademy.project.taskmanagementsystem.models.team.contacts.Eventlog;
import com.telerikacademy.project.taskmanagementsystem.models.team.contacts.Person;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class PersonImplTest {

    public static String getString(int length) {
        return "x".repeat(length);
    }

    public static int NAME_MIN_LENGTH = 5;
    public static int NAME_MAX_LENGTH = 15;

    public static String VALID_NAME = getString(NAME_MIN_LENGTH + 1);
    public static String INVALID_NAME_SHORTER = getString(NAME_MIN_LENGTH - 1);
    public static String INVALID_NAME_LONGER = getString(NAME_MAX_LENGTH + 1);

    @Test
    public void constructor_Should_ThrowException_When_Name_OutOfBounds_Shorter() {
        assertThrows(IllegalArgumentException.class, () -> new PersonImpl(INVALID_NAME_SHORTER));
    }

    @Test
    public void constructor_Should_ThrowException_When_Name_OutOfBounds_Longer() {
        assertThrows(IllegalArgumentException.class, () -> new PersonImpl(INVALID_NAME_LONGER));
    }

    @Test
    public void constructor_Should_Initialize_WhenValidArgumentsArePassed(){
        PersonImpl person = initializePerson();
        assertAll(() -> assertEquals(VALID_NAME, person.getName()));
    }

    @Test
    public void getTask_Should_Return_Copy(){
        Person person = initializePerson();

        assertEquals(0, person.getTasks().size());
    }

    @Test
    public void getLogs_Should_Return_Copy(){
        Person person = initializePerson();
        assertEquals(0, person.getLogs().size());
    }

    @Test
    public void addLog_Should_AddLog(){
        Person person = initializePerson();
        Eventlog eventlog = new EventLogImpl("Testing");
        assertAll(
                () -> assertDoesNotThrow(() -> person.addEventLog(eventlog)),
                () -> assertEquals(1, person.getLogs().size())
        );
    }

    @Test
    public void addTask_Should_AddTask(){
        Person person = initializePerson();
        Task task = new FeedbackImpl(1, "NiceFeedback", "Doing well", 4);

        assertAll(
                () -> assertDoesNotThrow(() -> person.addTask(task)),
                () -> assertEquals(1, person.getTasks().size())
        );
    }

    @Test
    public void deleteTask_Should_DeleteTask(){
        Person person = initializePerson();
        Task task = new FeedbackImpl(1, "NiceFeedback", "Doing well", 4);
        person.addTask(task);
        assertAll(
                () -> assertDoesNotThrow(() -> person.deleteTask(task)),
                () -> assertEquals(0, person.getTasks().size())
        );
    }


    public static PersonImpl initializePerson() {
        return new PersonImpl(VALID_NAME);
    }
}