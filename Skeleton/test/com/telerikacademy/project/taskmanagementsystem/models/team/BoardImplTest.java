package com.telerikacademy.project.taskmanagementsystem.models.team;

import com.telerikacademy.project.taskmanagementsystem.models.tasks.FeedbackImpl;
import com.telerikacademy.project.taskmanagementsystem.models.tasks.contracts.Task;
import com.telerikacademy.project.taskmanagementsystem.models.team.contacts.Board;
import com.telerikacademy.project.taskmanagementsystem.models.team.contacts.Eventlog;
import com.telerikacademy.project.taskmanagementsystem.models.team.contacts.Person;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;


public class BoardImplTest {

    public static String getString(int length) {
        return "x".repeat(length);
    }

    public static int NAME_MIN_LENGTH = 5;
    public static int NAME_MAX_LENGTH = 10;

    public static String VALID_NAME = getString(NAME_MIN_LENGTH + 1);
    public static String INVALID_NAME_SHORTER = getString(NAME_MIN_LENGTH - 1);
    public static String INVALID_NAME_LONGER = getString(NAME_MAX_LENGTH + 1);


    @Test
    public void constructor_Should_ThrowException_When_Name_OutOfBounds_Shorter(){
        assertThrows(IllegalArgumentException.class, () -> new BoardImpl(INVALID_NAME_SHORTER));
    }

    @Test
    public void constructor_Should_ThrowException_When_Name_OutOfBounds_Longer(){
        assertThrows(IllegalArgumentException.class, () -> new BoardImpl(INVALID_NAME_LONGER));
    }

    @Test
    public void constructor_Should_Initialize_WhenValidArgumentsArePassed(){
        BoardImpl board= initializeBoard();
        assertAll(() -> assertEquals(VALID_NAME, board.getName()));
    }


    @Test
    public void getTask_Should_Return_Copy(){
        Board board = initializeBoard();

        assertEquals(0, board.getTasks().size());
    }

    @Test
    public void getLogs_Should_Return_Copy(){
        Board board = initializeBoard();
        assertEquals(0, board.getLogs().size());
    }

    @Test
    public void addLog_Should_AddLog(){
        Board board = initializeBoard();
        Eventlog eventlog = new EventLogImpl("Testing");
        assertAll(
                () -> assertDoesNotThrow(() -> board.addEventLog(eventlog)),
                () -> assertEquals(1, board.getLogs().size())
        );
    }

    @Test
    public void addTask_Should_AddTask(){
        Board board = initializeBoard();
        Task task = new FeedbackImpl(1, "NiceFeedback", "Doing well", 4);

        assertAll(
                () -> assertDoesNotThrow(() -> board.addTask(task)),
                () -> assertEquals(1, board.getTasks().size())
        );
    }

    @Test
    public void deleteTask_Should_DeleteTask(){
        Board board = initializeBoard();
        Task task = new FeedbackImpl(1, "NiceFeedback", "Doing well", 4);
        board.addTask(task);
        assertAll(
                () -> assertDoesNotThrow(() -> board.deleteTask(task)),
                () -> assertEquals(0, board.getTasks().size())
        );
    }
    public static BoardImpl initializeBoard() {
        return new BoardImpl(VALID_NAME);
    }
}