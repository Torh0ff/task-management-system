package com.telerikacademy.project.taskmanagementsystem.models.team;

import com.telerikacademy.project.taskmanagementsystem.models.team.contacts.Board;
import com.telerikacademy.project.taskmanagementsystem.models.team.contacts.Eventlog;
import com.telerikacademy.project.taskmanagementsystem.models.team.contacts.Person;
import com.telerikacademy.project.taskmanagementsystem.models.team.contacts.Team;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class TeamImplTest {
    public static String getString(int length) {
        return "x".repeat(length);
    }


    public static int NAME_MIN_LENGTH = 5;
    public static int NAME_MAX_LENGTH = 15;

    public static String VALID_NAME = getString(NAME_MIN_LENGTH + 1);
    public static String INVALID_NAME_SHORTER = getString(NAME_MIN_LENGTH - 1);
    public static String INVALID_NAME_LONGER = getString(NAME_MAX_LENGTH + 1);

    @Test
    public void constructor_Should_ThrowException_When_Name_OutOfBounds_Shorter() {
        assertThrows(IllegalArgumentException.class, () -> new TeamImpl(INVALID_NAME_SHORTER));
    }

    @Test
    public void constructor_Should_ThrowException_When_Name_OutOfBounds_Longer() {
        assertThrows(IllegalArgumentException.class, () -> new TeamImpl(INVALID_NAME_LONGER));
    }

    @Test
    public void constructor_Should_Initialize_WhenValidArgumentsArePassed(){
        TeamImpl team = initializeTeam();
        assertAll(() -> assertEquals(VALID_NAME, team.getName()));
    }

    @Test
    public void addMember_Should_AddMemberToList(){
        Team team = new TeamImpl(VALID_NAME);
        Person person = new PersonImpl(VALID_NAME);


        assertAll(
                () -> assertDoesNotThrow(() -> team.addMember(person)),
                () -> assertEquals(1, team.getMembers().size())
        );
    }


    @Test
    public void removeMember_ShouldRemove_MemberFromList(){
        Team team = new TeamImpl(VALID_NAME);
        Person person = new PersonImpl(VALID_NAME);

        team.addMember(person);

        assertAll(
                () -> assertDoesNotThrow(() -> team.removeMember(person)),
                () -> assertEquals(0, team.getMembers().size())
        );
    }


    @Test
    public void removeBoars_ShouldRemove_BoardFromList(){
        Team team = new TeamImpl(VALID_NAME);
        Board board = new BoardImpl(VALID_NAME);

        team.addBoard(board);

        assertAll(
                () -> assertDoesNotThrow(() -> team.removeBoard(board)),
                () -> assertEquals(0, team.getBoards().size())
        );
    }

    @Test
    public void addBoard_Should_AddBoardToList(){
        Team team = new TeamImpl(VALID_NAME);
        Board board = new BoardImpl(VALID_NAME);

        assertAll(
                () -> assertDoesNotThrow(() -> team.addBoard(board)),
                () -> assertEquals(1, team.getBoards().size())
        );
    }

    @Test
    public void getHistory_Should_ReturnCopy(){
        Team team = initializeTeam();
        assertEquals(0, team.getHistory().size());
    }

    @Test
    public void addEventLog_Should_Add(){
        Team team = initializeTeam();
        Eventlog eventlog = new EventLogImpl("Testing");

        assertAll(
                () -> assertDoesNotThrow(() -> team.addEventLog(eventlog)),
                () -> assertEquals(1, team.getHistory().size())
        );
    }
    public static TeamImpl initializeTeam() {
        return new TeamImpl(VALID_NAME);
    }

}