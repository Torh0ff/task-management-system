package com.telerikacademy.project.taskmanagementsystem;

import com.telerikacademy.project.taskmanagementsystem.core.TaskManagementEngineImpl;

public class StartUp {

    public static void main(String[] args) {
        TaskManagementEngineImpl engine = new TaskManagementEngineImpl();
        engine.start();
    }

}
