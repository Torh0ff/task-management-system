package com.telerikacademy.project.taskmanagementsystem.core.contract;

public interface Engine {

    void start();

}
