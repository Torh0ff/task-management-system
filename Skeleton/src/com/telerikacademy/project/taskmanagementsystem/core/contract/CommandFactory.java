package com.telerikacademy.project.taskmanagementsystem.core.contract;


import com.telerikacademy.project.taskmanagementsystem.commands.contracts.Command;

public interface CommandFactory {

    Command createCommandFromCommandName(String commandName, TaskManagementRepository repository);

}
