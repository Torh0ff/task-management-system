package com.telerikacademy.project.taskmanagementsystem.core.contract;


import com.telerikacademy.project.taskmanagementsystem.models.tasks.contracts.*;
import com.telerikacademy.project.taskmanagementsystem.models.tasks.contracts.enums.TaskPriority;
import com.telerikacademy.project.taskmanagementsystem.models.tasks.contracts.enums.TaskType;
import com.telerikacademy.project.taskmanagementsystem.models.tasks.contracts.enums.bug.Severity;
import com.telerikacademy.project.taskmanagementsystem.models.tasks.contracts.enums.story.Size;
import com.telerikacademy.project.taskmanagementsystem.models.tasks.contracts.enums.story.StoryStatus;
import com.telerikacademy.project.taskmanagementsystem.models.team.contacts.Board;
import com.telerikacademy.project.taskmanagementsystem.models.team.contacts.Namable;
import com.telerikacademy.project.taskmanagementsystem.models.team.contacts.Person;
import com.telerikacademy.project.taskmanagementsystem.models.team.contacts.Team;
import com.telerikacademy.project.taskmanagementsystem.models.tasks.contracts.enums.feedback.FeedbackStatus;

import java.util.List;

public interface TaskManagementRepository {

    Person findPersonByName(String name);

    boolean personExists(String name);

    void createPerson(String name);

    List<Person> getPeople();

    boolean teamExists(String name);

    void createTeam(String name);

    List<Team> getTeams();

    List<Task> getTasks();

    List<Bug> getBugs();
    List<Story> getStories();

    List<Feedback> getFeedbacks();

    Team findTeamByName(String name);

    List<Board> getBoards();

    void createBoard(String boardName);

    void createBug(String title, String description, Severity severity, TaskPriority priority, Board board, List<String> steps);

    void createStory(String title, String description, Size size, TaskPriority priority, Board board);

    void createFeedback(String title, String description,int rating, Board board);

    Board findBoardByName(String boardName);

    boolean boardExists(String name);

    boolean taskExists(String name);

    <T extends Namable> T findElementByName(List<T> elements, String elementName, String name);
    <T extends Namable> boolean elementExists(List<T> elements, String name);

    <T extends Identifiable> T findTaskById(List<T> elements, int id);

    <T extends Namable> T findTaskByName(List<T> elements, String elementName, String name);

}
