package com.telerikacademy.project.taskmanagementsystem.core;


import com.telerikacademy.project.taskmanagementsystem.commands.*;
import com.telerikacademy.project.taskmanagementsystem.commands.contracts.Command;
import com.telerikacademy.project.taskmanagementsystem.commands.enums.CommandType;
import com.telerikacademy.project.taskmanagementsystem.commands.listings.*;
import com.telerikacademy.project.taskmanagementsystem.commands.listings.listingAndSorting.*;
import com.telerikacademy.project.taskmanagementsystem.core.contract.CommandFactory;
import com.telerikacademy.project.taskmanagementsystem.core.contract.TaskManagementRepository;

public class CommandFactoryImpl implements CommandFactory {
    private static final String COMMAND_NOT_SUPPORTED_MESSAGE = "Command %s is not supported.";


    @Override
    public Command createCommandFromCommandName(String commandName, TaskManagementRepository repository) {

        CommandType commandType = tryParseCommandType(commandName);

        switch (commandType) {
            case CREATEPERSON:
                return new CreatePersonCommand(repository);
            case SHOWALLPEOPLE:
                return new ShowAllPeopleCommand(repository);
            case SHOWPERSONSACTIVITY:
                return new ShowPersonActivityCommand(repository);
            case CREATETEAM:
                return new CreateTeamCommand(repository);
            case SHOWALLTEAMS:
                return new ShowAllTeamsCommand(repository);
            case SHOWTEAMSACTIVITY:
                return new ShowTeamsActivityCommand(repository);
            case SHOWALLTEAMBOARDS:
                return new ShowAllTeamBoardsCommand(repository);
            case SHOWALLTEAMMEMBERS:
                return new ShowAllTeamMembersCommand(repository);
            case SHOWBOARDACTIVITY:
                return new ShowBoardsActivityCommand(repository);
            case ADDPERSONTOTEAM:
                return new AddPersonToTeamCommand(repository);
            case CREATENEWBOARDINATEAM:
                return new CreateNewBoardInTeamCommand(repository);
            case CREATEBUG:
                return new CreateBugCommand(repository);
            case CREATEFEEDBACK:
                return new CreateFeedbackCommand(repository);
            case CREATESTORY:
                return new CreateStoryCommand(repository);
            case CHANGEBUGSTATUS:
                return new ChangeBugStatusCommand(repository);
            case CHANGEFEEDBACKSTATUS:
                return new ChangeFeedbackStatusCommand(repository);
            case CHANGESTORYSTATUS:
                return new ChangeStoryStatusCommand(repository);
            case CHANGEBUGSEVERITY:
                return new ChangeBugSeverityCommand(repository);
            case CHANGEPRIORITY:
                return new ChangePriorityCommand(repository);
            case CHANGESIZE:
                return new ChangeSizeCommand(repository);
            case ADDCOMMENT:
                return new AddCommentCommand(repository);
            case SHOWALLTASKSCOMMENTS:
                return new ShowAllTasksCommentsCommand(repository);
            case ASSIGNTASK:
                return new AssignTaskCommand(repository);
            case UNASSIGNTASK:
                return new UnAssignTaskCommand(repository);
            case LISTALLTASKSWITHASSIGNEE:
                return new ListAllTasksWithAndFilteredByAssigneeCommand(repository);
            case LISTALLTASKSWITHSTATUS:
                return new ListAllTasksWithAssigneeFilteredByStatusCommand(repository);
            case FILTERBUGSBYPRIORITY:
                return new FilterBugsByPriorityCommand(repository);
            case FILTERBUGSBYSEVERITY:
                return new FilterBugsBySeverityCommand(repository);
            case FILTERBUGSBYSTATUS:
                return new FilterBugsByStatusCommand(repository);
            case FILTERSTORIESBYPRIORITY:
                return new FilterStoriesByPriorityCommand(repository);
            case FILTERSTORIESBYSIZE:
                return new FilterStoriesBySizeCommand(repository);
            case FILTERSTORIESBYSTATUS:
                return new FilterStoriesByStatusCommand(repository);
            case FILTERFEEDBACKSBYRATING:
                return new FilterFeedbacksByRatingCommand(repository);
            case FILTERFEEDBACKSBYSTATUS:
                return new FilterFeedbacksByStatusCommand(repository);
            case SHOWTASKACTIVITY:
                return new ShowTaskActivityCommand(repository);
            case LISTALLTASKS:
                return new ListAllTasksCommand(repository);
            case LISTTASKFILTERESBYASSIGNEE:
                return new ListTaskFilteredByAssigneeCommand(repository);
            case LISTTASKSFILTEREDBYSTATUS:
                return new ListTasksFilteredByStatusCommand(repository);
            case LISTTASKSSORTEDBY:
                return new ListTasksSortedByCommand(repository);
            default:
                throw new IllegalArgumentException(String.format(COMMAND_NOT_SUPPORTED_MESSAGE, commandName));
        }
    }

    private CommandType tryParseCommandType(String value) {
        try {
            return CommandType.valueOf(value.toUpperCase());
        } catch (IllegalArgumentException e) {
            // This exception means that the user has entered not existing command.
            throw new IllegalArgumentException(String.format(COMMAND_NOT_SUPPORTED_MESSAGE, value));
        }
    }

}
