package com.telerikacademy.project.taskmanagementsystem.core;


import com.telerikacademy.project.taskmanagementsystem.core.contract.TaskManagementRepository;
import com.telerikacademy.project.taskmanagementsystem.models.tasks.BugImpl;
import com.telerikacademy.project.taskmanagementsystem.models.tasks.FeedbackImpl;
import com.telerikacademy.project.taskmanagementsystem.models.tasks.StoryImpl;
import com.telerikacademy.project.taskmanagementsystem.models.tasks.contracts.*;
import com.telerikacademy.project.taskmanagementsystem.models.tasks.contracts.enums.TaskPriority;
import com.telerikacademy.project.taskmanagementsystem.models.tasks.contracts.enums.bug.Severity;
import com.telerikacademy.project.taskmanagementsystem.models.tasks.contracts.enums.story.Size;
import com.telerikacademy.project.taskmanagementsystem.models.team.BoardImpl;
import com.telerikacademy.project.taskmanagementsystem.models.team.EventLogImpl;
import com.telerikacademy.project.taskmanagementsystem.models.team.PersonImpl;
import com.telerikacademy.project.taskmanagementsystem.models.team.TeamImpl;
import com.telerikacademy.project.taskmanagementsystem.models.team.contacts.*;

import java.util.ArrayList;
import java.util.List;

public class TaskManagementRepositoryImpl implements TaskManagementRepository {

    private static final String PERSON_DOES_NOT_EXIST = "There is no such person with name: %s";
    private static final String TEAM_DOES_NOT_EXISTS = "There is no such team with name: %s";
    private static final String BOARD_DOES_NOT_EXIST = "There is no such board with name: %s";
    private static final String ELEMENT_DOES_NOT_EXISTS = "There is no such %s with name: %s.";

    private static final String TASK_DOES_NOT_EXIST = "There is no such task with ID %d number";
    private final List<Person> people;
    private final List<Team> teams;
    private final List<Board> boards;
    private final List<Task> tasks;
    private final List<Bug> bugs;
    private final List<Story> stories;
    private final List<Feedback> feedbacks;
    private int nextId;



    public TaskManagementRepositoryImpl() {
        nextId = 0;
        people = new ArrayList<>();
        teams = new ArrayList<>();
        boards = new ArrayList<>();
        tasks = new ArrayList<>();
        bugs = new ArrayList<>();
        stories = new ArrayList<>();
        feedbacks = new ArrayList<>();

    }


    @Override
    public Person findPersonByName(String name) {
        return findElementByName(people, "person", name);
    }

    @Override
    public boolean personExists(String name) {
        return elementExists(people, name);
    }

    @Override
    public void createPerson(String name) {
        this.people.add(new PersonImpl(name));
        Eventlog eventlog = new EventLogImpl(String.format("User '%s' was created on this Date.", name));
        this.people.get(this.people.size() - 1).addEventLog(eventlog);
    }

    @Override
    public List<Person> getPeople() {
        return new ArrayList<>(this.people);
    }

    @Override
    public boolean teamExists(String name) {
        return elementExists(teams, name);
    }

    @Override
    public void createTeam(String name) {
        this.teams.add(new TeamImpl(name));
        Eventlog eventlog = new EventLogImpl(String.format("Team '%s' was created on this Date.", name));
        this.teams.get(teams.size() - 1).addEventLog(eventlog);
    }

    @Override
    public List<Team> getTeams() {
        return new ArrayList<>(teams);
    }

    public List<Task> getTasks() {
        return new ArrayList<>(tasks);
    }

    public List<Bug> getBugs() {
        return new ArrayList<>(bugs);
    }

    public List<Story> getStories() {
        return new ArrayList<>(stories);
    }

    public List<Feedback> getFeedbacks() {
        return new ArrayList<>(feedbacks);
    }


    @Override
    public Team findTeamByName(String name) {
        return findElementByName(teams, "team", name);
    }

    @Override
    public List<Board> getBoards() {
        return new ArrayList<>(boards);
    }

    @Override
    public void createBoard(String boardName) {
        this.boards.add(new BoardImpl(boardName));
        Eventlog eventlog = new EventLogImpl(String.format("Board '%s' was created on this Date.", boardName));
        this.boards.get(boards.size() - 1).addEventLog(eventlog);
    }

    @Override
    public void createBug(String title, String description, Severity severity, TaskPriority priority, Board board, List<String> steps) {
        Bug bug = new BugImpl(++nextId, title, description, severity, priority, steps);
        this.tasks.add(bug);
        this.bugs.add(bug);
        board.addTask(bug);
        Eventlog eventlog = new EventLogImpl(String.format("Bug '%s' was created on this Date.", title));
        this.tasks.get(tasks.size() -1).addEventLog(eventlog);
    }

    @Override
    public void createStory(String title, String description, Size size, TaskPriority priority, Board board) {
        Story story = new StoryImpl(++nextId, title, description, size, priority);
        this.tasks.add(story);
        this.stories.add(story);
        board.addTask(story);
        Eventlog eventlog = new EventLogImpl(String.format("Story '%s' was created on this Date.", title));
        this.tasks.get(tasks.size() -1).addEventLog(eventlog);
    }

    @Override
    public void createFeedback(String title, String description, int rating, Board board) {
        Feedback feedback = new FeedbackImpl(++nextId, title, description, rating);
        this.tasks.add(feedback);
        this.feedbacks.add(feedback);
        board.addTask(feedback);
        Eventlog eventlog = new EventLogImpl(String.format("Feedback '%s' was created on this Date.", title));
        this.tasks.get(tasks.size() -1).addEventLog(eventlog);
    }

    @Override
    public Board findBoardByName(String boardName) {
        return findElementByName(boards, "board", boardName);
    }

    @Override
    public boolean boardExists(String name) {
        return elementExists(boards, name);
    }

    @Override
    public boolean taskExists(String name) {
        return elementExists(tasks, name);
    }


    public <T extends Namable> T findElementByName(List<T> elements, String elementName, String name) {
        for (T element : elements) {
            if (element.getName().equals(name)){
                return element;
            }
        }
        throw new IllegalArgumentException(String.format(ELEMENT_DOES_NOT_EXISTS, elementName, name));
    }

    @Override
    public <T extends Namable> boolean elementExists(List<T> elements, String name) {
        for (T element : elements) {
            if (element.getName().equalsIgnoreCase(name)){
                return true;
            }
        }
        return false;
    }

    public <T extends Identifiable> T findTaskById(List<T> elements, int id) {
        for (T element : elements){
            if (element.getId() == id){
                return element;
            }
        }
        throw new IllegalArgumentException(String.format(TASK_DOES_NOT_EXIST, id));
    }

    @Override
    public <T extends Namable> T findTaskByName(List<T> elements, String elementName, String name) {
        for (T element : elements) {
            if (element.getName().equals(name)){
                return element;
            }
        }
        throw new IllegalArgumentException(String.format(ELEMENT_DOES_NOT_EXISTS, elementName, name));
    }
}
