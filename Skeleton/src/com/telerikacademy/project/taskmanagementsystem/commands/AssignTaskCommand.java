package com.telerikacademy.project.taskmanagementsystem.commands;

import com.telerikacademy.project.taskmanagementsystem.commands.contracts.Command;
import com.telerikacademy.project.taskmanagementsystem.core.contract.TaskManagementRepository;
import com.telerikacademy.project.taskmanagementsystem.models.tasks.contracts.Task;
import com.telerikacademy.project.taskmanagementsystem.models.team.EventLogImpl;
import com.telerikacademy.project.taskmanagementsystem.models.team.contacts.*;
import com.telerikacademy.project.taskmanagementsystem.utils.ParsingHelpers;
import com.telerikacademy.project.taskmanagementsystem.utils.ValidationHelpers;

import java.util.List;
import java.util.Optional;

public class AssignTaskCommand implements Command {

    public static final int EXPECTED_NUMBER_OF_ARGUMENTS = 2;
    public static final String USER_ASSIGNED_MESSAGE = "User '%s' has been assigned to the task ID:%d.";
    public static final String NO_MEMBER_IN_TEAM = "User '%s' is a part of a team '%s' and cannot be assign to the task ID:%d";

    private final TaskManagementRepository taskManagementRepository;

    public AssignTaskCommand(TaskManagementRepository taskManagementRepository) {
        this.taskManagementRepository = taskManagementRepository;
    }

    @Override
    public String execute(List<String> parameters) {
        ValidationHelpers.validateArgumentsCount(parameters, EXPECTED_NUMBER_OF_ARGUMENTS);
        int id = ParsingHelpers.tryParseInteger(parameters.get(0), "id");
        String user = parameters.get(1);
        return assignTask(id, user);

    }

    private String assignTask(int taskId, String user) {

        Person assignee = taskManagementRepository.findPersonByName(user);
        Task task = taskManagementRepository.findTaskById(taskManagementRepository.getTasks(), taskId);

        Team team = getMemberTeam(assignee);
        Board board = getBoardTeam(team, taskId);

        if (team.getMembers().contains(assignee) && team.getBoards().contains(board)) {

            task.addAssignee(assignee);
            assignee.addTask(task);

            Eventlog eventlog = new EventLogImpl(String.format(USER_ASSIGNED_MESSAGE, user, taskId));
            assignee.addEventLog(eventlog);
            task.addEventLog(eventlog);

            return String.format(USER_ASSIGNED_MESSAGE, user, taskId);
        }

        throw new IllegalArgumentException(String.format(NO_MEMBER_IN_TEAM, user, team.getName(), taskId));

    }

    private Board getBoardTeam(Team team, int id) {
        Board board = null;
        for (Board b : team.getBoards()) {
            for (Task t : b.getTasks()) {
                if (t.getId() == id) {
                    board = b;
                }
            }
        }
        return board;
    }

    private Team getMemberTeam(Person member) {
        List<Team> teams = taskManagementRepository.getTeams();
        return teams.stream()
                .filter(t -> t.getMembers().contains(member))
                .findFirst().orElse(null);
    }
}