package com.telerikacademy.project.taskmanagementsystem.commands;

import com.telerikacademy.project.taskmanagementsystem.commands.contracts.Command;
import com.telerikacademy.project.taskmanagementsystem.core.contract.TaskManagementRepository;
import com.telerikacademy.project.taskmanagementsystem.models.tasks.CommentImpl;
import com.telerikacademy.project.taskmanagementsystem.models.tasks.contracts.Comment;
import com.telerikacademy.project.taskmanagementsystem.models.tasks.contracts.Task;
import com.telerikacademy.project.taskmanagementsystem.models.team.EventLogImpl;
import com.telerikacademy.project.taskmanagementsystem.models.team.contacts.Eventlog;
import com.telerikacademy.project.taskmanagementsystem.models.team.contacts.Person;
import com.telerikacademy.project.taskmanagementsystem.utils.ParsingHelpers;
import com.telerikacademy.project.taskmanagementsystem.utils.ValidationHelpers;

import java.util.List;

public class AddCommentCommand implements Command {
    public final static String COMMENT_ADDED_MESSAGE = "User '%s' added a comment to the task '%s' ID: %d.";
    public static final String INVALID_INPUT_MESSAGE = "Invalid input. Expected a number.";
    public static final int EXPECTED_NUMBER_OF_ARGUMENTS = 3;

    private final TaskManagementRepository taskManagementRepository;

    public AddCommentCommand(TaskManagementRepository taskManagementRepository) {
        this.taskManagementRepository = taskManagementRepository;
    }

    @Override
    public String execute(List<String> parameters) {
        ValidationHelpers.validateArgumentsCount(parameters, EXPECTED_NUMBER_OF_ARGUMENTS);
        String contentString = parameters.get(0);
        String user = parameters.get(1);
        int taskId = ParsingHelpers.tryParseInteger(parameters.get(2), INVALID_INPUT_MESSAGE);
        String[] content = contentString.split(";");
        return addComment(taskId, user, content);
    }


    private String addComment(int taskId, String user, String[] content) {

        Person person = taskManagementRepository.findPersonByName(user);
        Task task = taskManagementRepository.findTaskById(taskManagementRepository.getTasks(), taskId);
        Comment comment = new CommentImpl(List.of(content).toString(), person.getName());
        task.addComment(comment);
        Eventlog eventlog = new EventLogImpl(String.format(COMMENT_ADDED_MESSAGE, user, task.getTitle(), task.getId()));
        person.addEventLog(eventlog);
        task.addEventLog(eventlog);
        return String.format(COMMENT_ADDED_MESSAGE, user, task.getTitle(), task.getId());
    }
}
