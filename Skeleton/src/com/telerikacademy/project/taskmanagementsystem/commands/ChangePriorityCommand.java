package com.telerikacademy.project.taskmanagementsystem.commands;

import com.telerikacademy.project.taskmanagementsystem.commands.contracts.Command;
import com.telerikacademy.project.taskmanagementsystem.core.contract.TaskManagementRepository;
import com.telerikacademy.project.taskmanagementsystem.models.tasks.contracts.Bug;
import com.telerikacademy.project.taskmanagementsystem.models.tasks.contracts.Story;
import com.telerikacademy.project.taskmanagementsystem.models.tasks.contracts.Task;
import com.telerikacademy.project.taskmanagementsystem.models.tasks.contracts.enums.TaskPriority;
import com.telerikacademy.project.taskmanagementsystem.models.tasks.contracts.enums.TaskType;
import com.telerikacademy.project.taskmanagementsystem.models.team.EventLogImpl;
import com.telerikacademy.project.taskmanagementsystem.models.team.contacts.Eventlog;
import com.telerikacademy.project.taskmanagementsystem.utils.ParsingHelpers;
import com.telerikacademy.project.taskmanagementsystem.utils.ValidationHelpers;

import java.util.List;

public class ChangePriorityCommand implements Command {

    public static final int EXPECTED_NUMBER_OF_ARGUMENTS_CHANGE = 2;
    public static final String INVALID_ID_PARAMETER = "Invalid input, ID number has to be an integer";
    public static final String TASK_PRIORITY_CHANGED = "Priority of '%s' with ID %d has been changed to '%s'.";
    private final TaskManagementRepository taskManagementRepository;

    public ChangePriorityCommand(TaskManagementRepository taskManagementRepository) {
        this.taskManagementRepository = taskManagementRepository;
    }

    @Override
    public String execute(List<String> parameters) {
        ValidationHelpers.validateArgumentsCount(parameters, EXPECTED_NUMBER_OF_ARGUMENTS_CHANGE);
        int bugId = ParsingHelpers.tryParseInteger(parameters.get(0), INVALID_ID_PARAMETER);
        TaskPriority priority = ParsingHelpers.tryParseEnum(parameters.get(1), TaskPriority.class);

        return changedPriority(bugId, priority);

    }

    private String changedPriority(int bugId, TaskPriority priority) {
        Task task = taskManagementRepository.findTaskById(taskManagementRepository.getTasks(), bugId);
        if (task.getType().equals(TaskType.BUG)) {
            Bug bug = (Bug) taskManagementRepository.findTaskById(taskManagementRepository.getTasks(), bugId);
            bug.setPriority(priority);
            Eventlog eventlog = new EventLogImpl(String.format(TASK_PRIORITY_CHANGED, bug.getTitle(), bugId, priority));
            bug.addEventLog(eventlog);
        }
        if (task.getType().equals(TaskType.STORY)) {
            Story story = (Story) taskManagementRepository.findTaskById(taskManagementRepository.getTasks(), bugId);
            story.setPriority(priority);
            Eventlog eventlog = new EventLogImpl(String.format(TASK_PRIORITY_CHANGED, story.getTitle(), bugId, priority));
            story.addEventLog(eventlog);
        }
        return String.format(TASK_PRIORITY_CHANGED, task.getType(), bugId, priority);
    }
}
