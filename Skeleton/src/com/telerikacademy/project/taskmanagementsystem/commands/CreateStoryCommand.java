package com.telerikacademy.project.taskmanagementsystem.commands;

import com.telerikacademy.project.taskmanagementsystem.commands.contracts.Command;
import com.telerikacademy.project.taskmanagementsystem.core.contract.TaskManagementRepository;
import com.telerikacademy.project.taskmanagementsystem.models.tasks.contracts.Task;
import com.telerikacademy.project.taskmanagementsystem.models.tasks.contracts.enums.TaskPriority;
import com.telerikacademy.project.taskmanagementsystem.models.tasks.contracts.enums.story.Size;
import com.telerikacademy.project.taskmanagementsystem.models.team.EventLogImpl;
import com.telerikacademy.project.taskmanagementsystem.models.team.contacts.Board;
import com.telerikacademy.project.taskmanagementsystem.models.team.contacts.Eventlog;
import com.telerikacademy.project.taskmanagementsystem.models.team.contacts.Team;
import com.telerikacademy.project.taskmanagementsystem.utils.ParsingHelpers;
import com.telerikacademy.project.taskmanagementsystem.utils.ValidationHelpers;

import java.util.List;

public class CreateStoryCommand implements Command {


    public static final int EXPECTED_NUMBER_OF_ARGUMENTS = 6;

    public static final String STORY_CREATED_MESSAGE = "Story %s with ID %d has been created.";

    private static final String EXISTING_STORY = "Story with name '%s' already exists.";

    private final TaskManagementRepository taskManagementRepository;

    public CreateStoryCommand(TaskManagementRepository taskManagementRepository) {
        this.taskManagementRepository = taskManagementRepository;
    }


    @Override
    public String execute(List<String> parameters) {
        ValidationHelpers.validateArgumentsCount(parameters, EXPECTED_NUMBER_OF_ARGUMENTS);
        String description = parameters.get(0);
        String title = parameters.get(1);
        Size size = ParsingHelpers.tryParseEnum(parameters.get(2), Size.class);
        TaskPriority priority = ParsingHelpers.tryParseEnum(parameters.get(3), TaskPriority.class);
        String nameOfBoard = parameters.get(4);
        String nameOfTeam = parameters.get(5);
        return createStory(title, description, size, priority, nameOfBoard, nameOfTeam);

    }

    private String createStory(String title, String description, Size size, TaskPriority priority,
                               String nameOfBoard, String nameOfTeam) {
        if (taskManagementRepository.taskExists(title)) {
            throw new IllegalArgumentException(String.format(EXISTING_STORY, title));
        }
        Board board = taskManagementRepository.findBoardByName(nameOfBoard);
        Team team = taskManagementRepository.findTeamByName(nameOfTeam);
        taskManagementRepository.createStory(title, description, size, priority, board);
        Task story = taskManagementRepository.findTaskByName(taskManagementRepository.getTasks(), "Story", title);
        int id = story.getId();
        Eventlog eventlog = new EventLogImpl(String.format("'%s' Story with ID %d was assigned to the board '%s'", title, id, nameOfBoard));
        board.addEventLog(eventlog);
        team.addEventLog(eventlog);
        return String.format(STORY_CREATED_MESSAGE, title, id);
    }
}
