package com.telerikacademy.project.taskmanagementsystem.commands;

import com.telerikacademy.project.taskmanagementsystem.commands.contracts.Command;
import com.telerikacademy.project.taskmanagementsystem.core.contract.TaskManagementRepository;
import com.telerikacademy.project.taskmanagementsystem.models.team.contacts.Eventlog;
import com.telerikacademy.project.taskmanagementsystem.models.team.contacts.Team;
import com.telerikacademy.project.taskmanagementsystem.utils.ValidationHelpers;

import java.util.List;

public class ShowTeamsActivityCommand implements Command {

    private static final int EXPECTED_ARGUMENTS_COUNT = 1;
    private final TaskManagementRepository taskManagementRepository;

    private static final String BORDER_LINE = "==================================================================================================";

    public ShowTeamsActivityCommand(TaskManagementRepository taskManagementRepository) {
        this.taskManagementRepository = taskManagementRepository;
    }

    @Override
    public String execute(List<String> parameters) {
        ValidationHelpers.validateArgumentsCount(parameters, EXPECTED_ARGUMENTS_COUNT);

        String name = parameters.get(0);
        return showAllTeamsActivity(name);
    }

    private String showAllTeamsActivity(String name) {
        Team team = taskManagementRepository.findTeamByName(name);


        StringBuilder sb = new StringBuilder();
        sb.append(BORDER_LINE).append(System.lineSeparator());
        String start = "Team with name " + name + " has activity:\n";
        sb.append(start);
        int count = 1;
        for (Eventlog eventlog : team.getHistory()) {
            sb.append("  --").append(count).append(" ").append(eventlog.viewInfo()).append(System.lineSeparator());
            count++;
        }

        sb.append(BORDER_LINE);

        return sb.toString();

    }
}
