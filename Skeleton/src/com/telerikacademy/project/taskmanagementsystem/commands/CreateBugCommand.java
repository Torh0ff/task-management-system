package com.telerikacademy.project.taskmanagementsystem.commands;

import com.telerikacademy.project.taskmanagementsystem.commands.contracts.Command;
import com.telerikacademy.project.taskmanagementsystem.core.contract.TaskManagementRepository;
import com.telerikacademy.project.taskmanagementsystem.models.tasks.contracts.Task;
import com.telerikacademy.project.taskmanagementsystem.models.tasks.contracts.enums.TaskPriority;
import com.telerikacademy.project.taskmanagementsystem.models.tasks.contracts.enums.bug.Severity;
import com.telerikacademy.project.taskmanagementsystem.models.team.EventLogImpl;
import com.telerikacademy.project.taskmanagementsystem.models.team.contacts.Board;
import com.telerikacademy.project.taskmanagementsystem.models.team.contacts.Eventlog;
import com.telerikacademy.project.taskmanagementsystem.models.team.contacts.Team;
import com.telerikacademy.project.taskmanagementsystem.utils.ParsingHelpers;
import com.telerikacademy.project.taskmanagementsystem.utils.ValidationHelpers;

import java.util.List;

public class CreateBugCommand implements Command {

    public static final int EXPECTED_NUMBER_OF_ARGUMENTS = 7;
    public static final String BUG_CREATED_MESSAGE = "Bug %s with ID %d has been created.";

    private static final String EXISTING_BUG = "Bug with name '%s' already exists.";

    private final TaskManagementRepository taskManagementRepository;
    public CreateBugCommand(TaskManagementRepository taskManagementRepository) {
        this.taskManagementRepository = taskManagementRepository;
    }


    @Override
    public String execute(List<String> parameters) {
        ValidationHelpers.validateArgumentsCount(parameters, EXPECTED_NUMBER_OF_ARGUMENTS);
        String description = parameters.get(0);
        String steps = parameters.get(1);
        Severity severity = ParsingHelpers.tryParseEnum(parameters.get(2), Severity.class);
        TaskPriority priority = ParsingHelpers.tryParseEnum(parameters.get(3), TaskPriority.class);
        String nameOfBoard = parameters.get(4);
        String nameOfTeam = parameters.get(5);
        String title = parameters.get(6);
        String[] arrayOfSteps = steps.split(";");

        return createBug(title, description, severity, priority, nameOfBoard, nameOfTeam, arrayOfSteps);

    }

    private String createBug(String name, String description, Severity severity, TaskPriority taskPriority, String nameOfBoard, String nameOfTeam, String[] steps) {
        if (taskManagementRepository.taskExists(name)) {
            throw new IllegalArgumentException(String.format(EXISTING_BUG, name));
        }
        Board board = taskManagementRepository.findBoardByName(nameOfBoard);
        Team team = taskManagementRepository.findTeamByName(nameOfTeam);
        taskManagementRepository.createBug(name, description, severity, taskPriority, board, List.of(steps));
        Task bug = taskManagementRepository.findTaskByName(taskManagementRepository.getTasks(), "Bug", name);
        int id = bug.getId();
        Eventlog eventlog = new EventLogImpl(String.format("'%s' Bug with ID %d was assigned to the board '%s'.", name, id, nameOfBoard));
        board.addEventLog(eventlog);
        team.addEventLog(eventlog);
        return String.format(BUG_CREATED_MESSAGE, name, id);

    }
}
