package com.telerikacademy.project.taskmanagementsystem.commands;

import com.telerikacademy.project.taskmanagementsystem.commands.contracts.Command;
import com.telerikacademy.project.taskmanagementsystem.core.contract.TaskManagementRepository;
import com.telerikacademy.project.taskmanagementsystem.models.tasks.contracts.Task;
import com.telerikacademy.project.taskmanagementsystem.models.team.EventLogImpl;
import com.telerikacademy.project.taskmanagementsystem.models.team.contacts.Board;
import com.telerikacademy.project.taskmanagementsystem.models.team.contacts.Eventlog;
import com.telerikacademy.project.taskmanagementsystem.models.team.contacts.Team;
import com.telerikacademy.project.taskmanagementsystem.utils.ParsingHelpers;
import com.telerikacademy.project.taskmanagementsystem.utils.ValidationHelpers;

import java.util.List;

public class CreateFeedbackCommand implements Command {


    public static final int EXPECTED_NUMBER_OF_ARGUMENTS = 5;
    public static final String FEEDBACK_CREATED_MESSAGE = "Feedback %s with ID %d has been created.";

    private static final String EXISTING_FEEDBACK = "Feedback with name '%s' already exists.";
    private final TaskManagementRepository taskManagementRepository;

    public CreateFeedbackCommand(TaskManagementRepository taskManagementRepository) {
        this.taskManagementRepository = taskManagementRepository;
    }


    @Override
    public String execute(List<String> parameters) {
        ValidationHelpers.validateArgumentsCount(parameters, EXPECTED_NUMBER_OF_ARGUMENTS);
        String description = parameters.get(0);
        String title = parameters.get(1);
        int rating = ParsingHelpers.tryParseInteger(parameters.get(2), "rating");
        String nameOfBoard = parameters.get(3);
        String nameOfTeam = parameters.get(4);
        return createFeedback(title, description, rating, nameOfBoard, nameOfTeam);
    }

    private String createFeedback(String title, String description, int rating, String nameOfBoard, String nameOfTeam) {
        if (taskManagementRepository.taskExists(title)) {
            throw new IllegalArgumentException(String.format(EXISTING_FEEDBACK, title));
        }
        Board board = taskManagementRepository.findBoardByName(nameOfBoard);
        Team team = taskManagementRepository.findTeamByName(nameOfTeam);
        taskManagementRepository.createFeedback(title, description, rating, board);
        Task feedback = taskManagementRepository.findTaskByName(taskManagementRepository.getTasks(), "Feedback", title);
        int id = feedback.getId();
        Eventlog eventlog = new EventLogImpl(String.format("'%s' Feedback with ID %d was assigned to the board '%s'.", title, id, nameOfBoard));
        board.addEventLog(eventlog);
        team.addEventLog(eventlog);
        return String.format(FEEDBACK_CREATED_MESSAGE, title, id);

    }
}
