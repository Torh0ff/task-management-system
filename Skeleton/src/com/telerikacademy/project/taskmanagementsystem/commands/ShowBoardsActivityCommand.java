package com.telerikacademy.project.taskmanagementsystem.commands;

import com.telerikacademy.project.taskmanagementsystem.commands.contracts.Command;
import com.telerikacademy.project.taskmanagementsystem.core.contract.TaskManagementRepository;
import com.telerikacademy.project.taskmanagementsystem.models.team.contacts.Board;
import com.telerikacademy.project.taskmanagementsystem.models.team.contacts.Eventlog;
import com.telerikacademy.project.taskmanagementsystem.utils.ValidationHelpers;

import java.util.List;

public class ShowBoardsActivityCommand implements Command {
    private static final int EXPECTED_ARGUMENTS_COUNT = 1;
    private static final String EMPTY_BOARDS_ACTIVITY = "Board %s has no activities.";

    private static final String BORDER_LINE = "==================================================================================================";

    private  final TaskManagementRepository taskManagementRepository;

    public ShowBoardsActivityCommand(TaskManagementRepository taskManagementRepository) {
        this.taskManagementRepository = taskManagementRepository;
    }

    @Override
    public String execute(List<String> parameters) {
        ValidationHelpers.validateArgumentsCount(parameters, EXPECTED_ARGUMENTS_COUNT);
        String boardName = parameters.get(0);

        return showBoardActivity(boardName);
    }

    private String showBoardActivity(String boardName) {
        Board board = taskManagementRepository.findBoardByName(boardName);



        StringBuilder sb = new StringBuilder();
        sb.append(BORDER_LINE).append(System.lineSeparator());
        String start = "Board with name " + boardName + " has activity:\n";
        sb.append(start);
        int count = 1;
        for (Eventlog eventlog : board.getLogs()) {
            sb.append("  --").append(count).append(" ").append(eventlog.viewInfo()).append(System.lineSeparator());
            count++;
        }

        sb.append(BORDER_LINE);

        return sb.toString();
    }
}
