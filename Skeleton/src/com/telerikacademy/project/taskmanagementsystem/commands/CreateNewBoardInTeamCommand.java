package com.telerikacademy.project.taskmanagementsystem.commands;

import com.telerikacademy.project.taskmanagementsystem.commands.contracts.Command;
import com.telerikacademy.project.taskmanagementsystem.core.contract.TaskManagementRepository;
import com.telerikacademy.project.taskmanagementsystem.models.team.EventLogImpl;
import com.telerikacademy.project.taskmanagementsystem.models.team.contacts.Board;
import com.telerikacademy.project.taskmanagementsystem.models.team.contacts.Eventlog;
import com.telerikacademy.project.taskmanagementsystem.models.team.contacts.Team;
import com.telerikacademy.project.taskmanagementsystem.utils.ValidationHelpers;

import java.util.List;

public class CreateNewBoardInTeamCommand implements Command {

    private static final int EXPECTED_ARGUMENTS_COUNT = 2;
    private static final String BOARD_WAS_ADDED_TO_TEAM = "%s board was created in %s team.";

    private static final String EXISTING_BOARD = "Board with name '%s' already exists.";
    private final TaskManagementRepository taskManagementRepository;

    public CreateNewBoardInTeamCommand(TaskManagementRepository taskManagementRepository) {
        this.taskManagementRepository = taskManagementRepository;
    }

    @Override
    public String execute(List<String> parameters) {
        ValidationHelpers.validateArgumentsCount(parameters, EXPECTED_ARGUMENTS_COUNT);
        String boardName = parameters.get(0);
        String teamName = parameters.get(1);


        return createNewBoardInTeam(boardName, teamName);
    }

    private String createNewBoardInTeam(String boardName, String teamName) {
        if (taskManagementRepository.boardExists(boardName)) {
            throw new IllegalArgumentException(String.format(EXISTING_BOARD, boardName));
        }
        if (taskManagementRepository.getBoards().isEmpty()){
            taskManagementRepository.createBoard(boardName);
        }
            taskManagementRepository.createBoard(boardName);
            Board board = taskManagementRepository.findBoardByName(boardName);
            Team team = taskManagementRepository.findTeamByName(teamName);
            team.addBoard(board);
            Eventlog eventlog = new EventLogImpl(String.format("'%s' board was created in '%s' team.", boardName, teamName));
            board.addEventLog(eventlog);
            team.addEventLog(eventlog);

        return String.format(BOARD_WAS_ADDED_TO_TEAM, boardName, teamName);
    }
}
