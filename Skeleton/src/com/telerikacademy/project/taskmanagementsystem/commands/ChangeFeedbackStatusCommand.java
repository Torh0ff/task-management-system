package com.telerikacademy.project.taskmanagementsystem.commands;

import com.telerikacademy.project.taskmanagementsystem.commands.contracts.Command;
import com.telerikacademy.project.taskmanagementsystem.core.contract.TaskManagementRepository;
import com.telerikacademy.project.taskmanagementsystem.models.tasks.contracts.Feedback;
import com.telerikacademy.project.taskmanagementsystem.models.tasks.contracts.enums.feedback.FeedbackStatus;
import com.telerikacademy.project.taskmanagementsystem.models.team.EventLogImpl;
import com.telerikacademy.project.taskmanagementsystem.models.team.contacts.Eventlog;
import com.telerikacademy.project.taskmanagementsystem.utils.ParsingHelpers;
import com.telerikacademy.project.taskmanagementsystem.utils.ValidationHelpers;

import java.util.List;

public class ChangeFeedbackStatusCommand implements Command {


    public static final int EXPECTED_NUMBER_OF_ARGUMENTS_CHANGE = 2;
    public static final String INVALID_ID_PARAMETER = "Invalid input, ID number has to be an integer";
    public static final String FEEDBACK_STATUS_CHANGED = "Status of feedback '%s' with ID %d has been changed to '%s'.";
    private static final String ALREADY_HAS_THIS_STATUS = "Feedback with title %s has already status %s.";
    private final TaskManagementRepository taskManagementRepository;

    public ChangeFeedbackStatusCommand(TaskManagementRepository taskManagementRepository) {
        this.taskManagementRepository = taskManagementRepository;
    }


    @Override
    public String execute(List<String> parameters) {
        ValidationHelpers.validateArgumentsCount(parameters, EXPECTED_NUMBER_OF_ARGUMENTS_CHANGE);
        int feedbackId = ParsingHelpers.tryParseInteger(parameters.get(0), INVALID_ID_PARAMETER);
        FeedbackStatus status = ParsingHelpers.tryParseEnum(parameters.get(1), FeedbackStatus.class);

        return changedFeedback(feedbackId, status);
    }

    private String changedFeedback(int feedbackId, FeedbackStatus status) {
        Feedback feedback = (Feedback) taskManagementRepository.findTaskById(taskManagementRepository.getTasks(), feedbackId);
        if (feedback.getStatus().equals(status.toString())){
            throw new IllegalArgumentException(String.format(ALREADY_HAS_THIS_STATUS, feedback.getTitle(),status));
        }
        feedback.setStatus(status);
        Eventlog eventlog = new EventLogImpl(String.format(FEEDBACK_STATUS_CHANGED, feedback.getTitle(), feedbackId, status));
        feedback.addEventLog(eventlog);
        return String.format(FEEDBACK_STATUS_CHANGED, feedback.getTitle(), feedbackId, status);

    }
}

