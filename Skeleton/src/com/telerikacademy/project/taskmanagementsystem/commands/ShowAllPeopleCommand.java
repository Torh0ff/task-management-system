package com.telerikacademy.project.taskmanagementsystem.commands;

import com.telerikacademy.project.taskmanagementsystem.commands.contracts.Command;
import com.telerikacademy.project.taskmanagementsystem.core.contract.TaskManagementRepository;
import com.telerikacademy.project.taskmanagementsystem.models.team.contacts.Person;
import com.telerikacademy.project.taskmanagementsystem.utils.ValidationHelpers;

import java.util.List;

public class ShowAllPeopleCommand implements Command {
    public static final int EXPECTED_PARAMETERS_COUNT = 0;
    private static final String EMPTY_LIST_PEOPLE = "There are no people added!";

    private final TaskManagementRepository taskManagementRepository;

    public ShowAllPeopleCommand(TaskManagementRepository taskManagementRepository) {
        this.taskManagementRepository = taskManagementRepository;
    }

    @Override
    public String execute(List<String> parameters) {
        ValidationHelpers.validateArgumentsCount(parameters, EXPECTED_PARAMETERS_COUNT);

        return showPeople();
    }

    private String showPeople() {

        if (taskManagementRepository.getPeople().isEmpty()){
            throw new IllegalArgumentException(EMPTY_LIST_PEOPLE);
        }
        StringBuilder sb = new StringBuilder();
        int count = 1;
        sb.append("People----").append(System.lineSeparator());
        for (Person person : taskManagementRepository.getPeople()) {
            sb.append("   --").append(count).append(". ").append(person.getName()).append(System.lineSeparator());
            count++;
        }
        sb.append("==========");

        return sb.toString();
    }
}
