package com.telerikacademy.project.taskmanagementsystem.commands;

import com.telerikacademy.project.taskmanagementsystem.commands.contracts.Command;
import com.telerikacademy.project.taskmanagementsystem.core.contract.TaskManagementRepository;
import com.telerikacademy.project.taskmanagementsystem.models.tasks.contracts.Bug;
import com.telerikacademy.project.taskmanagementsystem.models.tasks.contracts.enums.bug.Severity;
import com.telerikacademy.project.taskmanagementsystem.models.team.EventLogImpl;
import com.telerikacademy.project.taskmanagementsystem.models.team.contacts.Eventlog;
import com.telerikacademy.project.taskmanagementsystem.utils.ParsingHelpers;
import com.telerikacademy.project.taskmanagementsystem.utils.ValidationHelpers;

import java.util.List;

public class ChangeBugSeverityCommand implements Command {

    public static final int EXPECTED_NUMBER_OF_ARGUMENTS_CHANGE = 2;
    public static final String INVALID_ID_PARAMETER = "Invalid input, ID number has to be an integer";
    public static final String BUG_SEVERITY_CHANGED = "Severity of bug '%s' with ID %d has been changed to '%s'.";
    private final TaskManagementRepository taskManagementRepository;

    public ChangeBugSeverityCommand(TaskManagementRepository taskManagementRepository) {
        this.taskManagementRepository = taskManagementRepository;
    }

    @Override
    public String execute(List<String> parameters) {
        ValidationHelpers.validateArgumentsCount(parameters, EXPECTED_NUMBER_OF_ARGUMENTS_CHANGE);
        int bugId = ParsingHelpers.tryParseInteger(parameters.get(0), INVALID_ID_PARAMETER);
        Severity severity = ParsingHelpers.tryParseEnum(parameters.get(1), Severity.class);

        return changedSeverity(bugId, severity);

    }

    private String changedSeverity(int bugId, Severity severity) {
        Bug bug = (Bug) taskManagementRepository.findTaskById(taskManagementRepository.getTasks(), bugId);
        bug.setSeverity(severity);
        Eventlog eventlog = new EventLogImpl(String.format(BUG_SEVERITY_CHANGED, bug.getTitle(), bugId, severity));
        bug.addEventLog(eventlog);
        return String.format(BUG_SEVERITY_CHANGED, bug.getTitle(), bugId, severity);

    }
}
