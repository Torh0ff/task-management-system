package com.telerikacademy.project.taskmanagementsystem.commands;



import com.telerikacademy.project.taskmanagementsystem.commands.contracts.Command;
import com.telerikacademy.project.taskmanagementsystem.core.contract.TaskManagementRepository;
import com.telerikacademy.project.taskmanagementsystem.utils.ValidationHelpers;

import java.util.List;

public class CreateTeamCommand implements Command {
    private static final int ARGUMENTS_COUNT = 1;
    private static final String EXISTING_TEAM = "Team with name '%s' already exists.";
    private static final String TEAM_CREATED = "Created Team with name '%s'.";

    private final TaskManagementRepository taskManagementRepository;

    public CreateTeamCommand(TaskManagementRepository taskManagementRepository) {
        this.taskManagementRepository = taskManagementRepository;
    }

    @Override
    public String execute(List<String> parameters) {
        ValidationHelpers.validateArgumentsCount(parameters, ARGUMENTS_COUNT);

        String name = parameters.get(0);

        return createTeam(name);
    }

    private String createTeam(String name) {
        if (taskManagementRepository.teamExists(name)){
            throw new IllegalArgumentException(String.format(EXISTING_TEAM, name));
        }

        taskManagementRepository.createTeam(name);

        return String.format(TEAM_CREATED, name);

    }
}
