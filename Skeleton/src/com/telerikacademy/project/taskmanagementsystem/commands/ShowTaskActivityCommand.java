package com.telerikacademy.project.taskmanagementsystem.commands;

import com.telerikacademy.project.taskmanagementsystem.commands.contracts.Command;
import com.telerikacademy.project.taskmanagementsystem.core.contract.TaskManagementRepository;
import com.telerikacademy.project.taskmanagementsystem.models.tasks.contracts.Task;
import com.telerikacademy.project.taskmanagementsystem.models.team.contacts.Eventlog;
import com.telerikacademy.project.taskmanagementsystem.utils.ParsingHelpers;
import com.telerikacademy.project.taskmanagementsystem.utils.ValidationHelpers;

import java.util.List;

public class ShowTaskActivityCommand implements Command {

    private static final int EXPECTED_ARGUMENTS_COUNT = 1;
    private static final String EMPTY_TASK_ACTIVITY = "Task with ID:%d has no activities.";
    private static final String BORDER_LINE = "==================================================================================================";

    private static final String START_MESSAGE = "Task with name '%s' has activity: %n";


    private final TaskManagementRepository taskManagementRepository;

    public ShowTaskActivityCommand(TaskManagementRepository taskManagementRepository) {
        this.taskManagementRepository = taskManagementRepository;
    }

    @Override
    public String execute(List<String> parameters) {
        ValidationHelpers.validateArgumentsCount(parameters, EXPECTED_ARGUMENTS_COUNT);
        int taskId = ParsingHelpers.tryParseInteger(parameters.get(0), "task id");

        return showTaskActivity(taskId);
    }

    private String showTaskActivity(int taskId) {

        Task bug = taskManagementRepository.findTaskById(taskManagementRepository.getTasks(), taskId);

        StringBuilder sb = new StringBuilder();

        sb.append(BORDER_LINE).append(System.lineSeparator());
        sb.append(String.format(START_MESSAGE, bug.getTitle()));
        int count = 1;
        for (Eventlog eventlog : bug.getLogs()) {
            sb.append("  --").append(count).append(" ").append(eventlog.viewInfo()).append(System.lineSeparator());
            count++;
        }
        sb.append(BORDER_LINE);
        return sb.toString();

    }
}