package com.telerikacademy.project.taskmanagementsystem.commands;

import com.telerikacademy.project.taskmanagementsystem.commands.contracts.Command;
import com.telerikacademy.project.taskmanagementsystem.core.contract.TaskManagementRepository;
import com.telerikacademy.project.taskmanagementsystem.models.tasks.contracts.Bug;
import com.telerikacademy.project.taskmanagementsystem.models.tasks.contracts.Story;
import com.telerikacademy.project.taskmanagementsystem.models.tasks.contracts.enums.bug.Severity;
import com.telerikacademy.project.taskmanagementsystem.models.tasks.contracts.enums.story.Size;
import com.telerikacademy.project.taskmanagementsystem.models.team.EventLogImpl;
import com.telerikacademy.project.taskmanagementsystem.models.team.contacts.Eventlog;
import com.telerikacademy.project.taskmanagementsystem.utils.ParsingHelpers;
import com.telerikacademy.project.taskmanagementsystem.utils.ValidationHelpers;

import java.util.List;

public class ChangeSizeCommand implements Command {


    public static final int EXPECTED_NUMBER_OF_ARGUMENTS_CHANGE = 2;
    public static final String INVALID_ID_PARAMETER = "Invalid input, ID number has to be an integer";
    public static final String STORY_SIZE_CHANGED = "Size of Story '%s' with ID %d has been changed to '%s'.";
    private final TaskManagementRepository taskManagementRepository;

    public ChangeSizeCommand(TaskManagementRepository taskManagementRepository) {
        this.taskManagementRepository = taskManagementRepository;
    }

    @Override
    public String execute(List<String> parameters) {
        ValidationHelpers.validateArgumentsCount(parameters, EXPECTED_NUMBER_OF_ARGUMENTS_CHANGE);
        int bugId = ParsingHelpers.tryParseInteger(parameters.get(0), INVALID_ID_PARAMETER);
        Size size = ParsingHelpers.tryParseEnum(parameters.get(1), Size.class);

        return changedSize(bugId, size);

    }

    private String changedSize(int bugId, Size size) {
        Story story = (Story) taskManagementRepository.findTaskById(taskManagementRepository.getTasks(), bugId);
        story.setSize(size);
        Eventlog eventlog = new EventLogImpl(String.format(STORY_SIZE_CHANGED, story.getTitle(), bugId, size));
        story.addEventLog(eventlog);
        return String.format(STORY_SIZE_CHANGED, story.getTitle(), bugId, size);

    }
}
