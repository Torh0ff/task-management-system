package com.telerikacademy.project.taskmanagementsystem.commands;


import com.telerikacademy.project.taskmanagementsystem.commands.contracts.Command;
import com.telerikacademy.project.taskmanagementsystem.core.contract.TaskManagementRepository;
import com.telerikacademy.project.taskmanagementsystem.utils.ValidationHelpers;

import java.util.List;

public class CreatePersonCommand implements Command {

    public static final int EXPECTED_PARAMETERS_COUNT = 1;

    private static final String PERSON_EXIST_MESSAGE = "User '%s' already exist.";
    private static final String PERSON_CREATED = "User with name '%s' was created!";


    private final TaskManagementRepository taskManagementRepository;

    public CreatePersonCommand(TaskManagementRepository taskManagementRepository) {
        this.taskManagementRepository = taskManagementRepository;
    }

    @Override
    public String execute(List<String> parameters) {
        ValidationHelpers.validateArgumentsCount(parameters, EXPECTED_PARAMETERS_COUNT);

        String personName = parameters.get(0);
        return createPerson(personName);
    }

    private String createPerson(String personName) {

        if (taskManagementRepository.personExists(personName)){
            throw new IllegalArgumentException(String.format(PERSON_EXIST_MESSAGE, personName));
        }

        taskManagementRepository.createPerson(personName);
        return String.format(PERSON_CREATED, personName);
    }
}
