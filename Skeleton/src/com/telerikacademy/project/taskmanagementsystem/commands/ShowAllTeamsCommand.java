package com.telerikacademy.project.taskmanagementsystem.commands;

import com.telerikacademy.project.taskmanagementsystem.commands.contracts.Command;
import com.telerikacademy.project.taskmanagementsystem.core.contract.TaskManagementRepository;
import com.telerikacademy.project.taskmanagementsystem.models.team.contacts.Team;
import com.telerikacademy.project.taskmanagementsystem.utils.ValidationHelpers;

import java.util.List;

public class ShowAllTeamsCommand implements Command {

    private static final int EXPECTED_ARGUMENTS = 0;

    private static final String BORDER_LINE = "==================================================================================================";
    private static final String EMPTY_LIST_OF_TEAMS = "There are no added teams.";
    private final TaskManagementRepository taskManagementRepository;

    public ShowAllTeamsCommand(TaskManagementRepository taskManagementRepository) {
        this.taskManagementRepository = taskManagementRepository;
    }

    @Override
    public String execute(List<String> parameters) {
        ValidationHelpers.validateArgumentsCount(parameters, EXPECTED_ARGUMENTS);

        return showAllTeams();
    }

    private String showAllTeams() {
        if (taskManagementRepository.getTeams().isEmpty()){
            throw new IllegalArgumentException(EMPTY_LIST_OF_TEAMS);
        }

        StringBuilder sb = new StringBuilder();
        sb.append(BORDER_LINE).append(System.lineSeparator());
        sb.append("Teams----").append(System.lineSeparator());
        int count = 1;
        for (Team team : taskManagementRepository.getTeams()) {
            sb.append(" --").append(count).append(". ").append(team.getName()).append(System.lineSeparator());
            count++;
        }
        sb.append(BORDER_LINE);

        return sb.toString();
    }
}
