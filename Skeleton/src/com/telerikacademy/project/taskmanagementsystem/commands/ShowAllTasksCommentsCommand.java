package com.telerikacademy.project.taskmanagementsystem.commands;

import com.telerikacademy.project.taskmanagementsystem.commands.contracts.Command;
import com.telerikacademy.project.taskmanagementsystem.core.contract.TaskManagementRepository;
import com.telerikacademy.project.taskmanagementsystem.models.tasks.contracts.Task;
import com.telerikacademy.project.taskmanagementsystem.utils.ValidationHelpers;

import java.util.List;

public class ShowAllTasksCommentsCommand implements Command {
    private static final int EXPECTED_ARGUMENTS_COUNT = 1;

    private static final String BORDER_LINE = "==================================================================================================";
    private  final TaskManagementRepository taskManagementRepository;

    public ShowAllTasksCommentsCommand(TaskManagementRepository taskManagementRepository) {
        this.taskManagementRepository = taskManagementRepository;
    }

    @Override
    public String execute(List<String> parameters) {
        ValidationHelpers.validateArgumentsCount(parameters, EXPECTED_ARGUMENTS_COUNT);
        String taskName = parameters.get(0);

        return ShowAllTasksComments(taskName);
    }

    private String ShowAllTasksComments(String taskName) {
        Task task = taskManagementRepository.findTaskByName(taskManagementRepository.getTasks(), "Task", taskName);

        StringBuilder sb = new StringBuilder();
        sb.append(BORDER_LINE).append(System.lineSeparator());
        sb.append(String.format(" Task:'%s'%n ID:%d%n ", task.getTitle(), task.getId()));
        sb.append(task.printComments());
        sb.append(BORDER_LINE);
        return sb.toString();
    }
}
