package com.telerikacademy.project.taskmanagementsystem.commands.listings.listingAndSorting;

import com.telerikacademy.project.taskmanagementsystem.commands.contracts.Command;
import com.telerikacademy.project.taskmanagementsystem.core.contract.TaskManagementRepository;
import com.telerikacademy.project.taskmanagementsystem.models.tasks.contracts.Task;
import com.telerikacademy.project.taskmanagementsystem.utils.ValidationHelpers;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

//List tasks with assignee.
//Filter by assignee
public class ListAllTasksWithAndFilteredByAssigneeCommand implements Command {
    private static final int ARGUMENTS_COUNT = 2;
    private static final String NO_TASKS_WITH_THIS_ASSIGNEE = "%s assignee has no tasks";
    private static final String BORDER_LINE =
            "==================================================================================================";


    private final TaskManagementRepository taskManagementRepository;

    public ListAllTasksWithAndFilteredByAssigneeCommand(TaskManagementRepository taskManagementRepository) {
        this.taskManagementRepository = taskManagementRepository;
    }

    @Override
    public String execute(List<String> parameters) {
        ValidationHelpers.validateArgumentsCount(parameters, ARGUMENTS_COUNT);
        String teamName = parameters.get(0);
        String assignee = parameters.get(1);

        List<Task> result = taskManagementRepository.findPersonByName(assignee).getTasks()
                .stream()
                .sorted(Comparator.comparing(Task::getTitle))
                .collect(Collectors.toList());

        if (result.isEmpty()){
            throw new IllegalArgumentException(String.format(NO_TASKS_WITH_THIS_ASSIGNEE, assignee));
        }
        StringBuilder sb = new StringBuilder();
        sb.append(BORDER_LINE).append(System.lineSeparator());
        sb.append(String.format("Tasks assigned to: %s(Team %s's) are:\n", assignee, teamName));

        for (Task task : result) {
            sb.append(" ---").append(task.getTitle()).append(System.lineSeparator());
        }
        sb.append(BORDER_LINE);
        return sb.toString();
    }
}
