package com.telerikacademy.project.taskmanagementsystem.commands.listings.listingAndSorting;

import com.telerikacademy.project.taskmanagementsystem.commands.contracts.Command;
import com.telerikacademy.project.taskmanagementsystem.core.contract.TaskManagementRepository;
import com.telerikacademy.project.taskmanagementsystem.models.tasks.contracts.Task;
import com.telerikacademy.project.taskmanagementsystem.utils.ValidationHelpers;

import java.util.List;
import java.util.stream.Collectors;

//List bugs/stories/feedback only.
//· Filter by assignee
public class ListTaskFilteredByAssigneeCommand implements Command {
    private static final int ARGUMENTS_COUNT = 2;
    private static final String EMPTY_ASSIGNEES_TASKS = "Assignee %s has no %s tasks.";
    private static final String BORDER_LINE =
            "==================================================================================================";
    private static final String NO_SUCH_TASK = "Task with name %s does not exist.";


    private final TaskManagementRepository taskManagementRepository;

    public ListTaskFilteredByAssigneeCommand(TaskManagementRepository taskManagementRepository) {
        this.taskManagementRepository = taskManagementRepository;
    }

    @Override
    public String execute(List<String> parameters) {
        ValidationHelpers.validateArgumentsCount(parameters, 2);
        String taskType = parameters.get(0);
        String assignee = parameters.get(1);

        if (!taskType.equals("Bug") && !taskType.equals("Feedback") && !taskType.equals("Story")){
            throw new IllegalArgumentException(String.format(NO_SUCH_TASK, taskType));
        }

        List<Task> tasks = taskManagementRepository.getTasks()
                .stream()
                .filter(t -> t.getClass().getInterfaces()[0].getSimpleName().equals(taskType))
                .filter(t -> t.getAssignee().equals(assignee))
                .collect(Collectors.toList());

        if (tasks.isEmpty()){
            throw new IllegalArgumentException(String.format(EMPTY_ASSIGNEES_TASKS, assignee, taskType));
        }
        StringBuilder sb = new StringBuilder();
        sb.append(BORDER_LINE).append(System.lineSeparator());
        sb.append(String.format("Assignee %s have %s tasks: \n", assignee, taskType));
        for (Task task : tasks) {
            sb.append(" ---").append(task.getTitle()).append(System.lineSeparator());
        }
        sb.append(BORDER_LINE);


        return sb.toString();
    }

}

