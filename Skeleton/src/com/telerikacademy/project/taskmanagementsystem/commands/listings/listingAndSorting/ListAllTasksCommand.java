package com.telerikacademy.project.taskmanagementsystem.commands.listings.listingAndSorting;

import com.telerikacademy.project.taskmanagementsystem.commands.contracts.Command;
import com.telerikacademy.project.taskmanagementsystem.core.contract.TaskManagementRepository;
import com.telerikacademy.project.taskmanagementsystem.models.tasks.contracts.Task;
import com.telerikacademy.project.taskmanagementsystem.models.team.contacts.Board;
import com.telerikacademy.project.taskmanagementsystem.utils.ValidationHelpers;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
//List all tasks (display the most important info).
public class ListAllTasksCommand implements Command {

    private static final int ARGUMENTS_COUNT = 1;
    private static final String NO_TASKS_WITH_TITLE = "There are no tasks with title %s";
    private static final String BORDER_LINE =
            "==================================================================================================";

    private final TaskManagementRepository taskManagementRepository;

    public ListAllTasksCommand(TaskManagementRepository taskManagementRepository) {
        this.taskManagementRepository = taskManagementRepository;
    }

    @Override
    public String execute(List<String> parameters) {
        ValidationHelpers.validateArgumentsCount(parameters, ARGUMENTS_COUNT);
        String title = parameters.get(0);
        List<Task> result = taskManagementRepository.getTasks()
                .stream()
                .filter(t -> t.getTitle().contains(title))
                .sorted(Comparator.comparing(Task::getTitle))
                .collect(Collectors.toList());

        if (result.isEmpty()){
            throw new IllegalArgumentException(String.format(NO_TASKS_WITH_TITLE, title));
        }
        StringBuilder sb = new StringBuilder();
        sb.append(BORDER_LINE).append(System.lineSeparator());
        sb.append(String.format("Tasks with title containing '%s' are: \n", title));

        for (Task task : result) {
            sb.append(" ---").append("Id: ").append(task.getId()).append(", Title: ").append(task.getTitle())
                    .append(", Assigned to: ").append(task.getAssignee())
                    .append(System.lineSeparator());
        }
        sb.append(BORDER_LINE);
        return sb.toString();
    }
}
