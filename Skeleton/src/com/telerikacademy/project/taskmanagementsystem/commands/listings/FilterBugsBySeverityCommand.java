package com.telerikacademy.project.taskmanagementsystem.commands.listings;

import com.telerikacademy.project.taskmanagementsystem.commands.contracts.Command;
import com.telerikacademy.project.taskmanagementsystem.core.contract.TaskManagementRepository;
import com.telerikacademy.project.taskmanagementsystem.models.tasks.contracts.Bug;
import com.telerikacademy.project.taskmanagementsystem.models.tasks.contracts.enums.bug.Severity;
import com.telerikacademy.project.taskmanagementsystem.utils.ParsingHelpers;
import com.telerikacademy.project.taskmanagementsystem.utils.ValidationHelpers;

import java.util.List;
import java.util.stream.Collectors;

public class FilterBugsBySeverityCommand implements Command {
    public static final int EXPECTED_NUMBER_OF_ARGUMENTS = 1;

    private final TaskManagementRepository taskManagementRepository;
    public FilterBugsBySeverityCommand(TaskManagementRepository taskManagementRepository) {
        this.taskManagementRepository = taskManagementRepository;
    }
    @Override
    public String execute(List<String> parameters) {
        ValidationHelpers.validateArgumentsCount(parameters, EXPECTED_NUMBER_OF_ARGUMENTS);
        Severity severity = ParsingHelpers.tryParseEnum(parameters.get(0), Severity.class);
        return filterBugsBySeverity(severity);
    }private String filterBugsBySeverity(Severity severity) {
        StringBuilder stringBuilder = new StringBuilder();
        List<Bug> output = taskManagementRepository.getBugs().stream()
                .filter(bug -> bug.getSeverity().equals(severity.toString()))
                .collect(Collectors.toList());

        if (output.size() == 0){
            throw new IllegalArgumentException(String.format("There are no bugs with severity '%s'%n----------------------------------------------", severity.toString()));}

        stringBuilder.append(String.format("Bugs filtered by '%s' severity:%n", severity));
        for (Bug b : output) {
            stringBuilder.append(b.print());
        }
        return stringBuilder.toString();}}