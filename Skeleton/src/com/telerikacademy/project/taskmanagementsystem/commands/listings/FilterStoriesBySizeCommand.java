package com.telerikacademy.project.taskmanagementsystem.commands.listings;

import com.telerikacademy.project.taskmanagementsystem.commands.contracts.Command;
import com.telerikacademy.project.taskmanagementsystem.core.contract.TaskManagementRepository;
import com.telerikacademy.project.taskmanagementsystem.models.tasks.contracts.Story;
import com.telerikacademy.project.taskmanagementsystem.models.tasks.contracts.enums.story.Size;
import com.telerikacademy.project.taskmanagementsystem.utils.ParsingHelpers;
import com.telerikacademy.project.taskmanagementsystem.utils.ValidationHelpers;

import java.util.List;
import java.util.stream.Collectors;

public class FilterStoriesBySizeCommand implements Command {
    public static final int EXPECTED_NUMBER_OF_ARGUMENTS = 1;
    private final TaskManagementRepository taskManagementRepository;

    public FilterStoriesBySizeCommand(TaskManagementRepository taskManagementRepository) {
        this.taskManagementRepository = taskManagementRepository;
    }


    @Override
    public String execute(List<String> parameters) {
        ValidationHelpers.validateArgumentsCount(parameters, EXPECTED_NUMBER_OF_ARGUMENTS);
        Size size = ParsingHelpers.tryParseEnum(parameters.get(0), Size.class);

        return filterStoriesBySize(size);
    }

    private String filterStoriesBySize(Size size) {

        StringBuilder stringBuilder = new StringBuilder();
        List<Story> output = taskManagementRepository.getStories().stream()
                .filter(story -> story.getSize().equals(size.toString()))
                .collect(Collectors.toList());

        if (output.size() == 0) {
            throw new IllegalArgumentException(String.format("There are no stories with size '%s'%n----------------------------------------------", size.toString()));
        }
        stringBuilder.append(String.format("Stories filtered by '%s' size:%n", size));
        for (Story b : output) {
            stringBuilder.append(b.print());
        }

        return stringBuilder.toString();
    }
}