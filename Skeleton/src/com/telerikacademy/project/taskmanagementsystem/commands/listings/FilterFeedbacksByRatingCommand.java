package com.telerikacademy.project.taskmanagementsystem.commands.listings;

import com.telerikacademy.project.taskmanagementsystem.commands.contracts.Command;
import com.telerikacademy.project.taskmanagementsystem.core.contract.TaskManagementRepository;
import com.telerikacademy.project.taskmanagementsystem.models.tasks.contracts.Feedback;
import com.telerikacademy.project.taskmanagementsystem.utils.ParsingHelpers;
import com.telerikacademy.project.taskmanagementsystem.utils.ValidationHelpers;

import java.util.List;
import java.util.stream.Collectors;

public class FilterFeedbacksByRatingCommand implements Command {

    public static final int EXPECTED_NUMBER_OF_ARGUMENTS = 1;
    private final TaskManagementRepository taskManagementRepository;
    public FilterFeedbacksByRatingCommand(TaskManagementRepository taskManagementRepository) {
        this.taskManagementRepository = taskManagementRepository;
    }


    @Override
    public String execute(List<String> parameters) {
        ValidationHelpers.validateArgumentsCount(parameters, EXPECTED_NUMBER_OF_ARGUMENTS);
        int rating = ParsingHelpers.tryParseInteger(parameters.get(0), "rating");

        return filterFeedbackByRating(rating);
    }

    private String filterFeedbackByRating(int rating) {

        StringBuilder stringBuilder = new StringBuilder();
        List<Feedback> output = taskManagementRepository.getFeedbacks().stream()
                .filter(feedback -> feedback.getRating() == rating)
                .collect(Collectors.toList());

        if (output.size() == 0){
            throw new IllegalArgumentException(String.format("There are no feedbacks with rating '%d'%n----------------------------------------------", rating));
        }
        stringBuilder.append(String.format("Feedbacks filtered by '%d' points rating:%n", rating));
        for (Feedback b : output) {
            stringBuilder.append(b.print());
        }
        return stringBuilder.toString();
    }
}