package com.telerikacademy.project.taskmanagementsystem.commands.listings.listingAndSorting;

import com.telerikacademy.project.taskmanagementsystem.commands.contracts.Command;
import com.telerikacademy.project.taskmanagementsystem.core.contract.TaskManagementRepository;
import com.telerikacademy.project.taskmanagementsystem.models.tasks.contracts.Bug;
import com.telerikacademy.project.taskmanagementsystem.models.tasks.contracts.Feedback;
import com.telerikacademy.project.taskmanagementsystem.models.tasks.contracts.Story;
import com.telerikacademy.project.taskmanagementsystem.models.tasks.contracts.Task;
import com.telerikacademy.project.taskmanagementsystem.models.tasks.contracts.enums.TaskPriority;
import com.telerikacademy.project.taskmanagementsystem.models.tasks.contracts.enums.bug.Severity;
import com.telerikacademy.project.taskmanagementsystem.models.tasks.contracts.enums.story.Size;
import com.telerikacademy.project.taskmanagementsystem.utils.ParsingHelpers;
import com.telerikacademy.project.taskmanagementsystem.utils.ValidationHelpers;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

//Sort by title/priority/severity/size/rating (depending on the task type)
public class ListTasksSortedByCommand implements Command {

    private static final int ARGUMENTS_COUNT = 3;
    private static final String NO_SUCH_SORTING_OPTION = "Option %s is not supported";
    private static final String BORDER_LINE =
            "==================================================================================================";
    private static final String EMPTY_BUG_TASKS = "There are no bug tasks with priority %s.";
    private static final String EMPTY_STORY_TASKS = "There are no story tasks with priority %s.";
    private static final String EMPTY_TASK = "There are no tasks with title %s";
    private static final String EMPTY_FEEDBACK_TASKS = "There are no feedback tasks with status %s.";
    private static final String NO_SUCH_TASK = "Task with name %s does not exist.";
    private static final String NO_SORTING_OPTION = "Task %s has no %s options.";

    private final TaskManagementRepository taskManagementRepository;

    public ListTasksSortedByCommand(TaskManagementRepository taskManagementRepository) {
        this.taskManagementRepository = taskManagementRepository;
    }

    @Override
    public String execute(List<String> parameters) {
        ValidationHelpers.validateArgumentsCount(parameters, ARGUMENTS_COUNT);
        String typeTask = parameters.get(0);
        String sortedBy = parameters.get(1);
        String argumentNameToSort = parameters.get(2);

        if (!typeTask.equals("Bug") && !typeTask.equals("Feedback") && !typeTask.equals("Story")) {
            throw new IllegalArgumentException(String.format(NO_SUCH_TASK, typeTask));
        }
        List<Task> tasks;
        StringBuilder sb = new StringBuilder();
        if ("title".equals(sortedBy)) {
            tasks = taskManagementRepository.getTasks()
                    .stream()
                    .filter(t -> t.getClass().getInterfaces()[0].getSimpleName().equals(typeTask))
                    .filter(t -> t.getTitle().contains(argumentNameToSort))
                    .sorted(Comparator.comparing(Task::getTitle))
                    .collect(Collectors.toList());
            return printTasksByTitle(argumentNameToSort, tasks, sb);
        } else if ("priority".equals(sortedBy)) {
            if ("Bug".equals(typeTask)) {
                ParsingHelpers.tryParseEnum(argumentNameToSort, TaskPriority.class);
                List<Bug> bugTasks = taskManagementRepository.getBugs()
                        .stream()
                        .filter(f -> f.getPriority().equals(TaskPriority.valueOf(argumentNameToSort).toString()))
                        .sorted(Comparator.comparing(Bug::getPriority))
                        .collect(Collectors.toList());
                return printBugTasks(argumentNameToSort, sb, bugTasks, sortedBy);
            } else if ("Story".equals(typeTask)) {
                ParsingHelpers.tryParseEnum(argumentNameToSort, TaskPriority.class);
                List<Story> storiesTasks = taskManagementRepository.getStories()
                        .stream()
                        .filter(f -> f.getPriority().equals(TaskPriority.valueOf(argumentNameToSort).toString()))
                        .sorted(Comparator.comparing(Story::getPriority))
                        .collect(Collectors.toList());
                return printStoriesTasks(argumentNameToSort, sb, storiesTasks, sortedBy);}
            throw new IllegalArgumentException(String.format(NO_SORTING_OPTION, typeTask, sortedBy));
        } else if ("severity".equals(sortedBy)) {
            if (!typeTask.equals("Bug")) {
                throw new IllegalArgumentException(String.format(NO_SORTING_OPTION, typeTask, sortedBy));
            }
            ParsingHelpers.tryParseEnum(argumentNameToSort, Severity.class);
            List<Bug> bugTasks = taskManagementRepository.getBugs()
                    .stream()
                    .filter(f -> f.getSeverity().equals(Severity.valueOf(argumentNameToSort).toString()))
                    .sorted(Comparator.comparing(Bug::getSeverity))
                    .collect(Collectors.toList());
            return printBugTasks(argumentNameToSort, sb, bugTasks, sortedBy);
        } else if ("size".equals(sortedBy)) {
            if (!typeTask.equals("Story")) {
                throw new IllegalArgumentException(String.format(NO_SORTING_OPTION, typeTask, sortedBy));}
            ParsingHelpers.tryParseEnum(argumentNameToSort, Size.class);
            List<Story> storiesTasks = taskManagementRepository.getStories()
                    .stream()
                    .filter(f -> f.getSize().equals(Size.valueOf(argumentNameToSort).toString()))
                    .sorted(Comparator.comparing(Story::getSize))
                    .collect(Collectors.toList());
            return printStoriesTasks(argumentNameToSort, sb, storiesTasks, sortedBy);
        } else if ("rating".equals(sortedBy)) {
            if (!typeTask.equals("Feedback")) {
                throw new IllegalArgumentException(String.format(NO_SORTING_OPTION, typeTask, sortedBy));}
            List<Feedback> feedbackTasks = taskManagementRepository.getFeedbacks()
                    .stream()
                    .filter(t -> t.getRating() >= 0)
                    .sorted(Comparator.comparing(Feedback::getRating))
                    .collect(Collectors.toList());

            return printFeedbackTasks(argumentNameToSort, sb, feedbackTasks, sortedBy);
        }
        throw new IllegalArgumentException(String.format(NO_SUCH_SORTING_OPTION, sortedBy));
    }

    private String printFeedbackTasks(String argumentNameToSort, StringBuilder sb, List<Feedback> feedbackTasks,
                                      String sortedBy) {
        sb.append(BORDER_LINE).append(System.lineSeparator());
        if (feedbackTasks.isEmpty()) {
            throw new IllegalArgumentException(String.format(EMPTY_FEEDBACK_TASKS, argumentNameToSort));
        }
        sb.append(String.format("Feedback tasks with %s %s are", sortedBy, argumentNameToSort))
                .append(System.lineSeparator());
        for (Feedback feedback : feedbackTasks) {
            sb.append(" ---").append("Feedback with id ").append(feedback.getId()).append(" and title ")
                    .append(feedback.getTitle()).append(System.lineSeparator());
        }
        sb.append(BORDER_LINE);
        return sb.toString();
    }
    private String printStoriesTasks(String argumentNameToSort, StringBuilder sb, List<Story> storiesTasks, String sortedBy) {
        sb.append(BORDER_LINE).append(System.lineSeparator());
        if (storiesTasks.isEmpty()) {
            throw new IllegalArgumentException(String.format(EMPTY_STORY_TASKS, argumentNameToSort));
        }
        sb.append(String.format("Story tasks with %s %s are", sortedBy, argumentNameToSort))
                .append(System.lineSeparator());
        for (Story story : storiesTasks) {
            sb.append(" ---").append("Story with id ").append(story.getId()).append(" and title ")
                    .append(story.getTitle()).append(System.lineSeparator());
        }
        sb.append(BORDER_LINE);
        return sb.toString();}
    private String printTasksByTitle(String argumentNameToSort, List<Task> tasks, StringBuilder sb) {
        sb.append(BORDER_LINE).append(System.lineSeparator());
        if (tasks.isEmpty()) {
            throw new IllegalArgumentException(String.format(EMPTY_TASK, argumentNameToSort));}
        sb.append(String.format("Task with title containing %s are:", argumentNameToSort))
                .append(System.lineSeparator());
        for (Task task : tasks) {
            sb.append(" ---").append("Task with id ").append(task.getId()).append(" and title ")
                    .append(task.getTitle()).append(System.lineSeparator());
        }
        sb.append(BORDER_LINE);
        return sb.toString();
    }
    private String printBugTasks(String argumentNameToSort, StringBuilder sb, List<Bug> bugTasks, String sortedBy) {
        sb.append(BORDER_LINE).append(System.lineSeparator());
        if (bugTasks.isEmpty()) {
            throw new IllegalArgumentException(String.format(EMPTY_BUG_TASKS, argumentNameToSort));
        }
        sb.append(String.format("Bug tasks with %s %s are", sortedBy, argumentNameToSort))
                .append(System.lineSeparator());
        for (Bug bugTask : bugTasks) {
            sb.append(" ---").append("Bug with id ").append(bugTask.getId()).append(" and title ")
                    .append(bugTask.getTitle()).append(System.lineSeparator());
        }

        sb.append(BORDER_LINE);
        return sb.toString();}
}
