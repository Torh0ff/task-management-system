package com.telerikacademy.project.taskmanagementsystem.commands.listings;

import com.telerikacademy.project.taskmanagementsystem.commands.contracts.Command;
import com.telerikacademy.project.taskmanagementsystem.core.contract.TaskManagementRepository;
import com.telerikacademy.project.taskmanagementsystem.models.tasks.contracts.Bug;
import com.telerikacademy.project.taskmanagementsystem.models.tasks.contracts.enums.bug.BugStatus;
import com.telerikacademy.project.taskmanagementsystem.utils.ParsingHelpers;
import com.telerikacademy.project.taskmanagementsystem.utils.ValidationHelpers;

import java.util.List;
import java.util.stream.Collectors;

public class FilterBugsByStatusCommand implements Command {

    public static final int EXPECTED_NUMBER_OF_ARGUMENTS = 1;
    private final TaskManagementRepository taskManagementRepository;
    public FilterBugsByStatusCommand(TaskManagementRepository taskManagementRepository) {
        this.taskManagementRepository = taskManagementRepository;
    }


    @Override
    public String execute(List<String> parameters) {
        ValidationHelpers.validateArgumentsCount(parameters, EXPECTED_NUMBER_OF_ARGUMENTS);
        BugStatus bugStatus = ParsingHelpers.tryParseEnum(parameters.get(0), BugStatus.class);

        return filterBugsByStatus(bugStatus);
    }

    private String filterBugsByStatus(BugStatus bugStatus) {

        StringBuilder stringBuilder = new StringBuilder();
        List<Bug> output = taskManagementRepository.getBugs().stream()
                .filter(bug -> bug.getStatus().equals(bugStatus.toString()))
                .collect(Collectors.toList());

        if (output.size() == 0){
            throw new IllegalArgumentException(String.format("There are no bugs with status '%s'%n----------------------------------------------", bugStatus.toString()));
        }
        stringBuilder.append(String.format("Bugs filtered by '%s' status:%n", bugStatus));
        for (Bug b : output) {
            stringBuilder.append(b.print());
        }

        return stringBuilder.toString();
    }
}