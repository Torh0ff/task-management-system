package com.telerikacademy.project.taskmanagementsystem.commands.listings.listingAndSorting;

import com.telerikacademy.project.taskmanagementsystem.commands.contracts.Command;
import com.telerikacademy.project.taskmanagementsystem.core.contract.TaskManagementRepository;
import com.telerikacademy.project.taskmanagementsystem.models.tasks.contracts.Task;
import com.telerikacademy.project.taskmanagementsystem.models.tasks.contracts.enums.TaskStatus;
import com.telerikacademy.project.taskmanagementsystem.utils.ParsingHelpers;
import com.telerikacademy.project.taskmanagementsystem.utils.ValidationHelpers;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

//List tasks with assignee.
//Filter by status
public class ListAllTasksWithAssigneeFilteredByStatusCommand implements Command {
    private static final int ARGUMENTS_COUNT = 1;

    private final TaskManagementRepository taskManagementRepository;
    private static final String NO_TASKS_WITH_THIS_STATUS = "There are no tasks with status %s";
    private static final String BORDER_LINE =
            "==================================================================================================";


    public ListAllTasksWithAssigneeFilteredByStatusCommand(TaskManagementRepository taskManagementRepository) {
        this.taskManagementRepository = taskManagementRepository;
    }

    @Override
    public String execute(List<String> parameters) {
        ValidationHelpers.validateArgumentsCount(parameters, ARGUMENTS_COUNT);
        TaskStatus taskStatus = ParsingHelpers.tryParseEnum(parameters.get(0), TaskStatus.class);

        List<Task> result = taskManagementRepository.getTasks()
                .stream()
                .filter(t -> t.getAssignee() != null)
                .filter(t -> t.getStatus().equals(taskStatus.toString()))
                .collect(Collectors.toList())
                .stream()
                .sorted(Comparator.comparing(Task::getTitle))
                .collect(Collectors.toList());

        if (result.isEmpty()) {
            throw new IllegalArgumentException(String.format(NO_TASKS_WITH_THIS_STATUS, taskStatus));
        }

        StringBuilder sb = new StringBuilder();
        sb.append(BORDER_LINE).append(System.lineSeparator());
        sb.append(String.format("Tasks with status %s are:\n", taskStatus));
        for (Task task : result) {
            sb.append(" ---").append(task.getTitle()).append(System.lineSeparator());
        }
        sb.append(BORDER_LINE);
        return sb.toString();
    }
}
