package com.telerikacademy.project.taskmanagementsystem.commands.listings;

import com.telerikacademy.project.taskmanagementsystem.commands.contracts.Command;
import com.telerikacademy.project.taskmanagementsystem.core.contract.TaskManagementRepository;
import com.telerikacademy.project.taskmanagementsystem.models.tasks.contracts.Feedback;
import com.telerikacademy.project.taskmanagementsystem.models.tasks.contracts.enums.feedback.FeedbackStatus;
import com.telerikacademy.project.taskmanagementsystem.utils.ParsingHelpers;
import com.telerikacademy.project.taskmanagementsystem.utils.ValidationHelpers;

import java.util.List;
import java.util.stream.Collectors;

public class FilterFeedbacksByStatusCommand implements Command {

    public static final int EXPECTED_NUMBER_OF_ARGUMENTS = 1;
    private final TaskManagementRepository taskManagementRepository;
    public FilterFeedbacksByStatusCommand(TaskManagementRepository taskManagementRepository) {
        this.taskManagementRepository = taskManagementRepository;
    }


    @Override
    public String execute(List<String> parameters) {
        ValidationHelpers.validateArgumentsCount(parameters, EXPECTED_NUMBER_OF_ARGUMENTS);
        FeedbackStatus status = ParsingHelpers.tryParseEnum(parameters.get(0), FeedbackStatus.class);

        return filterFeedbackByStatus(status);
    }

    private String filterFeedbackByStatus(FeedbackStatus status) {

        StringBuilder stringBuilder = new StringBuilder();
        List<Feedback> output = taskManagementRepository.getFeedbacks().stream()
                .filter(feedback -> feedback.getStatus().equals(status.toString()))
                .collect(Collectors.toList());

        if (output.size() == 0) {
            throw new IllegalArgumentException(String.format("There are no feedbacks with status '%s'%n----------------------------------------------", status.toString()));
        }
        stringBuilder.append(String.format("Feedbacks filtered by '%s' status:%n", status));
        for (Feedback b : output) {
            stringBuilder.append(b.print());
        }
        return stringBuilder.toString();
    }
}