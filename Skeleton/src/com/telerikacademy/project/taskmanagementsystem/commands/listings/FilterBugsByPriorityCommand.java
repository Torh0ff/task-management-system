package com.telerikacademy.project.taskmanagementsystem.commands.listings;

import com.telerikacademy.project.taskmanagementsystem.commands.contracts.Command;
import com.telerikacademy.project.taskmanagementsystem.core.TaskManagementRepositoryImpl;
import com.telerikacademy.project.taskmanagementsystem.core.contract.TaskManagementRepository;
import com.telerikacademy.project.taskmanagementsystem.models.tasks.contracts.Bug;
import com.telerikacademy.project.taskmanagementsystem.models.tasks.contracts.enums.TaskPriority;
import com.telerikacademy.project.taskmanagementsystem.utils.ParsingHelpers;
import com.telerikacademy.project.taskmanagementsystem.utils.ValidationHelpers;

import java.util.List;
import java.util.stream.Collectors;

public class FilterBugsByPriorityCommand implements Command {
    public static final int EXPECTED_NUMBER_OF_ARGUMENTS = 1;
    private final TaskManagementRepository taskManagementRepository;

    public FilterBugsByPriorityCommand(TaskManagementRepository taskManagementRepository) {
        this.taskManagementRepository = taskManagementRepository;
    }


    @Override
    public String execute(List<String> parameters) {
        ValidationHelpers.validateArgumentsCount(parameters, EXPECTED_NUMBER_OF_ARGUMENTS);
        TaskPriority priority = ParsingHelpers.tryParseEnum(parameters.get(0), TaskPriority.class);

        return filterBugsByPriority(priority);
    }

    private String filterBugsByPriority(TaskPriority priority) {
        StringBuilder stringBuilder = new StringBuilder();
        List<Bug> output = taskManagementRepository.getBugs().stream()
                .filter(bug -> bug.getPriority().equals(priority.toString()))
                .collect(Collectors.toList());
        if (output.size() == 0){
            throw new IllegalArgumentException(String.format("There are no bugs with priority '%s'%n----------------------------------------------", priority.toString()));}
        stringBuilder.append(String.format("Bugs filtered by '%s' priority:%n", priority));
        for (Bug b : output) {
            stringBuilder.append(b.print());
        }
        return stringBuilder.toString();
    }
}