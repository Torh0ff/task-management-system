package com.telerikacademy.project.taskmanagementsystem.commands.listings.listingAndSorting;


import com.telerikacademy.project.taskmanagementsystem.commands.contracts.Command;
import com.telerikacademy.project.taskmanagementsystem.core.contract.TaskManagementRepository;
import com.telerikacademy.project.taskmanagementsystem.models.tasks.contracts.Task;
import com.telerikacademy.project.taskmanagementsystem.models.tasks.contracts.enums.TaskStatus;
import com.telerikacademy.project.taskmanagementsystem.utils.ParsingHelpers;
import com.telerikacademy.project.taskmanagementsystem.utils.ValidationHelpers;

import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

//List bugs/stories/feedback only.
//· Filter by status
public class ListTasksFilteredByStatusCommand implements Command {

    private static final int ARGUMENTS_COUNT = 2;
    private static final String NO_SUCH_TASK = "Task with name %s does not exist.";
    private static final String EMPTY_STATUS_TASKS = "There are no %s tasks with status %s.";
    private static final String BORDER_LINE =
            "==================================================================================================";
    private final TaskManagementRepository taskManagementRepository;

    public ListTasksFilteredByStatusCommand(TaskManagementRepository taskManagementRepository) {
        this.taskManagementRepository = taskManagementRepository;
    }


    @Override
    public String execute(List<String> parameters) {
        ValidationHelpers.validateArgumentsCount(parameters, ARGUMENTS_COUNT);
        String taskType = parameters.get(0);
        TaskStatus status = ParsingHelpers.tryParseEnum(parameters.get(1), TaskStatus.class);

        if (!taskType.equals("Bug") && !taskType.equals("Feedback") && !taskType.equals("Story")){
            throw new IllegalArgumentException(String.format(NO_SUCH_TASK, taskType));
        }

        List<Task> tasks = taskManagementRepository.getTasks()
                .stream()
                .filter(t -> t.getClass().getInterfaces()[0].getSimpleName().equals(taskType))
                .filter(t -> t.getStatus().equalsIgnoreCase(status.toString()))
                .collect(Collectors.toList());

        if (tasks.isEmpty()){
            throw new IllegalArgumentException(String.format(EMPTY_STATUS_TASKS, taskType, status));
        }
        StringBuilder sb = new StringBuilder();
        sb.append(BORDER_LINE).append(System.lineSeparator());
        sb.append(String.format("%s task with %s status are: \n", taskType, status));
        for (Task task : tasks) {
            sb.append(" ---").append("ID: ").append(task.getId()).append(" and title ")
                    .append(task.getTitle()).append(System.lineSeparator());
        }
        sb.append(BORDER_LINE);


        return sb.toString();
    }
}
