package com.telerikacademy.project.taskmanagementsystem.commands.listings;

import com.telerikacademy.project.taskmanagementsystem.commands.contracts.Command;
import com.telerikacademy.project.taskmanagementsystem.core.contract.TaskManagementRepository;
import com.telerikacademy.project.taskmanagementsystem.models.tasks.contracts.Story;
import com.telerikacademy.project.taskmanagementsystem.models.tasks.contracts.enums.story.StoryStatus;
import com.telerikacademy.project.taskmanagementsystem.utils.ParsingHelpers;
import com.telerikacademy.project.taskmanagementsystem.utils.ValidationHelpers;

import java.util.List;
import java.util.stream.Collectors;

public class FilterStoriesByStatusCommand implements Command {
    public static final int EXPECTED_NUMBER_OF_ARGUMENTS = 1;
    private final TaskManagementRepository taskManagementRepository;

    public FilterStoriesByStatusCommand(TaskManagementRepository taskManagementRepository) {
        this.taskManagementRepository = taskManagementRepository;
    }


    @Override
    public String execute(List<String> parameters) {
        ValidationHelpers.validateArgumentsCount(parameters, EXPECTED_NUMBER_OF_ARGUMENTS);
       StoryStatus status = ParsingHelpers.tryParseEnum(parameters.get(0), StoryStatus.class);

        return filterStoriesByStatus(status);
    }

    private String filterStoriesByStatus(StoryStatus status) {

        StringBuilder stringBuilder = new StringBuilder();
        List<Story> output = taskManagementRepository.getStories().stream()
                .filter(story -> story.getStatus().equals(status.toString()))
                .collect(Collectors.toList());

        if (output.size() == 0) {
            throw new IllegalArgumentException(String.format("There are no stories with status '%s'%n----------------------------------------------", status.toString()));
        }
        stringBuilder.append(String.format("Stories filtered by '%s' status:%n", status));
        for (Story b : output) {
            stringBuilder.append(b.print());
        }

        return stringBuilder.toString();
    }
}