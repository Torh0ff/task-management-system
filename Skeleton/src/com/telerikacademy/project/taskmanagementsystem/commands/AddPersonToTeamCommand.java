package com.telerikacademy.project.taskmanagementsystem.commands;

import com.telerikacademy.project.taskmanagementsystem.commands.contracts.Command;
import com.telerikacademy.project.taskmanagementsystem.core.contract.TaskManagementRepository;
import com.telerikacademy.project.taskmanagementsystem.models.team.EventLogImpl;
import com.telerikacademy.project.taskmanagementsystem.models.team.contacts.Eventlog;
import com.telerikacademy.project.taskmanagementsystem.models.team.contacts.Person;
import com.telerikacademy.project.taskmanagementsystem.models.team.contacts.Team;
import com.telerikacademy.project.taskmanagementsystem.utils.ValidationHelpers;

import java.util.List;

public class AddPersonToTeamCommand implements Command {

    private static final int EXPECTED_ARGUMENTS_COUNT = 2;
    private static final String PERSON_ADDED_TO_TEAM = "User with name '%s' was added to team '%s'.";
    private final TaskManagementRepository taskManagementRepository;

    public AddPersonToTeamCommand(TaskManagementRepository taskManagementRepository) {
        this.taskManagementRepository = taskManagementRepository;
    }

    @Override
    public String execute(List<String> parameters) {
        ValidationHelpers.validateArgumentsCount(parameters, EXPECTED_ARGUMENTS_COUNT);

        String personName = parameters.get(0);
        String teamName = parameters.get(1);

        return addPersonToTeam(personName, teamName);
    }

    private String addPersonToTeam(String personName, String teamName) {
        Person person = taskManagementRepository.findPersonByName(personName);
        Team team = taskManagementRepository.findTeamByName(teamName);
        team.addMember(person);
        Eventlog eventlog = new EventLogImpl(String.format(PERSON_ADDED_TO_TEAM, personName, teamName));
        person.addEventLog(eventlog);
        team.addEventLog(eventlog);
        return String.format(PERSON_ADDED_TO_TEAM, personName, teamName);
    }
}
