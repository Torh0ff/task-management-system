package com.telerikacademy.project.taskmanagementsystem.commands;

import com.telerikacademy.project.taskmanagementsystem.commands.contracts.Command;
import com.telerikacademy.project.taskmanagementsystem.core.contract.TaskManagementRepository;
import com.telerikacademy.project.taskmanagementsystem.models.tasks.contracts.Task;
import com.telerikacademy.project.taskmanagementsystem.models.team.EventLogImpl;
import com.telerikacademy.project.taskmanagementsystem.models.team.contacts.Board;
import com.telerikacademy.project.taskmanagementsystem.models.team.contacts.Eventlog;
import com.telerikacademy.project.taskmanagementsystem.models.team.contacts.Person;
import com.telerikacademy.project.taskmanagementsystem.models.team.contacts.Team;
import com.telerikacademy.project.taskmanagementsystem.utils.ParsingHelpers;
import com.telerikacademy.project.taskmanagementsystem.utils.ValidationHelpers;

import java.util.List;

public class UnAssignTaskCommand implements Command {
    private final TaskManagementRepository taskManagementRepository;

    public UnAssignTaskCommand(TaskManagementRepository taskManagementRepository) {
        this.taskManagementRepository = taskManagementRepository;
    }

    public static final int EXPECTED_NUMBER_OF_ARGUMENTS = 2;

    public static final String USER_UNASSIGNED_MESSAGE = "User '%s' has been unassigned from the task '%s' ID:%d.";

    public static final String TASK_ASSIGNED_MESSAGE = "Task '%s' ID:%d hasn't been assigned to the user '%s'";

    public static final String USER_HAS_NO_ASSIGNED_TASK_MESSAGE = "User '%s' hasn't been assigned to any task, therefore cannot be unassigned";

    @Override
    public String execute(List<String> parameters) {
        ValidationHelpers.validateArgumentsCount(parameters, EXPECTED_NUMBER_OF_ARGUMENTS);
        int id = ParsingHelpers.tryParseInteger(parameters.get(0), "id");
        String user = parameters.get(1);
        return unAssignTask(id, user);

    }

    private String unAssignTask(int taskId, String user) {
        Person assignee = taskManagementRepository.findPersonByName(user);
        Task task = taskManagementRepository.findTaskById(taskManagementRepository.getTasks(), taskId);
        if (!task.getIsAssigned()) {
            throw new IllegalArgumentException(String.format(TASK_ASSIGNED_MESSAGE, task.getTitle(), task.getId(), user));
        }
        if (!assignee.isAssigned()) {
            throw new IllegalArgumentException(String.format(USER_HAS_NO_ASSIGNED_TASK_MESSAGE, user));
        }
        if (task.getAssignee().equals(assignee.getName()) && task.getId() == taskId){
            task.unAssignTask(task);
            Eventlog eventlog = new EventLogImpl(String.format(USER_UNASSIGNED_MESSAGE, user, task.getTitle(), task.getId()));
            assignee.addEventLog(eventlog);
            task.addEventLog(eventlog);
        }
        return String.format(USER_UNASSIGNED_MESSAGE, user, task.getTitle(), task.getId());
    }
}
