package com.telerikacademy.project.taskmanagementsystem.commands;

import com.telerikacademy.project.taskmanagementsystem.commands.contracts.Command;
import com.telerikacademy.project.taskmanagementsystem.core.contract.TaskManagementRepository;
import com.telerikacademy.project.taskmanagementsystem.models.tasks.contracts.Story;
import com.telerikacademy.project.taskmanagementsystem.models.tasks.contracts.enums.story.StoryStatus;
import com.telerikacademy.project.taskmanagementsystem.models.team.EventLogImpl;
import com.telerikacademy.project.taskmanagementsystem.models.team.contacts.Eventlog;
import com.telerikacademy.project.taskmanagementsystem.utils.ParsingHelpers;
import com.telerikacademy.project.taskmanagementsystem.utils.ValidationHelpers;

import java.util.List;

public class ChangeStoryStatusCommand implements Command {

    public static final int EXPECTED_NUMBER_OF_ARGUMENTS_CHANGE = 2;
    public static final String INVALID_ID_PARAMETER = "Invalid input, ID number has to be an integer";
    public static final String STORY_STATUS_CHANGED = "Status of story '%s' with ID %d has been changed to '%s'.";
    private final TaskManagementRepository taskManagementRepository;

    public ChangeStoryStatusCommand(TaskManagementRepository taskManagementRepository) {
        this.taskManagementRepository = taskManagementRepository;
    }


    @Override
    public String execute(List<String> parameters) {
        ValidationHelpers.validateArgumentsCount(parameters, EXPECTED_NUMBER_OF_ARGUMENTS_CHANGE);
        int storyId = ParsingHelpers.tryParseInteger(parameters.get(0), INVALID_ID_PARAMETER);
        StoryStatus status = ParsingHelpers.tryParseEnum(parameters.get(1), StoryStatus.class);

        return changedFeedback(storyId, status);
    }

    private String changedFeedback(int storyId, StoryStatus status) {
        Story story = (Story) taskManagementRepository.findTaskById(taskManagementRepository.getTasks(), storyId);
        story.setStatus(status);
        Eventlog eventlog = new EventLogImpl(String.format(STORY_STATUS_CHANGED, story.getTitle(), storyId, status));
        story.addEventLog(eventlog);
        return String.format(STORY_STATUS_CHANGED, story.getTitle(), storyId, status);

    }
}
