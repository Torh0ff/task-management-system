package com.telerikacademy.project.taskmanagementsystem.commands.contracts;

import java.util.List;

public interface Command {

    String execute(List<String> parameters);

}
