package com.telerikacademy.project.taskmanagementsystem.commands;

import com.telerikacademy.project.taskmanagementsystem.commands.contracts.Command;
import com.telerikacademy.project.taskmanagementsystem.core.contract.TaskManagementRepository;
import com.telerikacademy.project.taskmanagementsystem.models.team.contacts.Board;
import com.telerikacademy.project.taskmanagementsystem.models.team.contacts.Team;
import com.telerikacademy.project.taskmanagementsystem.utils.ValidationHelpers;

import java.util.List;

public class ShowAllTeamBoardsCommand implements Command {

    private static final int EXPECTED_ARGUMENTS_COUNT = 1;
    private static final String BORDER_LINE = "==================================================================================================";
    private final TaskManagementRepository taskManagementRepository;

    public ShowAllTeamBoardsCommand(TaskManagementRepository taskManagementRepository) {
        this.taskManagementRepository = taskManagementRepository;
    }

    @Override
    public String execute(List<String> parameters) {
        ValidationHelpers.validateArgumentsCount(parameters, EXPECTED_ARGUMENTS_COUNT);

        String teamName = parameters.get(0);

        return showTeamBoards(teamName);
    }

    private String showTeamBoards(String teamName) {
        Team team = taskManagementRepository.findTeamByName(teamName);

        StringBuilder sb = new StringBuilder();
        sb.append(BORDER_LINE).append(System.lineSeparator());
        sb.append(teamName).append(" team has boards: ").append(System.lineSeparator());
        int count = 1;
        for (Board board : team.getBoards()) {
            sb.append("  --").append(count).append(". ").append(board.getName()).append(System.lineSeparator());
            count++;
        }

        sb.append(BORDER_LINE);
        return sb.toString();
    }
}
