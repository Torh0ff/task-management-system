package com.telerikacademy.project.taskmanagementsystem.commands;


import com.telerikacademy.project.taskmanagementsystem.commands.contracts.Command;
import com.telerikacademy.project.taskmanagementsystem.core.contract.TaskManagementRepository;
import com.telerikacademy.project.taskmanagementsystem.models.team.contacts.Eventlog;
import com.telerikacademy.project.taskmanagementsystem.models.team.contacts.Person;
import com.telerikacademy.project.taskmanagementsystem.utils.ValidationHelpers;

import java.util.List;

public class ShowPersonActivityCommand implements Command {

    public static final int EXPECTED_PARAMETERS_COUNT = 1;
    private static final String EMPTY_LOGS_FOR_THIS_PERSON = "%s has no Activities";

    private static final String BORDER_LINE = "==================================================================================================";

    private final TaskManagementRepository taskManagementRepository;

    public ShowPersonActivityCommand(TaskManagementRepository taskManagementRepository) {
        this.taskManagementRepository = taskManagementRepository;
    }

    @Override
    public String execute(List<String> parameters) {
        ValidationHelpers.validateArgumentsCount(parameters, EXPECTED_PARAMETERS_COUNT);

        String name = parameters.get(0);

        return showActivity(name);
    }

    private String showActivity(String name) {
        Person person = taskManagementRepository.findPersonByName(name);


        StringBuilder sb = new StringBuilder();
        sb.append(BORDER_LINE).append(System.lineSeparator());
        String start = "User " + name + " has activity: \n";
        sb.append(start);
        int count = 1;
        for (Eventlog log : person.getLogs()) {
            sb.append("--").append(count).append(" ").append(log.viewInfo()).append(System.lineSeparator());
            count++;
        }
        sb.append(BORDER_LINE);
        return  sb.toString();
    }
}
