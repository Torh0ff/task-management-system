package com.telerikacademy.project.taskmanagementsystem.models.tasks.contracts;

import com.telerikacademy.project.taskmanagementsystem.models.tasks.contracts.enums.bug.BugStatus;
import com.telerikacademy.project.taskmanagementsystem.models.tasks.contracts.enums.feedback.FeedbackStatus;

public interface Feedback extends Task, Assignable{

    void setStatus(FeedbackStatus status);

    int getRating();

}
