package com.telerikacademy.project.taskmanagementsystem.models.tasks.contracts;

public interface Identifiable {

    int getId();

}
