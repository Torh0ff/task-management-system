package com.telerikacademy.project.taskmanagementsystem.models.tasks;

import com.telerikacademy.project.taskmanagementsystem.models.tasks.contracts.Feedback;
import com.telerikacademy.project.taskmanagementsystem.models.tasks.contracts.enums.TaskType;
import com.telerikacademy.project.taskmanagementsystem.models.tasks.contracts.enums.feedback.FeedbackStatus;

public class FeedbackImpl extends TaskBase implements Feedback {
    private FeedbackStatus status;

    private static final String RATING_ERROR_MESSAGE = "Rating cannot be less than %d or more than %d";

    public static final int MIN_RATE_VALUE = 1;
    public static final int MAX_RATE_VALUE = 10;

    private int rating;

    public FeedbackImpl(int id, String title, String description, int rating) {
        super(id, title, description, TaskType.FEEDBACK);
        setRating(rating);
        this.status = FeedbackStatus.NEW;
    }


    @Override
    public void setStatus(FeedbackStatus status) {
        if (this.status.equals(status)){
            throw new IllegalArgumentException(String.format("Status already on '%s' status", status));
        }
        this.status = status;
    }

    @Override
    public int getRating() {
        return rating;
    }

    protected void validateRating(int rating){
        if (rating < 1 || rating > 10){
            throw new IllegalArgumentException(String.format(RATING_ERROR_MESSAGE, MIN_RATE_VALUE, MAX_RATE_VALUE));
        }
    }
    private void setRating(int rating) {
        validateRating(rating);
        this.rating = rating;
    }

    @Override
    public String getStatus() {
        return status.toString();
    }


    @Override
    public String print() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(String.format("%sRating: %d%nStatus: %s%nAssignee: %s%n----------------------------------------------%n".trim(),
                super.print(), getRating(), getStatus(), getAssignee()));
        return stringBuilder.toString();
    }
}
