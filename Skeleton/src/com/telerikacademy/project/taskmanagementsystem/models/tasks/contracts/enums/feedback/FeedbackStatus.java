package com.telerikacademy.project.taskmanagementsystem.models.tasks.contracts.enums.feedback;

public enum FeedbackStatus {

    NEW,
    UNSCHEDULED,
    SCHEDULED,
    DONE;


    @Override
    public String toString() {
        switch (this) {
            case NEW:
                return "New";
            case UNSCHEDULED:
                return "Uncheduled";
            case SCHEDULED:
                return "Scheduled";
            case DONE:
                return "Done";
            default:
                throw new IllegalArgumentException("There is no such a feedback status");
        }
    }

}
