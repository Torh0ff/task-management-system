package com.telerikacademy.project.taskmanagementsystem.models.tasks.contracts.enums.bug;

public enum BugStatus {

    ACTIVE,
    FIXED;

    @Override
    public String toString() {
        switch (this) {
            case ACTIVE:
                return "Active";
            case FIXED:
                return "Fixed";
            default:
                throw new IllegalArgumentException("There is no such a bug status");
        }
    }

}
