package com.telerikacademy.project.taskmanagementsystem.models.tasks;

import com.telerikacademy.project.taskmanagementsystem.models.tasks.contracts.Bug;
import com.telerikacademy.project.taskmanagementsystem.models.tasks.contracts.enums.TaskPriority;
import com.telerikacademy.project.taskmanagementsystem.models.tasks.contracts.enums.TaskType;
import com.telerikacademy.project.taskmanagementsystem.models.tasks.contracts.enums.bug.BugStatus;
import com.telerikacademy.project.taskmanagementsystem.models.tasks.contracts.enums.bug.Severity;

import java.util.ArrayList;
import java.util.List;


public class BugImpl extends TaskBase implements Bug {

    private BugStatus status;
    private Severity severity;
    private TaskPriority priority;

    private final List<String> steps;


    public BugImpl(int id, String title, String description, Severity severity, TaskPriority priority, List<String> steps) {
        super(id, title, description, TaskType.BUG);
        this.priority = priority;
        this.severity = severity;
        this.steps = steps;
        this.status = BugStatus.ACTIVE;
    }

    @Override
    public void setPriority(TaskPriority priority) {
        if (this.priority.equals(priority)){
            throw new IllegalArgumentException(String.format("Priority is already set as %s ", priority));
        }
        this.priority = priority;
    }

    public List<String> getSteps() {
        return new ArrayList<>(steps);
    }


    @Override
    public void setSeverity(Severity severity) {
        if (this.severity.equals(severity)){
            throw new IllegalArgumentException(String.format("Severity of the bug %d is already set as %s ", getId(), severity));
        }
        this.severity = severity;
    }

    @Override
    public void setStatus(BugStatus status) {
        if (this.status.equals(status)){
            throw new IllegalArgumentException(String.format("Status already on %s status", status));
        }
        this.status = status;
    }

    @Override
    public String getStatus() {
        return this.status.toString();
    }


    public String getSeverity() {
        return severity.toString();
    }

    @Override
    public String getPriority() {
        return priority.toString();
    }

    public String print() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(String.format("%sSeverity: %s%nPriority: %s%nSteps: %s%nStatus: %s%nAssignee: %s%n----------------------------------------------%n".trim(),
                super.print(), getSeverity(), getPriority(), getSteps().toString(), getStatus(), getAssignee()));
        return stringBuilder.toString();
    }
}