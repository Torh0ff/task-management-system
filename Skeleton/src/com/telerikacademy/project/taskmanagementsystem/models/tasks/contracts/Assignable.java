package com.telerikacademy.project.taskmanagementsystem.models.tasks.contracts;

import com.telerikacademy.project.taskmanagementsystem.models.team.contacts.Person;

public interface Assignable {

    String getAssignee();

    void addAssignee(Person assignee);

    Boolean getIsAssigned();

    void unAssignTask(Task task);

    public void setAssigned(boolean assigned);

}
