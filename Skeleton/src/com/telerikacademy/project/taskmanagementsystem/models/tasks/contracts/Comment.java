package com.telerikacademy.project.taskmanagementsystem.models.tasks.contracts;

public interface Comment {

    String getContent();

    String getAuthor();

}
