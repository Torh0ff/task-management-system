package com.telerikacademy.project.taskmanagementsystem.models.tasks;

import com.telerikacademy.project.taskmanagementsystem.models.tasks.contracts.Comment;

public class CommentImpl implements Comment {

    private String author;
    private String content;

    public CommentImpl(String content, String author) {
        this.author = author;
        this.content = content;
    }

    @Override
    public String getContent() {
        return content;
    }

    @Override
    public String getAuthor() {
        return author;
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(String.format(" %s%n User: %s%n -----------------------------"
                ,getContent()
                ,getAuthor()));
        return stringBuilder.toString();
    }
}
