package com.telerikacademy.project.taskmanagementsystem.models.tasks.contracts;

import com.telerikacademy.project.taskmanagementsystem.models.tasks.contracts.enums.TaskPriority;
import com.telerikacademy.project.taskmanagementsystem.models.tasks.contracts.enums.bug.BugStatus;
import com.telerikacademy.project.taskmanagementsystem.models.tasks.contracts.enums.bug.Severity;
import com.telerikacademy.project.taskmanagementsystem.models.team.contacts.Namable;


import java.util.List;

public interface Bug extends Task, Assignable {

    String getSeverity();

    List<String> getSteps();
    void setSeverity(Severity severity);

    void setStatus(BugStatus status);


}
