package com.telerikacademy.project.taskmanagementsystem.models.tasks;

import com.telerikacademy.project.taskmanagementsystem.models.tasks.contracts.Story;
import com.telerikacademy.project.taskmanagementsystem.models.tasks.contracts.enums.TaskPriority;
import com.telerikacademy.project.taskmanagementsystem.models.tasks.contracts.enums.TaskType;
import com.telerikacademy.project.taskmanagementsystem.models.tasks.contracts.enums.story.Size;
import com.telerikacademy.project.taskmanagementsystem.models.tasks.contracts.enums.story.StoryStatus;

public class StoryImpl extends TaskBase implements Story {

    private StoryStatus status;
    private Size size;
    private TaskPriority priority;

    public StoryImpl(int id, String title, String description, Size size, TaskPriority priority) {
        super(id, title, description, TaskType.STORY);
        this.status = StoryStatus.NOT_DONE;
        this.size = size;
        this.priority = priority;
    }

    @Override
    public String getSize() {
        return size.toString();
    }

    @Override
    public String getPriority() {
        return priority.toString();
    }

    @Override
    public void setSize(Size size) {
        if (this.size.equals(size)){
            throw new IllegalArgumentException(String.format("Size is already on %s status", size));
        }
        this.size = size;
    }

    @Override
    public void setStatus(StoryStatus status) {
        if (this.status.equals(status)){
            throw new IllegalArgumentException(String.format("Status already on %s status", status));
        }
        this.status = status;
    }

    @Override
    public void setPriority(TaskPriority priority) {
        if (this.priority.equals(priority)){
            throw new IllegalArgumentException(String.format("Priority is already set as %s ", priority));
        }
        this.priority = priority;
    }

    @Override
    public String getStatus() {
        return status.toString();
    }

    @Override
    public String print() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(String.format("%sSize: %s%nPriority: %s%nAssignee: %s%n----------------------------------------------%n".trim(),
                super.print(), getSize(), getPriority(), getAssignee()));
        return stringBuilder.toString();
    }

}
