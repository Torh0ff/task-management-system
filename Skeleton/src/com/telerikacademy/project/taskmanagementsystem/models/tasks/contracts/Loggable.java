package com.telerikacademy.project.taskmanagementsystem.models.tasks.contracts;

import com.telerikacademy.project.taskmanagementsystem.models.team.contacts.Eventlog;

import java.util.List;

public interface Loggable {

    List<Eventlog> getLogs();

    void addEventLog(Eventlog eventlog);

}
