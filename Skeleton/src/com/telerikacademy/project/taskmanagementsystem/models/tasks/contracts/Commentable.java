package com.telerikacademy.project.taskmanagementsystem.models.tasks.contracts;

import java.util.List;

public interface Commentable {

    List<Comment> getComments();

    void addComment(Comment commentToAdd);

    String printComments();

}
