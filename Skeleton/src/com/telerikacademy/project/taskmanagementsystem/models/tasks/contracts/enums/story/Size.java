package com.telerikacademy.project.taskmanagementsystem.models.tasks.contracts.enums.story;

public enum Size {

    LARGE,
    MEDIUM,
    SMALL;

    @Override
    public String toString() {
        switch (this) {
            case LARGE:
                return "Large";
            case MEDIUM:
                return "Medium";
            case SMALL:
                return "Small";
            default:
                throw new IllegalArgumentException("There is no such a story size");
        }
    }

}
