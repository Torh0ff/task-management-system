package com.telerikacademy.project.taskmanagementsystem.models.tasks.contracts.enums;

public enum TaskStatus {

    NOTDONE,
    INPROGRESS,
    DONE,
    ACTIVE,
    FIXED,
    NEW,
    UNSCHEDULED,
    SCHEDULED;

    @Override
    public String toString() {
        switch (this) {
            case ACTIVE:
                return "Active";
            case FIXED:
                return "Fixed";
            case NEW:
                return "New";
            case DONE:
                return "Done";
            case NOTDONE:
                return "Not Done";
            case SCHEDULED:
                return "Scheduled";
            case INPROGRESS:
                return "In Progress";
            case UNSCHEDULED:
                return "Unscheduled";
            default:
                throw new IllegalArgumentException("There is no such a bug status");
        }
    }

}
