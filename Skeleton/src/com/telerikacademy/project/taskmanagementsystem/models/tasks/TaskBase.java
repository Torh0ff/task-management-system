package com.telerikacademy.project.taskmanagementsystem.models.tasks;

import com.telerikacademy.project.taskmanagementsystem.models.tasks.contracts.Assignable;
import com.telerikacademy.project.taskmanagementsystem.models.tasks.contracts.Comment;
import com.telerikacademy.project.taskmanagementsystem.models.tasks.contracts.Task;
import com.telerikacademy.project.taskmanagementsystem.models.tasks.contracts.enums.TaskPriority;
import com.telerikacademy.project.taskmanagementsystem.models.tasks.contracts.enums.TaskType;
import com.telerikacademy.project.taskmanagementsystem.models.team.contacts.Eventlog;
import com.telerikacademy.project.taskmanagementsystem.models.team.contacts.Person;
import com.telerikacademy.project.taskmanagementsystem.utils.ValidationHelpers;

import java.util.ArrayList;
import java.util.List;

public abstract class TaskBase implements Task, Assignable {

    public static final int TASK_TITLE_MIN_LENGTH = 10;
    public static final int TASK_TITLE_MAX_LENGTH = 50;

    private static final String TITLE_ERROR_MESSAGE = "Title cannot be less than %d or more than %d symbols.";

    public static final int TASK_DESCRIPTION_MIN_LENGTH = 10;
    public static final int TASK_DESCRIPTION_MAX_LENGTH = 500;

    private static final String DESCRIPTION_ERROR_MESSAGE = "Description cannot be less than %d or more than %d symbols.";

    private static final String USER_ASSIGNED_MESSAGE = "User '%s' has already been assigned to other task, and cannot be re-assigned at the moment.";
    private static final String TASK_ASSIGNED_MESSAGE = "This task has already been assigned to other user, therefore cannot be assign to '%s'";

    private static final String TASK_UNASSIGNED_MESSAGE = "This task ID:%d hasn't been assigned to the user.";
    private static final String USER_UNASSIGNED_MESSAGE = "This user hasn't been assigned to the task ID:%d, therefore cannot be unassigned.";
    private final int id;
    private String title;
    private String description;

    private final TaskType taskType;

    private Person assignee;

    private TaskPriority priority;

    private boolean taskIsAssigned = false;
    private final List<Comment> comments = new ArrayList<>();
    private List<Eventlog> eventLogs = new ArrayList<>();

    public TaskBase(int id, String title, String description, TaskType taskType) {
        this.id = id;
        setTitle(title);
        setDescription(description);
        this.taskType = taskType;
        this.eventLogs = new ArrayList<>();
        taskIsAssigned = false;
    }


    @Override
    public int getId() {
        return id;
    }

    protected void validateTitle(String title) {
        ValidationHelpers.validateStringLength(title,
                TASK_TITLE_MIN_LENGTH,
                TASK_TITLE_MAX_LENGTH,
                String.format(TITLE_ERROR_MESSAGE,
                        TASK_TITLE_MIN_LENGTH,
                        TASK_TITLE_MAX_LENGTH));
    }

    private void setTitle(String title) {
        validateTitle(title);
        this.title = title;
    }

    @Override
    public String getTitle() {
        return title;
    }

    protected void validateDescription(String description){
        ValidationHelpers.validateStringLength(description,
                TASK_DESCRIPTION_MIN_LENGTH,
                TASK_DESCRIPTION_MAX_LENGTH,
                String.format(DESCRIPTION_ERROR_MESSAGE,
                        TASK_DESCRIPTION_MIN_LENGTH,
                        TASK_DESCRIPTION_MAX_LENGTH));
    }

    private void setDescription(String description) {
        validateDescription(description);
        this.description = description;
    }
    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public List<Comment> getComments() {
        return new ArrayList<>(comments);
    }

    @Override
    public void addComment(Comment commentToAdd) {
        this.comments.add(commentToAdd);
    }

    public List<Eventlog> getLogs() {
        return new ArrayList<>(eventLogs);
    }

    public TaskType getType() {
        return taskType;
    }

    @Override
    public void addEventLog(Eventlog eventlog) {
            this.eventLogs.add(eventlog);
        }

    @Override
    public String getName() {
        return this.title;
    }

    @Override
    public void addAssignee(Person assignee) {
        if (assignee.isAssigned()) {
            throw new IllegalArgumentException(String.format(USER_ASSIGNED_MESSAGE, assignee.getName()));
        }
       if (getIsAssigned()){
           throw new IllegalArgumentException(String.format(TASK_ASSIGNED_MESSAGE,assignee.getName()));
       }
        else {
            taskIsAssigned = true;
            this.assignee = assignee;
            this.assignee.setAssigned(true);
            setAssigned(true);
        }
    }

    public void unAssignTask(Task task) {
        if (!getIsAssigned()) {
            throw new IllegalArgumentException(String.format(TASK_UNASSIGNED_MESSAGE, task.getId()));
        }
        if (!assignee.isAssigned()){
            throw new IllegalArgumentException(String.format(USER_UNASSIGNED_MESSAGE, task.getId()));
        }
        taskIsAssigned = false;
        assignee.deleteTask(task);
        this.assignee.setAssigned(false);
        setAssigned(false);
        this.assignee = null;
    }

    @Override
    public String getAssignee() {
        if (this.assignee == null){
            return "no assignee";
        }
        return assignee.getName();
    }
    @Override
    public Boolean getIsAssigned() {
        return taskIsAssigned;
    }

    public void setAssigned(boolean assigned) {
        this.taskIsAssigned = assigned;
    }

    @Override
    public String getPriority() {
        return priority.toString();
    }

    @Override
    public void setPriority(TaskPriority priority) {
        if (this.priority.equals(priority)){
            throw new IllegalArgumentException(String.format("Priority is already set as %s ", priority));
        }
        this.priority = priority;
    }

    @Override
    public String print() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(String.format("%s%n", getType()));
        stringBuilder.append(String.format("ID: %d%n", getId()));
        stringBuilder.append(String.format("Title: %s%n", getTitle()));
        stringBuilder.append(String.format("Description: %s%n", getDescription()));

        return stringBuilder.toString();
    }

    public String printComments(){
        StringBuilder stringBuilder = new StringBuilder();
        if (comments.size() == 0){
            stringBuilder.append("--NO COMMENTS--");
            stringBuilder.append(System.lineSeparator());
        }
        if (comments.size() > 0){
            stringBuilder.append("--COMMENTS--");
            stringBuilder.append(System.lineSeparator());
            for (Comment comment : comments) {
                stringBuilder.append(comment.toString());
                stringBuilder.append(System.lineSeparator());
            }
        }
        return stringBuilder.toString();
    }
}
