package com.telerikacademy.project.taskmanagementsystem.models.tasks.contracts.enums.story;

public enum StoryStatus {

    NOT_DONE,
    IN_PROGRESS,
    DONE;


    @Override
    public String toString() {
        switch (this) {
            case NOT_DONE:
                return "Not Done";
            case IN_PROGRESS:
                return "In Progress";
            case DONE:
                return "Done";
            default:
                throw new IllegalArgumentException("There is no such a story status");
        }
    }

}
