package com.telerikacademy.project.taskmanagementsystem.models.tasks.contracts.enums.bug;

public enum Severity {

    MINOR,
    MAJOR,

    CRITICAL;

    @Override
    public String toString() {
        switch (this) {
            case CRITICAL:
                return "Critical";
            case MAJOR:
                return "Major";
            case MINOR:
                return "Minor";
            default:
                throw new IllegalArgumentException("There is no such a bug severity");
        }
    }
}
