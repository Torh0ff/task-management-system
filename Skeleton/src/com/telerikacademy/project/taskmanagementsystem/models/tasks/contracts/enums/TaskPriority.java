package com.telerikacademy.project.taskmanagementsystem.models.tasks.contracts.enums;

public enum TaskPriority {

    HIGH,
    MEDIUM,
    LOW;

    @Override
    public String toString() {
        switch(this){
            case HIGH:
                return "High";
            case MEDIUM:
                return "Medium";
            case LOW:
                return "Low";
            default:
                throw new IllegalArgumentException("There is no such a priority");
        }
    }
}
