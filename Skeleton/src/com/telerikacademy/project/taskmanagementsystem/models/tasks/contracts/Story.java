package com.telerikacademy.project.taskmanagementsystem.models.tasks.contracts;

import com.telerikacademy.project.taskmanagementsystem.models.tasks.contracts.enums.TaskPriority;
import com.telerikacademy.project.taskmanagementsystem.models.tasks.contracts.enums.story.Size;
import com.telerikacademy.project.taskmanagementsystem.models.tasks.contracts.enums.story.StoryStatus;

public interface Story extends Task, Assignable{

    String getSize();

    void setSize(Size size);

    void setStatus(StoryStatus status);



}
