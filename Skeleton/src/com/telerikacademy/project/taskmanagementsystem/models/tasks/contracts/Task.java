package com.telerikacademy.project.taskmanagementsystem.models.tasks.contracts;

import com.telerikacademy.project.taskmanagementsystem.models.tasks.contracts.enums.TaskPriority;
import com.telerikacademy.project.taskmanagementsystem.models.tasks.contracts.enums.TaskType;
import com.telerikacademy.project.taskmanagementsystem.models.team.contacts.Namable;

public interface Task extends Commentable, Identifiable, Loggable, Namable, Assignable {

    String getDescription();

    String getTitle();

    String getStatus();

    int getId();

    TaskType getType();

    String getPriority();

    String print();

    void setPriority(TaskPriority priority);

}
