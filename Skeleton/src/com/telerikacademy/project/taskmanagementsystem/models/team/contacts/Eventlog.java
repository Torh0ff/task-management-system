package com.telerikacademy.project.taskmanagementsystem.models.team.contacts;

public interface Eventlog {
    String viewInfo();
    String getDescription();
}
