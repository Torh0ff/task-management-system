package com.telerikacademy.project.taskmanagementsystem.models.team;

import com.telerikacademy.project.taskmanagementsystem.models.team.contacts.Board;
import com.telerikacademy.project.taskmanagementsystem.models.team.contacts.Eventlog;
import com.telerikacademy.project.taskmanagementsystem.models.team.contacts.Person;
import com.telerikacademy.project.taskmanagementsystem.models.team.contacts.Team;
import com.telerikacademy.project.taskmanagementsystem.utils.ValidationHelpers;

import java.util.ArrayList;
import java.util.List;

public class TeamImpl implements Team {

    private static final int NAME_MIN_LENGTH = 5;
    private static final int NAME_MAX_LENGTH = 15;

    private String name;
    private final List<Person> people;
    private final List<Board> boards;
    private final List<Eventlog> history;


    public TeamImpl(String name) {
        setName(name);
        people = new ArrayList<>();
        boards = new ArrayList<>();
        history = new ArrayList<>();
    }


    @Override
    public String getName() {
        return this.name;
    }

    private void setName(String name) {
        ValidationHelpers.validateStringLength(name, NAME_MIN_LENGTH, NAME_MAX_LENGTH,
                "The name length must be between "
                        + NAME_MIN_LENGTH + " and "
                        + NAME_MAX_LENGTH + " symbols") ;
        this.name = name;
    }

    @Override
    public List<Person> getMembers() {
        return new ArrayList<>(people);
    }

    @Override
    public List<Board> getBoards() {
        return new ArrayList<>(boards);
    }

    @Override
    public void addMember(Person person) {
        this.people.add(person);
    }

    @Override
    public void removeMember(Person person) {
        this.people.remove(person);
    }

    @Override
    public void addBoard(Board board) {
        this.boards.add(board);
    }

    @Override
    public void removeBoard(Board board) {
        this.boards.remove(board);
    }

    @Override
    public List<Eventlog> getHistory() {
        return new ArrayList<>(this.history);
    }

    @Override
    public void addEventLog(Eventlog eventlog) {
        this.history.add(eventlog);
    }
}
