package com.telerikacademy.project.taskmanagementsystem.models.team;


import com.telerikacademy.project.taskmanagementsystem.models.tasks.contracts.Task;
import com.telerikacademy.project.taskmanagementsystem.models.team.contacts.Board;
import com.telerikacademy.project.taskmanagementsystem.models.team.contacts.Eventlog;
import com.telerikacademy.project.taskmanagementsystem.utils.ValidationHelpers;

import java.util.ArrayList;
import java.util.List;

public class BoardImpl implements Board {

    private static final int NAME_MIN_LENGTH = 5;
    private static final int NAME_MAX_LENGTH = 10;

    private String name;
    private final List<Task> tasks;
    private final List<Eventlog> eventLogs;

    public BoardImpl(String name) {
        setName(name);
        this.tasks = new ArrayList<>();
        this.eventLogs = new ArrayList<>();
    }


    @Override
    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        ValidationHelpers.validateStringLength(name, NAME_MIN_LENGTH, NAME_MAX_LENGTH,
                "The name length must be between "
                        + NAME_MIN_LENGTH + " and "
                        + NAME_MAX_LENGTH + " symbols") ;
        this.name  = name;
    }


    @Override
    public List<Task> getTasks() {
        return new ArrayList<>(tasks);
    }

    @Override
    public List<Eventlog> getLogs() {
        return new ArrayList<>(eventLogs);
    }

    @Override
    public void addEventLog(Eventlog eventlog) {
        this.eventLogs.add(eventlog);
    }

    @Override
    public void deleteTask(Task task) {
        this.tasks.remove(task);
    }

    @Override
    public void addTask(Task task) {
        this.tasks.add(task);
    }
}
