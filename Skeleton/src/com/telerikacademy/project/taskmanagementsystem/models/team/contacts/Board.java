package com.telerikacademy.project.taskmanagementsystem.models.team.contacts;



import com.telerikacademy.project.taskmanagementsystem.models.tasks.contracts.Task;

import java.util.List;

public interface Board extends Namable{

    List<Task> getTasks();

    List<Eventlog> getLogs();

    void addEventLog(Eventlog eventlog);

    void deleteTask(Task task);

    void addTask(Task task);
}
