package com.telerikacademy.project.taskmanagementsystem.models.team.contacts;

import java.util.List;

public interface Team extends Namable{

    List<Person> getMembers();

    List<Board> getBoards();

    void addMember(Person person);

    void removeMember(Person person);

    void addBoard(Board board);

    void removeBoard(Board board);

    List<Eventlog> getHistory();

    void addEventLog(Eventlog eventlog);
}
