package com.telerikacademy.project.taskmanagementsystem.models.team;


import com.telerikacademy.project.taskmanagementsystem.models.team.contacts.Eventlog;
import com.telerikacademy.project.taskmanagementsystem.utils.ValidationHelpers;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class EventLogImpl implements Eventlog {

    private final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MMMM-yyyy HH:mm:ss");

    private String description;
    private final LocalDateTime timestamp;


    public EventLogImpl(String description) {
        setDescription(description);
        this.timestamp = LocalDateTime.now();
    }


    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        ValidationHelpers.validateNotNull(description);
        this.description = description;
    }

    public String viewInfo() {
        return String.format("[%s] %s", timestamp.format(formatter), description);
    }
}
