package com.telerikacademy.project.taskmanagementsystem.models.team.contacts;

public interface Namable {
    String getName();
}
