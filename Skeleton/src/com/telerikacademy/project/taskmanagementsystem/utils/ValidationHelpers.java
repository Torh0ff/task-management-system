package com.telerikacademy.project.taskmanagementsystem.utils;

import com.telerikacademy.project.taskmanagementsystem.exceptions.InvalidArgumentCountException;

import java.util.List;

public class ValidationHelpers {

    public static void validateIntRange(int minLength, int maxLength, int actualLength, String errorMessage) {
        if (actualLength < minLength || actualLength > maxLength) {
            throw new IllegalArgumentException(errorMessage);
        }
    }

    public static void validateStringLength(String stringToValidate, int minLength, int maxLength, String errorMessage) {
        validateIntRange(minLength, maxLength, stringToValidate.length(), errorMessage);
    }

    public static void validateArgumentsCount(List<String> list, int expectedNumberOfParameters) {
        if (list.size() < expectedNumberOfParameters || list.size() > expectedNumberOfParameters) {
            throw new InvalidArgumentCountException(
                    String.format("Invalid number of arguments. Expected: %d; received: %d.",
                            expectedNumberOfParameters, list.size())
            );
        }
    }

    public static void validateNotNull(String description) {
        if (description.isEmpty()){
            throw new IllegalArgumentException("Cannot be null!");
        }
    }
}
