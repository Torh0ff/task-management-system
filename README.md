<img src="https://webassets.telerikacademy.com/images/default-source/logos/telerik-academy.svg" alt="logo" width="300px" style="margin-top: 20px;"/>

# Task Management System

## 1. Project Description

Design and implement a Tasks Management console application.

The application will be used by a small team of developers, who need to keep track of all the tasks, surrounding a software product they are building.
## 2. Goals

Build the whole application from scratch following the OOP best practices, write clean code, implement proper validations and exceptions handling, including Unit tests.

## 3. Functional Requirements

> *Note:* The application must support multiple teams.
> 
> Each team `must` have a name, members, and boards.

· The name `must` be unique in the application.

· The name is a string between 5 and 15 symbols.

#### Each member must have a name, list of tasks and activity history.

· The name `must` be unique in the application.

· The name is a string between 5 and 15 symbols.

#### Each board must have a name, list of tasks and activity history.

· Name `must` be unique in the team.

· Name is a string between 5 and 10 symbols.

#### There are 3 types of tasks: `bug`, `story`, and `feedback`.

### Class Bug();

> *Note:* Bugs `must` have an ID, a title, a description, a list of steps to reproduce it, a priority, a severity, a status, an assignee, a list of comments and a list of changes history.

· Title is a string between `10` and `50` symbols.

· Description is a string between `10` and `500` symbols.

· Steps to reproduce is a list of strings.

· Priority is one of the following: `High`, `Medium`, or `Low`.

· Severity is one of the following: `Critical`, `Major`, or `Minor`.

· Status is one of the following: `Active` or `Fixed`.

· Assignee is a member from the team.

· Comments is a list of comments (string messages with an author).

· History is a list of all changes (string messages) that were done to the bug

### Class Story();

> *Note:* Stories `must` have an ID, a title, a description, a priority, a size, a status, an assignee, a list of comments and a list of changes history.

· Title is a string between `10` and `50` symbols.

· Description is a string between `10` and `500` symbols.

· Priority is one of the following: `High`, `Medium`, or `Low`.

· Size is one of the following: `Large`, `Medium`, or `Small`.

· Status is one of the following: `Not Done`, `InProgress`, or `Done`.

· Assignee is a member from the team.

· Comments is a list of comments (string messages with author).

· History is a list of all changes (string messages) that were done to the story.

### Class Feedback();

> *Note:* Feedbacks `must` have an ID, a title, a description, a rating, a status, a list of comments and a list of changes history.

· Title is a string between `10` and `50` symbols.

· Description is a string between `10` and `500` symbols.

· Rating is an `integer`.

· Status is one of the following: `New`, `Unscheduled`, `Scheduled`, or `Done`.

· Comments is a list of comments (string messages with author).

· History is a list of all changes (string messages) that were done to the feedback.

#### Note: IDs of tasks must be unique in the application i.e., if we have a bug with ID 42 then we cannot have a story or a feedback with ID 42

## 4. Operations

### The application must support the following operations:

#### · Create a new person.

#### · Show all people.

#### · Show person's activity.

#### · Create a new team.

#### · Show all teams.

#### · Show team's activity.

#### · Add person to team.

#### · Show all team members.

#### · Create a new board in a team.

#### · Show all team boards.

#### · Show board's activity.

#### · Create a new Bug/Story/Feedback in a board.

#### · Change the Priority/Severity/Status of a bug.

#### · Change the Priority/Size/Status of a story.

#### · Change the Rating/Status of a feedback.

#### · Assign/Unassign a task to a person.

#### · Add comment to a task.

### · Listing:

####  List all tasks (display the most important info).

· `Filter by title`

· `Sort by title`

####  List bugs/stories/feedback only.

· `Filter by status and/or assignee`

· `Sort by title/priority/severity/size/rating (depending on the task type)`

#### List tasks with assignee.

· `Filter by status and/or assignee`

· `Sort by title`

> *Note:* There are also tests to verify your methods work as they should.

## 5. Use cases

### Use case #1

One of the developers has noticed a bug in the company’s product. He starts the
application and goes on to create a new Task for it. 
He creates a new Bug and gives it the title "The program freezes when the Log In button is clicked.
"For the description he adds "This needs to be fixed quickly!", he marks the Bug as High priority and gives it Critical severity. 
Since it is a new bug, it gets the Active status. 
The developer also assigns it to the senior developer in the team. 
To be able to fix the bug, the senior developer needs to know how to reproduce it, 
so the developer who logged the bug adds a list of steps to reproduce: 
1. Open the application; 
2. Click "Log In"; 
3. The application freezes!" The bug is saved to the application and is ready to be fixed.

### Use case #2

A new developer has joined the team. 
One of the other developers starts the application and creates a new team member. 
After that, he adds the new team member to one of the existing teams and assigns all Critical bugs to him.

### Use case #3

One of the developers has fixed a bug that was assigned to him. 
He adds a comment to that bug, saying "This one took me a while, but it is fixed now!", and then changes the status of the bug to Done. 
Just to be sure, he checks the changes history list of the bug and sees that the last entry in the list says, 
"The status of item with ID 42 switched from Active to Done."

## 6. Technical Requirements

### Follow the OOP best practices:

#### Use data encapsulation.

#### Use inheritance and polymorphism properly.

#### Use interfaces and abstract classes properly.

#### Use static members properly.

#### Use enumerations properly.

#### Aim for strong cohesion and loose coupling.

### Follow guidelines for writing clean code:

#### Proper naming of classes, methods, and fields.

#### Small classes and methods.

#### Well formatted and consistent code.

#### No duplicate code.

### Implement proper user input validation and display meaningful user messages.
 
### Implement proper exception handling.

#### Prefer using the Streaming API, then using ordinary loops, wherever you can.

#### Cover the core functionality with unit tests (at least 80% code coverage of the models and commands).

#### There is no need to test the printing commands.

### Use Git to keep your source code and for team collaboration.

#### Sample Input

```none
CREATEPERSON Aleks
CREATEPERSON Iliyan
CREATETEAM Telerik
CREATEPERSON Ilon_Musk
CREATEPERSON Bill_Gates
CREATETEAM Twitter
ADDPERSONTOTEAM Iliyan Telerik
ADDPERSONTOTEAM Aleks Telerik
ADDPERSONTOTEAM Bill_Gates Twitter
ADDPERSONTOTEAM Ilon_Musk Twitter
CREATENEWBOARDINATEAM webIssues Telerik
CREATENEWBOARDINATEAM OthIssues Twitter
CREATEBUG {{downloading Issue}} Major High webIssues Telerik downloading //*cannot connect to the web sites*//
CREATESTORY {{The programm runs smooth with no bugs}} All-is-good Small Medium webIssues Telerik
CREATEFEEDBACK {{Quick Solution Amazing Job Guys Thank you!}} PositiveFeedback 10 webIssues Telerik
CREATEBUG {{just Another Bug}} Major High OthIssues Twitter downloadingIssue //*cannot connect to the web sites*//
CREATESTORY {{There Is Something Wrong there AGAIN!!!}} SomethingWrong Large High OthIssues Twitter
CREATEFEEDBACK {{Very bad guys,I'm very dissapointed!}} NegativeFeed 1 OthIssues Twitter
ASSIGNTASK 1 Aleks
ASSIGNTASK 1 Ilon_Musk
ASSIGNTASK 3 Bill_gates
ASSIGNTASK 2 Ilon_Musk
ASSIGNTASK 3 Ilon_Musk
ASSIGNTASK 5 Iliyan
ASSIGNTASK 2 Aleks
ASSIGNTASK 5 Bill_Gates
SHOWTEAMSACTIVITY Twitter
SHOWTEAMSACTIVITY Telerik
SHOWBOARDACTIVITY webIssues
CHANGEBUGSTATUS 1 Fixed
CHANGEPRIORITY 1 Medium
CHANGEPRIORITY 2 Medium
CHANGEPRIORITY 2 Medium 
CHANGEPRIORITY 2 Low
CHANGEPRIORITY 1 Low
CHANGESIZE 2 Large
CHANGESIZE 2 Small
CHANGESIZE 2 Small
CHANGESIZE 2 Large
CHANGESTORYSTATUS 2 Not_Done
CHANGESTORYSTATUS 2 Not_Done
CHANGESTORYSTATUS 2 Done
CHANGEBUGSEVERITY 1 Minor
CHANGEBUGSEVERITY 1 Minor
ADDCOMMENT {{can't reproduce the bug}} Aleks 2
ADDCOMMENT {{never seen such bugs before}} Aleks 1
ADDCOMMENT {{doing tests, report is going to be sent}} Aleks 3
SHOWALLTASKSCOMMENTS SomethingWrong
SHOWALLTASKSCOMMENTS downloading
SHOWALLTASKSCOMMENTS NegativeFeedback
CREATEPERSON Stanislav
ADDPERSONTOTEAM Stanislav Telerik
ADDCOMMENT {{There is something wrong there}} Iliyan 1
ADDCOMMENT {{What happened to you guys ?}} Stanislav 1
ADDCOMMENT {{Blah blah blah}} Stanislav 2
ADDCOMMENT {{whatz Upppp}} Stanislav 2
SHOWALLTASKSCOMMENTS SomethingWrong
SHOWALLTASKSCOMMENTS downloading
SHOWALLTASKSCOMMENTS PositiveFeedback
ASSIGNTASK 1 Aleks
ASSIGNTASK 1 Iliyan
ASSIGNTASK 1 Aleks
ASSIGNTASK 2 Aleks
UNASSIGNTASK 1 Aleks
UNASSIGNTASK 1 Aleks
UNASSIGNTASK 1 Iliyan
SHOWPERSONSACTIVITY Iliyan
SHOWPERSONSACTIVITY Aleks
SHOWPERSONSACTIVITY Stanislav
SHOWBOARDACTIVITY webIssues
SHOWTEAMSACTIVITY Telerik
FILTERBUGSBYPRIORITY Medium
FILTERBUGSBYPRIORITY Low
FILTERBUGSBYPRIORITY High
FILTERSTORIESBYSTATUS Not_Done
FILTERSTORIESBYSTATUS In_Progress
FILTERFEEDBACKSBYSTATUS Scheduled
FILTERFEEDBACKSBYSTATUS UnScheduled
FILTERFEEDBACKSBYRATING 1
FILTERFEEDBACKSBYRATING 10
SHOWTASKACTIVITY 1
exit
```
#### Sample Output

```none
User with name 'Aleks' was created!
User with name 'Iliyan' was created!
Created Team with name 'Telerik'.
User with name 'Ilon_Musk' was created!
User with name 'Bill_Gates' was created!
Created Team with name 'Twitter'.
User with name 'Iliyan' was added to team 'Telerik'.
User with name 'Aleks' was added to team 'Telerik'.
User with name 'Bill_Gates' was added to team 'Twitter'.
User with name 'Ilon_Musk' was added to team 'Twitter'.
webIssues board was created in Telerik team.
OthIssues board was created in Twitter team.
Bug downloading with ID 1 has been created.
Story All-is-good with ID 2 has been created.
Feedback PositiveFeedback with ID 3 has been created.
Bug downloadingIssue with ID 4 has been created.
Story SomethingWrong with ID 5 has been created.
Feedback NegativeFeed with ID 6 has been created.
User 'Aleks' has been assigned to the task ID:1.
User 'Ilon_Musk' is a part of a team 'Twitter' and cannot be assign to the task ID:1
There is no such person with name: Bill_gates.
User 'Ilon_Musk' is a part of a team 'Twitter' and cannot be assign to the task ID:2
User 'Ilon_Musk' is a part of a team 'Twitter' and cannot be assign to the task ID:3
User 'Iliyan' is a part of a team 'Telerik' and cannot be assign to the task ID:5
User 'Aleks' has already been assigned to other task, and cannot be re-assigned at the moment.
User 'Bill_Gates' has been assigned to the task ID:5.
==================================================================================================
Team with name Twitter has activity:
  --1 [05-January-2023 10:07:18] Team 'Twitter' was created on this Date.
  --2 [05-January-2023 10:07:18] User with name 'Bill_Gates' was added to team 'Twitter'.
  --3 [05-January-2023 10:07:18] User with name 'Ilon_Musk' was added to team 'Twitter'.
  --4 [05-January-2023 10:07:18] 'OthIssues' board was created in 'Twitter' team.
  --5 [05-January-2023 10:07:18] 'downloadingIssue' Bug with ID 4 was assigned to the board 'OthIssues'.
  --6 [05-January-2023 10:07:18] 'SomethingWrong' Story with ID 5 was assigned to the board 'OthIssues'
  --7 [05-January-2023 10:07:18] 'NegativeFeed' Feedback with ID 6 was assigned to the board 'OthIssues'.
==================================================================================================
==================================================================================================
Team with name Telerik has activity:
  --1 [05-January-2023 10:07:18] Team 'Telerik' was created on this Date.
  --2 [05-January-2023 10:07:18] User with name 'Iliyan' was added to team 'Telerik'.
  --3 [05-January-2023 10:07:18] User with name 'Aleks' was added to team 'Telerik'.
  --4 [05-January-2023 10:07:18] 'webIssues' board was created in 'Telerik' team.
  --5 [05-January-2023 10:07:18] 'downloading' Bug with ID 1 was assigned to the board 'webIssues'.
  --6 [05-January-2023 10:07:18] 'All-is-good' Story with ID 2 was assigned to the board 'webIssues'
  --7 [05-January-2023 10:07:18] 'PositiveFeedback' Feedback with ID 3 was assigned to the board 'webIssues'.
==================================================================================================
==================================================================================================
Board with name webIssues has activity:
  --1 [05-January-2023 10:07:18] Board 'webIssues' was created on this Date.
  --2 [05-January-2023 10:07:18] 'webIssues' board was created in 'Telerik' team.
  --3 [05-January-2023 10:07:18] 'downloading' Bug with ID 1 was assigned to the board 'webIssues'.
  --4 [05-January-2023 10:07:18] 'All-is-good' Story with ID 2 was assigned to the board 'webIssues'
  --5 [05-January-2023 10:07:18] 'PositiveFeedback' Feedback with ID 3 was assigned to the board 'webIssues'.
==================================================================================================
Status of bug 'downloading' with ID 1 has been changed to 'Fixed'.
Priority of 'Bug' with ID 1 has been changed to 'Medium'.
Priority is already set as Medium 
Priority is already set as Medium 
Priority of 'Story' with ID 2 has been changed to 'Low'.
Priority of 'Bug' with ID 1 has been changed to 'Low'.
Size of Story 'All-is-good' with ID 2 has been changed to 'Large'.
Size of Story 'All-is-good' with ID 2 has been changed to 'Small'.
Size is already on Small status
Size of Story 'All-is-good' with ID 2 has been changed to 'Large'.
Status already on Not Done status
Status already on Not Done status
Status of story 'All-is-good' with ID 2 has been changed to 'Done'.
Severity of bug 'downloading' with ID 1 has been changed to 'Minor'.
Severity of the bug 1 is already set as Minor 
User 'Aleks' added a comment to the task 'All-is-good' ID: 2.
User 'Aleks' added a comment to the task 'downloading' ID: 1.
User 'Aleks' added a comment to the task 'PositiveFeedback' ID: 3.
==================================================================================================
 Task:'SomethingWrong'
 ID:5
 --NO COMMENTS--
==================================================================================================
==================================================================================================
 Task:'downloading'
 ID:1
 --COMMENTS--
 [never seen such bugs before]
 User: Aleks
 -----------------------------
==================================================================================================
There is no such Task with name: NegativeFeedback.
User with name 'Stanislav' was created!
User with name 'Stanislav' was added to team 'Telerik'.
User 'Iliyan' added a comment to the task 'downloading' ID: 1.
User 'Stanislav' added a comment to the task 'downloading' ID: 1.
User 'Stanislav' added a comment to the task 'All-is-good' ID: 2.
User 'Stanislav' added a comment to the task 'All-is-good' ID: 2.
==================================================================================================
 Task:'SomethingWrong'
 ID:5
 --NO COMMENTS--
==================================================================================================
==================================================================================================
 Task:'downloading'
 ID:1
 --COMMENTS--
 [never seen such bugs before]
 User: Aleks
 -----------------------------
 [There is something wrong there]
 User: Iliyan
 -----------------------------
 [What happened to you guys ?]
 User: Stanislav
 -----------------------------
==================================================================================================
==================================================================================================
 Task:'PositiveFeedback'
 ID:3
 --COMMENTS--
 [doing tests, report is going to be sent]
 User: Aleks
 -----------------------------
==================================================================================================
User 'Aleks' has already been assigned to other task, and cannot be re-assigned at the moment.
This task has already been assigned to other user, therefore cannot be assign to 'Iliyan'
User 'Aleks' has already been assigned to other task, and cannot be re-assigned at the moment.
User 'Aleks' has already been assigned to other task, and cannot be re-assigned at the moment.
User 'Aleks' has been unassigned from the task 'downloading' ID:1.
Task 'downloading' ID:1 hasn't been assigned to the user 'Aleks'
Task 'downloading' ID:1 hasn't been assigned to the user 'Iliyan'
==================================================================================================
User Iliyan has activity: 
--1 [05-January-2023 10:07:18] User 'Iliyan' was created on this Date.
--2 [05-January-2023 10:07:18] User with name 'Iliyan' was added to team 'Telerik'.
--3 [05-January-2023 10:07:18] User 'Iliyan' added a comment to the task 'downloading' ID: 1.
==================================================================================================
==================================================================================================
User Aleks has activity: 
--1 [05-January-2023 10:07:18] User 'Aleks' was created on this Date.
--2 [05-January-2023 10:07:18] User with name 'Aleks' was added to team 'Telerik'.
--3 [05-January-2023 10:07:18] User 'Aleks' has been assigned to the task ID:1.
--4 [05-January-2023 10:07:18] User 'Aleks' added a comment to the task 'All-is-good' ID: 2.
--5 [05-January-2023 10:07:18] User 'Aleks' added a comment to the task 'downloading' ID: 1.
--6 [05-January-2023 10:07:18] User 'Aleks' added a comment to the task 'PositiveFeedback' ID: 3.
--7 [05-January-2023 10:07:18] User 'Aleks' has been unassigned from the task 'downloading' ID:1.
==================================================================================================
==================================================================================================
User Stanislav has activity: 
--1 [05-January-2023 10:07:18] User 'Stanislav' was created on this Date.
--2 [05-January-2023 10:07:18] User with name 'Stanislav' was added to team 'Telerik'.
--3 [05-January-2023 10:07:18] User 'Stanislav' added a comment to the task 'downloading' ID: 1.
--4 [05-January-2023 10:07:18] User 'Stanislav' added a comment to the task 'All-is-good' ID: 2.
--5 [05-January-2023 10:07:18] User 'Stanislav' added a comment to the task 'All-is-good' ID: 2.
==================================================================================================
==================================================================================================
Board with name webIssues has activity:
  --1 [05-January-2023 10:07:18] Board 'webIssues' was created on this Date.
  --2 [05-January-2023 10:07:18] 'webIssues' board was created in 'Telerik' team.
  --3 [05-January-2023 10:07:18] 'downloading' Bug with ID 1 was assigned to the board 'webIssues'.
  --4 [05-January-2023 10:07:18] 'All-is-good' Story with ID 2 was assigned to the board 'webIssues'
  --5 [05-January-2023 10:07:18] 'PositiveFeedback' Feedback with ID 3 was assigned to the board 'webIssues'.
==================================================================================================
==================================================================================================
Team with name Telerik has activity:
  --1 [05-January-2023 10:07:18] Team 'Telerik' was created on this Date.
  --2 [05-January-2023 10:07:18] User with name 'Iliyan' was added to team 'Telerik'.
  --3 [05-January-2023 10:07:18] User with name 'Aleks' was added to team 'Telerik'.
  --4 [05-January-2023 10:07:18] 'webIssues' board was created in 'Telerik' team.
  --5 [05-January-2023 10:07:18] 'downloading' Bug with ID 1 was assigned to the board 'webIssues'.
  --6 [05-January-2023 10:07:18] 'All-is-good' Story with ID 2 was assigned to the board 'webIssues'
  --7 [05-January-2023 10:07:18] 'PositiveFeedback' Feedback with ID 3 was assigned to the board 'webIssues'.
  --8 [05-January-2023 10:07:18] User with name 'Stanislav' was added to team 'Telerik'.
==================================================================================================
There are no bugs with priority 'Medium'
----------------------------------------------
Bugs filtered by 'Low' priority:
Bug
ID: 1
Title: downloading
Description: downloading Issue
Severity: Minor
Priority: Low
Steps: [cannot connect to the web sites]
Status: Fixed
Assignee: no assignee
----------------------------------------------

Bugs filtered by 'High' priority:
Bug
ID: 4
Title: downloadingIssue
Description: just Another Bug
Severity: Major
Priority: High
Steps: [cannot connect to the web sites]
Status: Active
Assignee: no assignee
----------------------------------------------

Stories filtered by 'Not Done' status:
Story
ID: 5
Title: SomethingWrong
Description: There Is Something Wrong there AGAIN!!!
Size: Large
Priority: High
Assignee: Bill_Gates
----------------------------------------------

There are no stories with status 'In Progress'
----------------------------------------------
There are no feedbacks with status 'Scheduled'
----------------------------------------------
There are no feedbacks with status 'Uncheduled'
----------------------------------------------
Feedbacks filtered by '1' points rating:
Feedback
ID: 6
Title: NegativeFeed
Description: Very bad guys,I'm very dissapointed!
Rating: 1
Status: New
Assignee: no assignee
----------------------------------------------

Feedbacks filtered by '10' points rating:
Feedback
ID: 3
Title: PositiveFeedback
Description: Quick Solution Amazing Job Guys Thank you!
Rating: 10
Status: New
Assignee: no assignee
----------------------------------------------

==================================================================================================
Task with name 'downloading' has activity: 
  --1 [05-January-2023 10:07:18] Bug 'downloading' was created on this Date.
  --2 [05-January-2023 10:07:18] User 'Aleks' has been assigned to the task ID:1.
  --3 [05-January-2023 10:07:18] Status of bug 'downloading' with ID 1 has been changed to 'Fixed'.
  --4 [05-January-2023 10:07:18] Priority of 'downloading' with ID 1 has been changed to 'Medium'.
  --5 [05-January-2023 10:07:18] Priority of 'downloading' with ID 1 has been changed to 'Low'.
  --6 [05-January-2023 10:07:18] Severity of bug 'downloading' with ID 1 has been changed to 'Minor'.
  --7 [05-January-2023 10:07:18] User 'Aleks' added a comment to the task 'downloading' ID: 1.
  --8 [05-January-2023 10:07:18] User 'Iliyan' added a comment to the task 'downloading' ID: 1.
  --9 [05-January-2023 10:07:18] User 'Stanislav' added a comment to the task 'downloading' ID: 1.
  --10 [05-January-2023 10:07:18] User 'Aleks' has been unassigned from the task 'downloading' ID:1.
==================================================================================================
```

#### Sample Input for Listing Commands for Bugs.

```none
CREATEPERSON Ivann
CREATEPERSON Iliyan
CREATETEAM Telerik
ADDPERSONTOTEAM Iliyan Telerik
ADDPERSONTOTEAM Ivann Telerik
CREATENEWBOARDINATEAM webIssues Telerik
CREATEBUG {{downloading Issue}} Major High webIssues Telerik downloading //*The problem was, I had logged the bug several days before but later found that my steps were not spot on. I thought I had identified the cause of the issue, but the steps that I had originally documented would not reproduce the issue every time. This made it really difficult for the developer team who were assigned to find the bug’s cause. It was while I was testing another issue that I realized what the the problem was and so could update the bug ticket. This experience drove home to me the importance of not only reporting a bug, but documenting it correctly.*//
CREATEBUG {{downloading Bug}} Major High webIssues Telerik downloadingIssue //*cannot connect to the web sites*//
CREATEBUG {{downloading drivers Issue}} Major High webIssues Telerik justAnotherBug //*cannot download the drivers from the web site*//
CHANGEPRIORITY 1 Medium
CHANGEPRIORITY 2 Medium
CHANGEPRIORITY 2 Medium 
CHANGEPRIORITY 2 Low
CHANGEPRIORITY 1 Low
CHANGEBUGSEVERITY 1 Minor
CHANGEBUGSEVERITY 1 Minor
ASSIGNTASK 1 Ivann
ASSIGNTASK 2 Iliyan
SHOWTEAMSACTIVITY Telerik
UNASSIGNTASK 1 Ivann
UNASSIGNTASK 2 Iliyan
CHANGEBUGSTATUS 1 Fixed
CHANGEBUGSTATUS 1 Active
SHOWPERSONSACTIVITY Ivann
SHOWPERSONSACTIVITY Iliyan
LISTALLTASKSWITHASSIGNEE Telerik Iliyan
LISTALLTASKSWITHASSIGNEE Telerik Ivann
LISTALLTASKSWITHSTATUS Active
LISTALLTASKSWITHSTATUS Done
LISTALLTASKS downloading
LISTTASKFILTERESBYASSIGNEE Bug Ivann
LISTTASKSFILTEREDBYSTATUS Bug ACTIVE
LISTTASKSSORTEDBY Bug status ACTIVE
SHOWTASKACTIVITY 1
SHOWTASKACTIVITY 2
FILTERBUGSBYPRIORITY Low
exit
```

#### Sample Output for Listing Commands for Bugs.

```none
User with name 'Ivann' was created!
User with name 'Iliyan' was created!
Created Team with name 'Telerik'.
User with name 'Iliyan' was added to team 'Telerik'.
User with name 'Ivann' was added to team 'Telerik'.
webIssues board was created in Telerik team.
Bug downloading with ID 1 has been created.
Bug downloadingIssue with ID 2 has been created.
Bug justAnotherBug with ID 3 has been created.
Priority of 'Bug' with ID 1 has been changed to 'Medium'.
Priority of 'Bug' with ID 2 has been changed to 'Medium'.
Priority is already set as Medium 
Priority of 'Bug' with ID 2 has been changed to 'Low'.
Priority of 'Bug' with ID 1 has been changed to 'Low'.
Severity of bug 'downloading' with ID 1 has been changed to 'Minor'.
Severity of the bug 1 is already set as Minor 
User 'Ivann' has been assigned to the task ID:1.
User 'Iliyan' has been assigned to the task ID:2.
==================================================================================================
Team with name Telerik has activity:
  --1 [05-January-2023 10:29:35] Team 'Telerik' was created on this Date.
  --2 [05-January-2023 10:29:35] User with name 'Iliyan' was added to team 'Telerik'.
  --3 [05-January-2023 10:29:35] User with name 'Ivann' was added to team 'Telerik'.
  --4 [05-January-2023 10:29:35] 'webIssues' board was created in 'Telerik' team.
  --5 [05-January-2023 10:29:35] 'downloading' Bug with ID 1 was assigned to the board 'webIssues'.
  --6 [05-January-2023 10:29:35] 'downloadingIssue' Bug with ID 2 was assigned to the board 'webIssues'.
  --7 [05-January-2023 10:29:35] 'justAnotherBug' Bug with ID 3 was assigned to the board 'webIssues'.
==================================================================================================
User 'Ivann' has been unassigned from the task 'downloading' ID:1.
User 'Iliyan' has been unassigned from the task 'downloadingIssue' ID:2.
Status of bug 'downloading' with ID 1 has been changed to 'Fixed'.
Status of bug 'downloading' with ID 1 has been changed to 'Active'.
==================================================================================================
User Ivann has activity: 
--1 [05-January-2023 10:29:35] User 'Ivann' was created on this Date.
--2 [05-January-2023 10:29:35] User with name 'Ivann' was added to team 'Telerik'.
--3 [05-January-2023 10:29:35] User 'Ivann' has been assigned to the task ID:1.
--4 [05-January-2023 10:29:35] User 'Ivann' has been unassigned from the task 'downloading' ID:1.
==================================================================================================
==================================================================================================
User Iliyan has activity: 
--1 [05-January-2023 10:29:35] User 'Iliyan' was created on this Date.
--2 [05-January-2023 10:29:35] User with name 'Iliyan' was added to team 'Telerik'.
--3 [05-January-2023 10:29:35] User 'Iliyan' has been assigned to the task ID:2.
--4 [05-January-2023 10:29:35] User 'Iliyan' has been unassigned from the task 'downloadingIssue' ID:2.
==================================================================================================
Iliyan assignee has no tasks
Ivann assignee has no tasks
==================================================================================================
Tasks with status Active are:
 ---downloading
 ---downloadingIssue
 ---justAnotherBug
==================================================================================================
There are no tasks with status Done
==================================================================================================
Tasks with title containing 'downloading' are: 
 ---Id: 1, Title: downloading, Assigned to: no assignee
 ---Id: 2, Title: downloadingIssue, Assigned to: no assignee
==================================================================================================
Assignee Ivann has no Bug tasks.
==================================================================================================
Bug task with Active status are: 
 ---ID: 1 and title downloading
 ---ID: 2 and title downloadingIssue
 ---ID: 3 and title justAnotherBug
==================================================================================================
Option status is not supported
==================================================================================================
Task with name 'downloading' has activity: 
  --1 [05-January-2023 10:29:35] Bug 'downloading' was created on this Date.
  --2 [05-January-2023 10:29:35] Priority of 'downloading' with ID 1 has been changed to 'Medium'.
  --3 [05-January-2023 10:29:35] Priority of 'downloading' with ID 1 has been changed to 'Low'.
  --4 [05-January-2023 10:29:35] Severity of bug 'downloading' with ID 1 has been changed to 'Minor'.
  --5 [05-January-2023 10:29:35] User 'Ivann' has been assigned to the task ID:1.
  --6 [05-January-2023 10:29:35] User 'Ivann' has been unassigned from the task 'downloading' ID:1.
  --7 [05-January-2023 10:29:35] Status of bug 'downloading' with ID 1 has been changed to 'Fixed'.
  --8 [05-January-2023 10:29:35] Status of bug 'downloading' with ID 1 has been changed to 'Active'.
==================================================================================================
==================================================================================================
Task with name 'downloadingIssue' has activity: 
  --1 [05-January-2023 10:29:35] Bug 'downloadingIssue' was created on this Date.
  --2 [05-January-2023 10:29:35] Priority of 'downloadingIssue' with ID 2 has been changed to 'Medium'.
  --3 [05-January-2023 10:29:35] Priority of 'downloadingIssue' with ID 2 has been changed to 'Low'.
  --4 [05-January-2023 10:29:35] User 'Iliyan' has been assigned to the task ID:2.
  --5 [05-January-2023 10:29:35] User 'Iliyan' has been unassigned from the task 'downloadingIssue' ID:2.
==================================================================================================
Bugs filtered by 'Low' priority:
Bug
ID: 1
Title: downloading
Description: downloading Issue
Severity: Minor
Priority: Low
Steps: [The problem was, I had logged the bug several days before but later found that my steps were not spot on. I thought I had identified the cause of the issue, but the steps that I had originally documented would not reproduce the issue every time. This made it really difficult for the developer team who were assigned to find the bug’s cause. It was while I was testing another issue that I realized what the the problem was and so could update the bug ticket. This experience drove home to me the importance of not only reporting a bug, but documenting it correctly.]
Status: Active
Assignee: no assignee
----------------------------------------------
Bug
ID: 2
Title: downloadingIssue
Description: downloading Bug
Severity: Major
Priority: Low
Steps: [cannot connect to the web sites]
Status: Active
Assignee: no assignee
----------------------------------------------
```
#### Sample Input for Listing Commands for Stories.

```none
CREATEPERSON Ivann
CREATEPERSON Iliyan
CREATETEAM Telerik
ADDPERSONTOTEAM Iliyan Telerik
ADDPERSONTOTEAM Ivann Telerik
CREATENEWBOARDINATEAM webIssues Telerik
CREATESTORY {{There Is Something Wrong there}} AndAgaiWrong Medium Low webIssues Telerik
CREATESTORY {{There Is Something Wrong there AGAIN!!!}} SomethingWrong Large High webIssues Telerik
CREATESTORY {{There Is Something Wrong there as ALWAYS!!!}} whataHeckISThat Small Medium webIssues Telerik
ASSIGNTASK 1 Ivann
ASSIGNTASK 2 Iliyan
CHANGESTORYSTATUS 2 Not_Done
CHANGESTORYSTATUS 1 Done
LISTALLTASKSWITHASSIGNEE Telerik Iliyan
LISTALLTASKSWITHASSIGNEE Telerik Ivann
UNASSIGNTASK 1 Ivann
UNASSIGNTASK 2 Iliyan
LISTALLTASKSWITHSTATUS InProgress
LISTALLTASKSWITHSTATUS NotDone
LISTALLTASKS SomethingWrong
ASSIGNTASK 1 Ivann
ASSIGNTASK 2 Iliyan
LISTTASKFILTERESBYASSIGNEE Story Ivann
LISTTASKSFILTEREDBYSTATUS Story NotDone
LISTTASKSSORTEDBY Bug status Done
SHOWPERSONSACTIVITY Ivann
SHOWPERSONSACTIVITY Iliyan
SHOWTASKACTIVITY 1
SHOWTASKACTIVITY 2
exit
```
#### Sample Output for Listing and Sorting Commands for Stories.

```none
User with name 'Ivann' was created!
User with name 'Iliyan' was created!
Created Team with name 'Telerik'.
User with name 'Iliyan' was added to team 'Telerik'.
User with name 'Ivann' was added to team 'Telerik'.
webIssues board was created in Telerik team.
Story AndAgaiWrong with ID 1 has been created.
Story SomethingWrong with ID 2 has been created.
Story whataHeckISThat with ID 3 has been created.
User 'Ivann' has been assigned to the task ID:1.
User 'Iliyan' has been assigned to the task ID:2.
Status already on Not Done status
Status of story 'AndAgaiWrong' with ID 1 has been changed to 'Done'.
==================================================================================================
Tasks assigned to: Iliyan(Team Telerik's) are:
 ---SomethingWrong
==================================================================================================
==================================================================================================
Tasks assigned to: Ivann(Team Telerik's) are:
 ---AndAgaiWrong
==================================================================================================
User 'Ivann' has been unassigned from the task 'AndAgaiWrong' ID:1.
User 'Iliyan' has been unassigned from the task 'SomethingWrong' ID:2.
There are no tasks with status In Progress
==================================================================================================
Tasks with status Not Done are:
 ---SomethingWrong
 ---whataHeckISThat
==================================================================================================
==================================================================================================
Tasks with title containing 'SomethingWrong' are: 
 ---Id: 2, Title: SomethingWrong, Assigned to: no assignee
==================================================================================================
User 'Ivann' has been assigned to the task ID:1.
User 'Iliyan' has been assigned to the task ID:2.
==================================================================================================
Assignee Ivann have Story tasks: 
 ---AndAgaiWrong
==================================================================================================
==================================================================================================
Story task with Not Done status are: 
 ---ID: 2 and title SomethingWrong
 ---ID: 3 and title whataHeckISThat
==================================================================================================
Option status is not supported
==================================================================================================
User Ivann has activity: 
--1 [05-January-2023 10:30:29] User 'Ivann' was created on this Date.
--2 [05-January-2023 10:30:29] User with name 'Ivann' was added to team 'Telerik'.
--3 [05-January-2023 10:30:29] User 'Ivann' has been assigned to the task ID:1.
--4 [05-January-2023 10:30:29] User 'Ivann' has been unassigned from the task 'AndAgaiWrong' ID:1.
--5 [05-January-2023 10:30:29] User 'Ivann' has been assigned to the task ID:1.
==================================================================================================
==================================================================================================
User Iliyan has activity: 
--1 [05-January-2023 10:30:29] User 'Iliyan' was created on this Date.
--2 [05-January-2023 10:30:29] User with name 'Iliyan' was added to team 'Telerik'.
--3 [05-January-2023 10:30:29] User 'Iliyan' has been assigned to the task ID:2.
--4 [05-January-2023 10:30:29] User 'Iliyan' has been unassigned from the task 'SomethingWrong' ID:2.
--5 [05-January-2023 10:30:29] User 'Iliyan' has been assigned to the task ID:2.
==================================================================================================
==================================================================================================
Task with name 'AndAgaiWrong' has activity: 
  --1 [05-January-2023 10:30:29] Story 'AndAgaiWrong' was created on this Date.
  --2 [05-January-2023 10:30:29] User 'Ivann' has been assigned to the task ID:1.
  --3 [05-January-2023 10:30:29] Status of story 'AndAgaiWrong' with ID 1 has been changed to 'Done'.
  --4 [05-January-2023 10:30:29] User 'Ivann' has been unassigned from the task 'AndAgaiWrong' ID:1.
  --5 [05-January-2023 10:30:29] User 'Ivann' has been assigned to the task ID:1.
==================================================================================================
==================================================================================================
Task with name 'SomethingWrong' has activity: 
  --1 [05-January-2023 10:30:29] Story 'SomethingWrong' was created on this Date.
  --2 [05-January-2023 10:30:29] User 'Iliyan' has been assigned to the task ID:2.
  --3 [05-January-2023 10:30:29] User 'Iliyan' has been unassigned from the task 'SomethingWrong' ID:2.
  --4 [05-January-2023 10:30:29] User 'Iliyan' has been assigned to the task ID:2.
==================================================================================================
```

#### Sample Input for Listing and Sorting Commands for Feedbacks.

```none
CREATEPERSON Ivann
CREATEPERSON Iliyan
CREATETEAM Telerik
ADDPERSONTOTEAM Iliyan Telerik
ADDPERSONTOTEAM Ivann Telerik
CREATENEWBOARDINATEAM webIssues Telerik
CREATEFEEDBACK {{Quick Solution Amazing Job Guys Thank you!}} PositiveFeedback 10 webIssues Telerik
CREATEFEEDBACK {{Quick Solution Amazing Job Thank you!}} PositiveFeed 10 webIssues Telerik
CHANGEFEEDBACKSTATUS 1 Scheduled
CHANGEFEEDBACKSTATUS 2 UnScheduled
ASSIGNTASK 1 Ivann
ASSIGNTASK 2 Iliyan
CHANGEFEEDBACKSTATUS 3 Done
SHOWBOARDACTIVITY webIssues
LISTALLTASKSWITHASSIGNEE Telerik Iliyan
LISTALLTASKSWITHASSIGNEE Telerik Ivann
LISTALLTASKSWITHSTATUS New
LISTALLTASKSWITHSTATUS UnScheduled
LISTALLTASKS PositiveFeed
LISTTASKFILTERESBYASSIGNEE Feedback Ivann
LISTTASKSFILTEREDBYSTATUS Feedback New
LISTTASKSSORTEDBY Feedback status Scheduled
UNASSIGNTASK 1 Ivann
UNASSIGNTASK 2 Iliyan
CHANGEFEEDBACKSTATUS 1 Done
SHOWPERSONSACTIVITY Ivann
SHOWPERSONSACTIVITY Iliyan
exit
```

#### Sample Output for Listing and Sorting Commands for Feedbacks.

```none
User with name 'Ivann' was created!
User with name 'Iliyan' was created!
Created Team with name 'Telerik'.
User with name 'Iliyan' was added to team 'Telerik'.
User with name 'Ivann' was added to team 'Telerik'.
webIssues board was created in Telerik team.
Feedback PositiveFeedback with ID 1 has been created.
Feedback PositiveFeed with ID 2 has been created.
Status of feedback 'PositiveFeedback' with ID 1 has been changed to 'Scheduled'.
Status of feedback 'PositiveFeed' with ID 2 has been changed to 'Uncheduled'.
User 'Ivann' has been assigned to the task ID:1.
User 'Iliyan' has been assigned to the task ID:2.
There is no such task with ID 3 number
==================================================================================================
Board with name webIssues has activity:
  --1 [05-January-2023 10:31:45] Board 'webIssues' was created on this Date.
  --2 [05-January-2023 10:31:45] 'webIssues' board was created in 'Telerik' team.
  --3 [05-January-2023 10:31:45] 'PositiveFeedback' Feedback with ID 1 was assigned to the board 'webIssues'.
  --4 [05-January-2023 10:31:45] 'PositiveFeed' Feedback with ID 2 was assigned to the board 'webIssues'.
==================================================================================================
==================================================================================================
Tasks assigned to: Iliyan(Team Telerik's) are:
 ---PositiveFeed
==================================================================================================
==================================================================================================
Tasks assigned to: Ivann(Team Telerik's) are:
 ---PositiveFeedback
==================================================================================================
There are no tasks with status New
There are no tasks with status Unscheduled
==================================================================================================
Tasks with title containing 'PositiveFeed' are: 
 ---Id: 2, Title: PositiveFeed, Assigned to: Iliyan
 ---Id: 1, Title: PositiveFeedback, Assigned to: Ivann
==================================================================================================
==================================================================================================
Assignee Ivann have Feedback tasks: 
 ---PositiveFeedback
==================================================================================================
There are no Feedback tasks with status New.
Option status is not supported
User 'Ivann' has been unassigned from the task 'PositiveFeedback' ID:1.
User 'Iliyan' has been unassigned from the task 'PositiveFeed' ID:2.
Status of feedback 'PositiveFeedback' with ID 1 has been changed to 'Done'.
==================================================================================================
User Ivann has activity: 
--1 [05-January-2023 10:31:45] User 'Ivann' was created on this Date.
--2 [05-January-2023 10:31:45] User with name 'Ivann' was added to team 'Telerik'.
--3 [05-January-2023 10:31:45] User 'Ivann' has been assigned to the task ID:1.
--4 [05-January-2023 10:31:45] User 'Ivann' has been unassigned from the task 'PositiveFeedback' ID:1.
==================================================================================================
==================================================================================================
User Iliyan has activity: 
--1 [05-January-2023 10:31:45] User 'Iliyan' was created on this Date.
--2 [05-January-2023 10:31:45] User with name 'Iliyan' was added to team 'Telerik'.
--3 [05-January-2023 10:31:45] User 'Iliyan' has been assigned to the task ID:2.
--4 [05-January-2023 10:31:45] User 'Iliyan' has been unassigned from the task 'PositiveFeed' ID:2.
==================================================================================================
```